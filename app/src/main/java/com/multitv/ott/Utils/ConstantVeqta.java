package com.multitv.ott.Utils;

/**
 * Created by Lenovo on 04-03-2017.
 */


import android.text.SpannableString;

/**
 * Created by naseeb on 1/17/2017.
 */

public class ConstantVeqta {
    public static final int CONTENT_TYPE_MULTITV_PLAYER_ACTIVITY = 0;
    public static final int CONTENT_TYPE_SONY_PLAYER_ACTIVITY = 1;
    public static final int CONTENT_TYPE_VIU_PLAYER_ACTIVITY = 2;

    public static final String EXTRA_KEY = "EXTRA_KEY";

    public static final String EXTRA_REMINDER_DATA_KEY = "EXTRA_REMINDER_DATA_KEY";
    public static final String EXTRA_ICON_URL = "EXTRA_ICON_URL";
    public static final String EXTRA_NAME = "EXTRA_NAME";
    public static final String EXTRA_OPEN_HOME_SCREEN = "EXTRA_OPEN_HOME_SCREEN";
    public static SpannableString HTMLTEXT;
    public final static int TAB_SELECT_THARASH_HOLD = 8;
    public static int mTabSelectedCount = 0;

    public static final String EXTRA_CATEGORY_TYPE = "CATEGORY_TYPE";
    public static final String EXTRA_SEARCH_KEYWOARD = "SEARCH_KEYWORD";
    public static final String IS_FROM_SEARCH = "IS_FROM_SEARCH";

    public static final String EXTRA_TYPE = "EXTRA_TYPE";
    public static final String EXTRA_SEARCH_TEXT = "SEARCH_TEXT";
    public static final int TYPE_UNIVERSAL_SEARCH = 1;
    public static final int TYPE_MOVIE = 2;
    public static final int TYPE_VIDEO = 3;
    public static final int TYPE_LIVE = 4;
    public static final int TYPE_TV_SHOWS = 5;
    public static final int TYPE_DEFAULT = 6;
    public static final int TYPE_SONY_LIV = 7;
    public static final int TYPE_VIMEO = 8;
    public static final int TYPE_VIU = 8;

    public static final String VIDEO_ID = "VIDEO_ID";

    public static final String EXTRA_DATE = "EXTRA_DATE";

    public static final String URI_KEY = "MKRKEY:";
    public static final String EXTRA_LIVE_TV_ID = "EXTRA_LIVE_TV_ID";
    public static final String EXTRA_IS_15_MIN_REMINDER_SET = "EXTRA_IS_15_MIN_REMINDER_SET";

    public static String EXTRA_CONTENT_INFO = "contentInfo";
    public static String EXTRA_CONTENT_OBJECT_STRING = "contentObject";
    public static String EXTRA_THUMBNAIL_PATH = "thumbnailPath";
    public static String EXTRA_NOTIFICATION_ID = "notificationId";

    public static final String EXTRA_SHOW_LIVE_TAB = "EXTRA_SHOW_LIVE_TAB";

    public static final int EXTRA_VALUE_WATCH = 0;
    public static final int EXTRA_VALUE_FAV = 1;
    public static final int EXTRA_VALUE_LIKE = 2;
    public static final int EXTRA_VALUE_RATE = 3;

    public static final String CONTENT_TYPE = "CONTENT_TYPE";
    public static final int TYPE_MOVIES = 1;

    public static String foldername = "veqtaprofile";

    public static String EXTRA_ACKNOWLEDGE_TYPE = "acknowledge_type";
    public static String EXTRA_UPDATE_UNREAD_COUNT = "update_unread_notification_count";

    public static final String PREFS_NAME = "Content_APP";
    public static final String FAVORITES = "Content_Favorite";
    // All Shared Preferences Keys
    public static final String IS_LOGIN = "IsLoggedIn";
    // User name (make variable public to access from outside)
    public static final String KEY_NUMBER = "number";

    public static final String IS_TRIVIA_EXIST = "IsTriviaExist";
}
