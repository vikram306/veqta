package com.multitv.ott.Utils;

import android.content.Context;
import android.text.TextUtils;

import com.multitv.ott.controller.AppController;
import com.multitv.ott.models.home.Home;
import com.multitv.ott.models.live.LiveParent;
import com.multitv.ott.models.recommendeds.Recommended;
import com.multitv.ott.sharedpreference.SharedPreference;

/**
 * Created by Lenovo on 07-02-2017.
 */

public class VersionUtils {

    public static boolean getIsHomeVersionChanged(Context context, Home home, String key) {
        Version version = getSavedVersion(context);
        if (version != null && home != null && home.version != null
                && home.version.dash_version != null
                && !home.version.dash_version.equalsIgnoreCase(version.dash_version)) {
            AppController.getInstance().getCacheManager().put(key, null);

            return true;
        }

        return false;
    }

    private static Version getSavedVersion(Context context) {
        SharedPreference sharedPreference = new SharedPreference();
        String versionApiResponse = sharedPreference.getPreferencesString(context, "VERSION");
        if (!TextUtils.isEmpty(versionApiResponse)) {
            Version version = Json.parse(versionApiResponse, Version.class);
            return version;
        }

        return null;
    }

   /* public static boolean getIsLiveVersionChanged(Context context, LiveParent liveParent, String key) {
        Version version = getSavedVersion(context);
        if (version != null && liveParent != null && liveParent.version != null
                && !liveParent.version.equals(version.live_version)) {
            AppController.getInstance().getCacheManager().put(key, null);

            return true;
        }

        return false;
    }
*/

   /* public static boolean getIsContentVersionChanged(Context context, Home video, String key) {
        Version version = getSavedVersion(context);
        if (version != null && video != null && video.version != null
                && !video.version.equals(version.content_version)) {
            AppController.getInstance().getCacheManager().put(key, null);

            return true;
        }

        return false;
    }
*/


    /*public static boolean getIsContentVersionChanged(Context context, Recommended recommended, String key) {
        Version version = getSavedVersion(context);
        if (version != null && recommended != null && recommended.version != null
                && !recommended.version.equalsIgnoreCase(version.content_version)) {
            AppController.getInstance().getCacheManager().put(key, null);

            return true;
        }*//*

        return false;
    }*/

}
