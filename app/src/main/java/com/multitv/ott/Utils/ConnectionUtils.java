package com.multitv.ott.Utils;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by cyberlinks on 9/3/17.
 */

public class ConnectionUtils {

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }
}
