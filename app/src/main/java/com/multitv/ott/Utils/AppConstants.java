package com.multitv.ott.Utils;

import android.util.Log;

/**
 * Created by cyberlinks on 21/1/17.
 */

public class AppConstants {

    public static final String EXTRA_POSITION = "slider_position";
    String PUSH_STATUS_DELIVERED = "delivered";
    String PUSH_STATUS_SEEN = "seen";
    String PUSH_STATUS_CANCELED = "canceled";
    String PUSH_STATUS_CLICK = "click";
    public static final int CONTENT_TYPE_MULTITV_PLAYER_ACTIVITY = 0;


    String CACHE_TRIVIA_KEY = "Trivia";

    public static final int LOGIN_THROUGH_MULTITV = 0;
    public static final int LOGIN_THROUGH_FB = 1;
    public static final  int LOGIN_THROUGH_GOOGLE = 2;

    //=====================mobile number vaildation ======================================================================
    // ====================================================================================================

    public static boolean isValidMobile(String phone) {
        Log.e("vaildation mob", "isValidMobile: " + phone);
        String regexStr = "^[0-9]$";

        if (!android.util.Patterns.PHONE.matcher(phone).matches()) {
            return false;
        }
        if (phone.length() <= 9) {
            return false;
        }
        if (phone.length() == 14) {
            return phone.startsWith("0091");
        }
        if (phone.length() == 13) {
            return phone.startsWith("+91");
        }
        if (phone.length() == 11) {
            return phone.startsWith("0");
        }
        if (phone.length() > 14) {
            return false;
        }
        return true;
    }
}
