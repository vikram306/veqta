package com.multitv.ott.Utils;

/**
 * Created by Lenovo on 10-02-2017.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;

import com.multitv.ott.activity.MultiTvPlayerActivityLive;


/**
 * Created by root on 3/10/16.
 */
public class PlayerUtils {

    private static long watchedDuration;

    public static void setWatchedDuration(long watchedDuration) {
        PlayerUtils.watchedDuration = watchedDuration;
    }

    public static void startPlayerActivity(final Context activity, final String VIDEO_URL, final String url, final String CONTENT_ID,
                                           final String title, final String duration, final String des, final String CONTENT_TYPE_MULTITV, int handlerCode) {
        Handler handler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {

                switch (msg.what) {

                    case 4:
                        if (CONTENT_TYPE_MULTITV.equalsIgnoreCase("LIVE")) {
                            Intent videoIntent = new Intent(activity, MultiTvPlayerActivityLive.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            videoIntent.putExtra("VIDEO_URL", VIDEO_URL);
                            activity.startActivity(videoIntent);

                        } else {

                        }
                        break;
                    default:
                        break;
                }
                return false;
            }
        });

        handler.sendEmptyMessageDelayed(handlerCode, 1);
    }

}

