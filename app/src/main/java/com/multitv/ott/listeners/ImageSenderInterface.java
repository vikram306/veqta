package com.multitv.ott.listeners;

/**
 * Created by Lenovo on 07-03-2017.
 */

public interface ImageSenderInterface {

    void sendImage(String url);
    void viewPagerPosition(int position);
    void emptyImage(String url);
    void sendUserFiristName(String user_name);
    void sendUserLastName(String first_name,String user_last_name);
    void sendEmailId(String user_email);
    void sendMobileNumber(String user_phone);
    void sendDob(String user_phone);
}