package com.multitv.ott.listeners;

import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;

/**
 * Created by Lenovo on 13-02-2017.
 */

public interface HomeLoadDataInterface {
    void setLiveData(RecyclerView recyclerView,ProgressBar progressBar,LinearLayout textView);
    void setRecommendedData(RecyclerView recyclerView,ProgressBar progressBar,LinearLayout textView);
    void setFeatureBannerData(SliderLayout recyclerView);
    void setHomeCategoryItemData(RecyclerView recyclerView,ProgressBar progressBar,int postion,String cat_id,LinearLayout textView);
}
