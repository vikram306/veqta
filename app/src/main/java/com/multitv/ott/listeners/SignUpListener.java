package com.multitv.ott.listeners;

/**
 * Created by Lenovo on 03-02-2017.
 */

public interface SignUpListener {

    void onSuccess(String s);

    void onError();
}
