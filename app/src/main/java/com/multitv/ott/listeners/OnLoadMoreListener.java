package com.multitv.ott.listeners;

/**
 * Created by Lenovo on 07-02-2017.
 */

public interface OnLoadMoreListener {

    void onLoadMore();
}
