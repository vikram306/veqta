package com.multitv.ott.listeners;

/**
 * Created by cyberlinks on 10/3/17.
 */

public interface OnBackPressedListener {

    void onBackPressed();
}
