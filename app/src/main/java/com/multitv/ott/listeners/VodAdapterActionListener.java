package com.multitv.ott.listeners;

import android.support.v7.widget.RecyclerView;
import android.widget.ProgressBar;

import com.multitv.ott.models.CategoryVod.CategoryContent;
import com.multitv.ott.models.CategoryVod.VideoMain;
import com.multitv.ott.models.categories.Child;
import com.multitv.ott.models.categories.LiveCatData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cyberlinks on 7/3/17.
 */

public interface VodAdapterActionListener {

    void onGetChildList(VideoMain videoMain);

    void onSetContentInContentRecyclerView(RecyclerView recyclerView, ProgressBar progressBar, List<CategoryContent> contentArrayList, String categoryId);
}
