package com.multitv.ott.listeners;

/**
 * Created by Lenovo on 13-02-2017.
 */

public interface CategoryOnLoadMoreListener {
    void onLoadMore();
}
