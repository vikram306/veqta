package com.multitv.ott.listeners;

import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.multitv.ott.models.categories.LiveCatData;
import com.multitv.ott.models.home.Home_category;

import java.util.List;

/**
 * Created by Lenovo on 08-02-2017.
 */

public interface LiveCategoryInterfaces {

    void setLiveCategoryItemData(RecyclerView recyclerView, ProgressBar progressBar, int postion, String cat_id ,LinearLayout linearbg);

    void onGetChildList(LiveCatData liveCat);


}
