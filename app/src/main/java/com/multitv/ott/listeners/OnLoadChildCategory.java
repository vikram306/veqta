package com.multitv.ott.listeners;

import com.multitv.ott.models.CategoryVod.VideoMain;
import com.multitv.ott.models.categories.Child;

import java.util.List;

/**
 * Created by cyberlinks on 9/3/17.
 */

public interface OnLoadChildCategory {

    void onLoadChildCategoryFragment(VideoMain videoMain);
}
