package com.multitv.ott.models;

import java.util.ArrayList;

/**
 * Created by cyberlinks on 18/1/17.
 */

public class HomeData {
    public String cat_id;
    public String cat_name;
    public ArrayList<ContentItem> cat_cntn = new ArrayList<ContentItem>();
}
