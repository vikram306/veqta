
package com.multitv.ott.models.recommendeds;


public class Thumbnail {

    public String large;

    public Thumbnail(String large) {
        this.large=large;
    }

    public String getLarge() {
        return large;
    }

    public void setLarge(String large) {
        this.large = large;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }

    public String getSmall() {
        return small;
    }

    public void setSmall(String small) {
        this.small = small;
    }

    public String medium;
    public String small;


}
