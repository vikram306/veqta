package com.multitv.ott.models.categories;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo on 28-02-2017.
 */

public class VOD implements Serializable {
    public String id;
    public String name;
    public String thumbnail;
    public String index;
    public List<Child> children = new ArrayList<>();
}
