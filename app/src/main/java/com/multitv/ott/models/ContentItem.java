package com.multitv.ott.models;

/**
 * Created by cyberlinks on 17/1/17.
 */

public class ContentItem {

    int _id;
    String name;
    int imageId;


    public ContentItem(int _id, String name, int imageId) {
        this._id = _id;
        this.name = name;
        this.imageId = imageId;
    }

    public int get_id() {
        return _id;
    }

    public String getName() {
        return name;
    }

    public int getImageId() {
        return imageId;
    }
}
