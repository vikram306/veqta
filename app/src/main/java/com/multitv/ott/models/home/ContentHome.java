package com.multitv.ott.models.home;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 24/10/16.
 */
public class ContentHome {

    public String category;
    public String category_id;
    public String index;
    public String id;
    public String title;
    public String des;
    public String language_id;
    public String language;
    public String media_type;
    public String source;
    public String duration;
    public Integer price_type;
    public String url;
    public String likes;
    public Thumbnail thumbnail;
    public List<String> category_ids = new ArrayList<String>();
    public String likes_count;
    public Object rating;
    public String watch;
    public String favorite;
    public String playerUrl;
    //public Meta meta;
    public List<String> keywords = new ArrayList<String>();
    public String social_like;
    public String social_view;
}
