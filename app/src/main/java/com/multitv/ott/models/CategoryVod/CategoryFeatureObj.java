package com.multitv.ott.models.CategoryVod;

import com.multitv.ott.models.home.Meta;
import com.multitv.ott.models.home.Thumbnail;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cyberlinks on 7/3/17.
 */

public class CategoryFeatureObj {

    public String id;
    public String title;
    public String des;
    public String bcid;
    public String language_id;
    public String language;
    public Object genre;
    public String category_id;
    public String media_type;
    public String source;
    public String duration;
    public String price_type;
    public Integer favorite_count;
    public String url;
    public Integer likes;
    public Integer favorite;
    public String playerUrl;
    public Thumbnail thumbnail;
    public boolean drm;
    public Integer likes_count;
    public String rating;
    public String watch;
    public int social_like;
    public int social_view;
    public String episodenumber;
    public Meta meta;
    public List<String> keywords = new ArrayList<>();
    public List<String> category_ids = new ArrayList<>();
}
