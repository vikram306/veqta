package com.multitv.ott.models.CategoryVod;

import com.multitv.ott.models.home.Version;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cyberlinks on 7/3/17.
 */

public class CategoryContentObj {

    public int offset;
    public String version;
    public int totalCount;
    public List<CategoryContent> content = new ArrayList<>();
    //public List<CategoryFeatureObj> feature = new ArrayList<>();
}
