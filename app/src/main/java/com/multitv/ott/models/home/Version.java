
package com.multitv.ott.models.home;


public class Version {

    public String live_version;
    public String dash_version;
    public String epg_version;
    public String conf_version;

}
