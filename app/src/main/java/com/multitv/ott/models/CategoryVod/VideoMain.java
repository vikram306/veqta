package com.multitv.ott.models.CategoryVod;

import com.multitv.ott.models.categories.Child;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by naseeb on 1/30/2017.
 */

public class VideoMain implements Serializable{
    public String id;
    public String title;
    public ArrayList<CategoryContent> contentArrayList = new ArrayList<>();
    public List<Child> children = new ArrayList<>();
}
