package com.multitv.ott.models.categories;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo on 20-02-2017.
 */

public class LiveCategory implements Serializable{

    public String version;
    public List<VOD> vod = new ArrayList<>();
    @SerializedName("live")
    public List<LiveCatData> liveCategoryArrayList = new ArrayList<>();
}
