
package com.multitv.ott.models.home;


import java.io.Serializable;

public class Thumbnail implements Serializable{

    public String large;
    public String medium;
    public String small;

}
