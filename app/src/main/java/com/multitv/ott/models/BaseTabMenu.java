package com.multitv.ott.models;

import android.support.v4.app.Fragment;

import com.multitv.ott.models.categories.VOD;

/**
 * Created by Lenovo on 01-02-2017.
 */

public class BaseTabMenu {

    public String pagerTitle;
    public String thumbnail;
    //public String tabId;
    public Fragment fragment;

    public BaseTabMenu(String pagerTitle, String thumbnail) {
        this.pagerTitle = pagerTitle;
        this.thumbnail = thumbnail;
    }

    public BaseTabMenu() {

    }

    public String getPagerTitle() {
        return pagerTitle;
    }

    public void setPagerTitle(String pagerTitle) {
        this.pagerTitle = pagerTitle;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }
}
