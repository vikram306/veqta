package com.multitv.ott.models.players;

/**
 * Created by Lenovo on 31-01-2017.
 */

public class MorePlayerItems {
    public String title;
    public  String subTitle;
    public String genure;
    public String date;
    public MorePlayerItems(String title,String subTitle,String genure,String date){
        this.title=title;
        this.subTitle=subTitle;
        this.genure=genure;
        this.date=date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getGenure() {
        return genure;
    }

    public void setGenure(String genure) {
        this.genure = genure;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
