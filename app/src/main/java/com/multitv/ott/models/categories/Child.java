package com.multitv.ott.models.categories;

import com.multitv.ott.models.recommendeds.Content;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo on 28-02-2017.
 */

public class Child implements Serializable {

    private static final long serialVersionUID = 1L;

    public String id;
    public String name;
    public String thumbnail;
    public String index;
    //public List<Content> content = new ArrayList<>();
    public List<Child> children = new ArrayList<>();
}
