package com.multitv.ott.models.CategoryVod;

import com.multitv.ott.models.home.Meta;
import com.multitv.ott.models.home.Thumbnail;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by cyberlinks on 7/3/17.
 */

public class CategoryContent implements Serializable{

    public String id;
    public String index;
    public String bcid;
    public String title;
    public String des;
    public String category_id;
    public String language_id;
    public String language;
    public String media_type;
    public String source;
    public String duration;
    public Integer price_type;
    public String social_view;
    public String social_like;
    public String likes_count;
    public String rating;
    public String watch;
    public String favorite_count;
    public String url;
    public String preview_url;
    public List<String> category_ids = new ArrayList<>();
    public String likes;
    public String favorite;
    public String playerUrl;
    public Thumbnail thumbnail;
    public boolean drm;
    public String episodenumber;
    public Meta meta;
    public List<String> keywords = new ArrayList<>();
}
