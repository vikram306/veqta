package com.multitv.ott.models.recommendeds;

import com.multitv.ott.models.home.ContentHome;
import com.multitv.ott.models.home.Version;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo on 09-02-2017.
 */

public class Recommended {
    public Integer offset;
    public int totalCount;
    //public String version;
    public List<Content> content = new ArrayList<>();
}
