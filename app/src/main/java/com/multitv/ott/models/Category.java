package com.multitv.ott.models;

/**
 * Created by cyberlinks on 17/1/17.
 */

public class Category {

    private int _id;
    private String catName;
    int imageId;

    public Category(int _id, String name, int imageId) {
        this._id = _id;
        this.catName = name;
        this.imageId = imageId;
    }

    public int getId() {
        return _id;
    }

    public String getCatName() {
        return catName;
    }

    public int getImageId() {
        return imageId;
    }
}
