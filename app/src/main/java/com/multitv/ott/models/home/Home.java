
package com.multitv.ott.models.home;

import java.util.ArrayList;
import java.util.List;

public class Home {
    public Version version;
    public int display_offset;
    public int display_count;
    public Dashboard dashboard;
    public List<ContentHome> live = new ArrayList<>();
    public List<ContentHome> recomended = new ArrayList<>();
}
