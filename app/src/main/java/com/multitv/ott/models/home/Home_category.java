package com.multitv.ott.models.home;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 24/10/16.
 */
public class Home_category {
    public String cat_id;
    public String cat_name;
    public List<ContentHome> cat_cntn = new ArrayList<>();
}
