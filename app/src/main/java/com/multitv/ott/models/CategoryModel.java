package com.multitv.ott.models;

import java.util.ArrayList;

/**
 * Created by cyberlinks on 18/1/17.
 */

public class CategoryModel {

    public int _id;
    public String name;
    public ArrayList<Category> categoryArrayList;
}
