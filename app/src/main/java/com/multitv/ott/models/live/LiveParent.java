package com.multitv.ott.models.live;

import com.multitv.ott.models.home.ContentHome;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by naseeb on 1/18/2017.
 */

public class LiveParent {
    public String version;
    public int offset;
    public int totalcount;
    public List<ContentHome> live = new ArrayList<>();
}
