package com.multitv.ott.sharedpreference;

/**
 * Created by Lenovo on 03-02-2017.
 */


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.multitv.ott.activity.HomeActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SharedPreference {
    // Shared Preferences
    SharedPreferences pref;
    public String PREFS_NAME="Veqta";
    public  String IS_LOGIN = "IsLoggedIn";

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;


    public final String KEY_NOTIFICATION_SET = "unread_notifications_set";
    public final String KEY_IS_LOGGED_IN = "is_logged_in";
    public final String KEY_IS_OTP_VERIFIED = "is_otp_verified";
    //public final String KEY_OTP_CODE = "otp_code";

    public SharedPreference() {
        super();
    }

    public Set<String> getUnreadNotificationsList(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        Set<String> set = new HashSet<String>(prefs.getStringSet(key, new HashSet<String>()));
        return set;
    }

    public void setUnreadNotificationsList(Context context, String key, Set<String> value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putStringSet(key, value);
        editor.commit();
    }


    public void setPreferencesString(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreferencesString(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public boolean getPreferenceBoolean(Context _context, String key) {
        if (_context == null)
            return false;
        SharedPreferences prefs = _context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        boolean b = prefs.getBoolean(key, false);
        return b;
    }

    public void setPreferenceBoolean(Context _context, String key) {
        if (_context == null)
            return;
        SharedPreferences.Editor editor = _context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putBoolean(key, true);
        editor.commit();
    }

    public void setPreferencesLikes(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreferencesLikes(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }


    public void setPreferencesLikes_Count(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreferencesLikes_Count(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }


    public void setPreferencesInt(Context context, String key, int value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public int getPreferencesInt(Context context, String key) {
        if (context == null)
            return 0;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int value = prefs.getInt(key, 0);
        return value;
    }

    public void setPreferencesLong(Context context, String key, long value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public long getPreferencesLong(Context context, String key) {
        if (context == null)
            return 0;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        long value = prefs.getLong(key, 0);
        return value;
    }

    // This four methods are used for maintaining favorites.


    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            // Closing all the Activities
            // Add new Flag to start new Activity
            // Staring Login Activity
            _context.startActivity(new Intent(_context, HomeActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        }

    }

    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }




    public String getprefDatw(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setperfDate(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getEmailID(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setEmailId(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getFirstName(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setFirstName(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }


    public String getLastName(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setLastName(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }


    public String getpassword(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setPassword(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }


    public String getImageUrl(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }





    public void setImageUrl(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getUSerName(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setUserName(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getUSerLastName(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setUserLastName(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPhoneNumber(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setPhoneNumber(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getDob(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setDob(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getGender(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setGender(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }


    public String getAbout(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setAbout(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }


    public String getGenderId(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setGenderId(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }


    public String getStatusLogin(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setStatusLogin(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }


    public String getFromLogedIn(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setFromLogedIn(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }


    public String getGoogleLoginEmail(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setGoogleLoginEmail(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getGoogleLoginUSername(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setGoogleLoginUsername(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }


    public String getGoogleLoginLastName(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setGoogleLoginLastName(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }


    public String getGoogleLoginProfilePic(Context context, String key) {
        if (context == null)
            return null;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }

    public void setGoogleLoginProfilePic(Context context, String key, String value) {
        if (context == null)
            return;
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

}
