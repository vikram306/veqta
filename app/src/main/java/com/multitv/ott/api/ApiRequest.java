package com.multitv.ott.api;

/**
 * Created by Lenovo on 03-02-2017.
 */

public class ApiRequest {
    public static String TOKEN = "584ab077cc9f8";
   // public static String BASE_URL="http://automator.multitvsolution.com/api/v2";
    public static String BASE_URL="http://dev.multitvsolution.com/automator/api/v2";
    public static String LOGIN_URL1 = BASE_URL+"/user/social/token/" + TOKEN;
    public static String FORGET_URL = BASE_URL+"/user/forgot/token/" + TOKEN;
    public static String SIGNUP_URL = BASE_URL+"/user/add/token/" + TOKEN;
    public static String LOGIN_URL = BASE_URL+"/user/login/token/" + TOKEN;
    public static String VERIFY_OTP = BASE_URL+"/user/verify_otp/token/" + TOKEN;
    public static String HOME_URL = BASE_URL+"/content/home/token/" + TOKEN;
    public static String VERSION_URL = BASE_URL+"/content/version/token/" + TOKEN;

    public static String RECOMMENDED_LIST = BASE_URL+"/content/recomended/token/" + TOKEN;
    public static String VIDEO_CAT_URL_CLIST = BASE_URL+"/content/clist/token/" + TOKEN;
    public static String WATCHING_SET_DATA_URL = BASE_URL+"/content/watchDurationSubs/token/" + TOKEN;
    public static String LIVE_CHANNEL = BASE_URL+"/content/getLive/token/" + TOKEN;
 public static String USER_EDIT = BASE_URL+"/user/edit/token/" + TOKEN;
    //---for live and vod category  ----
    public static String LIVE_CATEGORY = BASE_URL+"/content/catList/token/" + TOKEN;
    public static String LIKES_USER_CONTENT = BASE_URL+"/content/userrelated_content/token/" + TOKEN;

    //-------like api---------------
    public static String LIKE_URL_Post = BASE_URL+"/content/like/token/" + TOKEN;
    public static String LIKE_URL = BASE_URL+"/content/rating_likes/token/" + TOKEN;
    public static String FAVORITE_URL = BASE_URL+"/content/favorite/token/" + TOKEN;
}
