package com.multitv.ott.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.multitv.ott.R;

/**
 * Created by Lenovo on 30-01-2017.
 */

public class HelpFragment extends Fragment {

    private Button btn_help;


    public HelpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_help, container, false);
        btn_help=(Button)view.findViewById(R.id.btn_rate);
        btn_help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"Coming Soon...",Toast.LENGTH_LONG).show();
            }
        });
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/app_customfonts.ttf");
        btn_help.setTypeface(tf);
        return view;
    }


}