package com.multitv.ott.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.gson.reflect.TypeToken;
import com.iainconnor.objectcache.GetCallback;
import com.iainconnor.objectcache.PutCallback;
import com.multitv.cipher.MultitvCipher;
import com.multitv.ott.R;
import com.multitv.ott.Utils.Json;
import com.multitv.ott.Utils.PlayerUtils;
import com.multitv.ott.Utils.RecyclerItemClickListener;
import com.multitv.ott.Utils.VersionUtils;

import com.multitv.ott.activity.VeqtaPlayerActivity;
import com.multitv.ott.adapter.HomeDisplayItemWithHeaderAdapter;
import com.multitv.ott.adapter.LiveHomeChannelAdapter;
import com.multitv.ott.adapter.RecommendedHomeAdapter;
import com.multitv.ott.api.ApiRequest;
import com.multitv.ott.controller.AppController;
import com.multitv.ott.listeners.CategoryOnLoadMoreListener;
import com.multitv.ott.listeners.HomeLoadDataInterface;
import com.multitv.ott.listeners.OnLoadMoreListener;
import com.multitv.ott.adapter.HomeAllDataListAdapter;
import com.multitv.ott.models.home.ContentHome;
import com.multitv.ott.models.home.Home;
import com.multitv.ott.models.home.Home_category;
import com.multitv.ott.models.live.LiveParent;
import com.multitv.ott.models.recommendeds.Content;
import com.multitv.ott.models.recommendeds.Recommended;
import com.multitv.ott.sharedpreference.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by cyberlinks on 17/1/17.
 */

public class HomeFragment extends Fragment implements HomeLoadDataInterface, BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.mProgress_bar_top)
    ProgressBar progressBarTop;
    @BindView(R.id.progress_bar_bottom)
    ProgressBar progressBarBottom;
    int currentPageHomeCategory = 0, currentPageRecommended = 0, currentPageLive = 0, NUM_PAGES = 0;
    private boolean isHomeDataFetched;
    private String TAG = "HomeFragment";
    private int offset, offsetLive;
    private int itemsLeftCategory = -1, startCountCategory = 0, endCountCategory = 0, countCategory = 0, current_pageCategory = 0;
    private int itemsLeftLive = -1, startCountLive = 0, endCountLive = 0, countLive = 0;
    private int itemsLeftRecommended = -1, startCountRecommended = 0, endCountRecommended = 0, countRecommended = 0;
    private int itemsLeftCatItem = -1, startCountCatItem = 0, endCountCatItem = 0, countCatItem = 0, current_pageDis_cat = 0;
    private boolean hasSearch_focus = false;
    private String user_id;
    private SharedPreference sharedPreference;
    //----object of home,recommended,live------
    private int changedContentType = 0;
    private Recommended recommended, recommendedSaved;
    private Recommended videoDataSaved, recommended1;
    private LiveParent liveParent, liveParentSaved;
    public Home home, homeDataSaved;

    private boolean isNeedToSetVersionInApi, isDataUpdateInProgress;

    private List<Home_category> displayCategoryList = new ArrayList<>();

    private List<Content> recommendArrayList = new ArrayList<>();
    private List<ContentHome> liveArrayList = new ArrayList<>();
    private List<ContentHome> featureBannerlList = new ArrayList<>();
    private List<Content> dispalyCategoryItemArrayList = new ArrayList<>();

    private HomeAllDataListAdapter homeAllDataCategoryListAdapter;
    private RecommendedHomeAdapter recommendedAdapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, rootview);
        sharedPreference = new SharedPreference();
        user_id = new SharedPreference().getPreferencesString(getActivity(), "user_id" + "_" + ApiRequest.TOKEN);
        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        if (getActivity() == null)
            return;
        if (isNetworkConnected()) {
            getVersionData();
        } else {
            setHomeAllData();
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    private void initializeFeatureBanner(Home home) {
        if (homeAllDataCategoryListAdapter != null && homeAllDataCategoryListAdapter.featureBannerSlider != null) {
            if (home != null && home.dashboard != null
                    && home.dashboard.feature_banner != null && home.dashboard.feature_banner.size() != 0) {
                Log.e(TAG, "feature_banner: FEATURE BANNER SIZE " + home.dashboard.feature_banner.size());
                for (int i = 0; i < home.dashboard.feature_banner.size(); i++) {
                    if (getActivity() == null)
                        return;
                    TextSliderView textSliderView = new TextSliderView(getActivity());
                    String url = home.dashboard.feature_banner.get(i).thumbnail.medium;
                    textSliderView.bundle(new Bundle());
                    textSliderView.getBundle()
                            .putString("video_url", home.dashboard.feature_banner.get(i).url);
                    textSliderView.getBundle()
                            .putString("title", home.dashboard.feature_banner.get(i).title);
                    textSliderView.getBundle()
                            .putString("des", home.dashboard.feature_banner.get(i).des);

                    textSliderView.getBundle()
                            .putString("type", home.dashboard.feature_banner.get(i).media_type);
                    textSliderView.getBundle()
                            .putString("thumbnail", home.dashboard.feature_banner.get(i).thumbnail.large);
                    textSliderView.getBundle()
                            .putString("content_type", home.dashboard.feature_banner.get(i).source);

                    textSliderView.getBundle()
                            .putString("fav_item", home.dashboard.feature_banner.get(i).favorite.toString());

                    textSliderView.getBundle()
                            .putString("position", String.valueOf(i));
                    textSliderView.getBundle()
                            .putString("video_id", home.dashboard.feature_banner.get(i).id);

                    textSliderView.getBundle()
                            .putString("like", String.valueOf(home.dashboard.feature_banner.get(i).likes));

                  /*  textSliderView.getBundle()
                            .putInt("social_like", home.dashboard.feature_banner.get(i).social_like);

                    textSliderView.getBundle()
                            .putInt("social_view", home.dashboard.feature_banner.get(i).social_view);*/

                  /*  if (home.dashboard.feature_banner.get(i).meta.star_cast != null && !home.dashboard.feature_banner.get(i).meta.star_cast.equals("null") && home.dashboard.feature_banner.get(i).meta.star_cast.isEmpty()) {
                        textSliderView.getBundle()
                                .putString("start_cast", String.valueOf(home.dashboard.feature_banner.get(i).meta.star_cast));
                    }*/
                    if (url != null && !url.equals(null) && !url.equals("")) {
                        textSliderView
                                .description(home.dashboard.feature_banner.get(i).title)
                                .image(url)
                                .setScaleType(BaseSliderView.ScaleType.Fit)
                                .setOnSliderClickListener(this);
                    } else {
                        textSliderView
                                .description(home.dashboard.feature_banner.get(i).title)
                                .image(R.mipmap.place_holder)
                                .setScaleType(BaseSliderView.ScaleType.Fit)
                                .setOnSliderClickListener(this);

                    }

                    homeAllDataCategoryListAdapter.featureBannerSlider.addSlider(textSliderView);

                }
                homeAllDataCategoryListAdapter.featureBannerSlider.setVisibility(View.VISIBLE);
            } else
                homeAllDataCategoryListAdapter.featureBannerSlider.setVisibility(View.GONE);
        }
    }

    @Override
    public void setFeatureBannerData(SliderLayout sliderLayout) {
        //feature_banner(sliderLayout);
    }


    //========================================get display category data data======================================================
    //============================================================================================================================
    private void homesDisplayCatData(boolean isFromApiResponse, Home home) {
        if (isFromApiResponse) {
            if (home != null && home.dashboard != null
                    && home.dashboard.home_category != null && home.dashboard.home_category.size() != 0) {
                displayCategoryList.addAll(home.dashboard.home_category);
                //homeAllDataCategoryListAdapter.notifyDataSetChanged();
            }
        }

        if (displayCategoryList != null && displayCategoryList.size() != 0) {
            homeAllDataCategoryListAdapter.notifyDataSetChanged();
            homeAllDataCategoryListAdapter.setLoaded();
        }

    }


    //========================================get home data==================================================================
    //============================================================================================================================
    public void getHomeData(final boolean isGettingHomeCategoryDataOnly) {
        Log.e(TAG + "Home", "getHomeData-method");
        Log.e("HomeUrl", ApiRequest.HOME_URL);

        if (getActivity() == null)
            return;

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isGettingHomeCategoryDataOnly && progressBarTop.getVisibility() != View.VISIBLE)
                    progressBarTop.setVisibility(View.VISIBLE);
                else if (isGettingHomeCategoryDataOnly)
                    progressBarBottom.setVisibility(View.VISIBLE);
            }
        });

        final String key = "Home";
        final Type homeObjectType = new TypeToken<Home>() {
        }.getType();
        AppController.getInstance().getCacheManager().getAsync(key, Home.class, homeObjectType, new GetCallback() {
            @Override
            public void onSuccess(final Object object) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        homeDataSaved = (Home) object;

                        if (getActivity() == null)
                            return;

                        if (!isDataUpdateInProgress) {
                            boolean isHomeDashBoardVersionChanged = VersionUtils.getIsHomeVersionChanged(getActivity(), homeDataSaved, key);
                            if (isHomeDashBoardVersionChanged) { //It means home data update is available
                                changedContentType = 2;
                                isNeedToSetVersionInApi = true;
                                updateData(key, false);
                            }
                        }

                        if (!isGettingHomeCategoryDataOnly && homeDataSaved != null && homeDataSaved.dashboard != null
                                && homeDataSaved.dashboard.feature_banner.size() != 0
                                && homeDataSaved.dashboard.home_category != null && homeDataSaved.dashboard.home_category.size() != 0
                            //&& homeDataSaved.live != null && homeDataSaved.live.size() != 0
                            //&& homeDataSaved.recomended != null && homeDataSaved.recomended.size() != 0
                                ) {
                            Log.e("CacheManager", key + " object retrieved successfully");

                            home = homeDataSaved;
                            if (getActivity() == null)
                                return;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    initializeFeatureBanner(home);
                                    progressBarTop.setVisibility(View.GONE);
                                }
                            });

                        }

                        if (homeDataSaved != null && homeDataSaved.dashboard != null
                                && homeDataSaved.dashboard.home_category != null && homeDataSaved.dashboard.home_category.size() != 0
                                && ((itemsLeftCategory == -1 && displayCategoryList.size() == 0) || itemsLeftCategory > 0)) {
                            Log.e("CacheManager", key + " object retrieved successfully");

                            home = homeDataSaved;

                            if (displayCategoryList.size() < home.dashboard.home_category.size()) {
                                if (itemsLeftCategory == -1) {
                                    displayCategoryList.clear();
                                    itemsLeftCategory = home.dashboard.home_category.size();
                                }

                                if (itemsLeftCategory == home.dashboard.home_category.size())
                                    startCountCategory = 0;
                                else
                                    startCountCategory = startCountCategory + 3;

                                endCountCategory = startCountCategory + 3;
                                if (endCountCategory > home.dashboard.home_category.size())
                                    endCountCategory = home.dashboard.home_category.size();

                                for (int i = startCountCategory; i < endCountCategory; i++) {
                                    displayCategoryList.add(home.dashboard.home_category.get(i));
                                }

                                itemsLeftCategory = itemsLeftCategory - 3;
                                if (itemsLeftCategory < 0)
                                    itemsLeftCategory = 0;

                                if (getActivity() == null)
                                    return;
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        homesDisplayCatData(false, home);
                                    }
                                });
                            }

                            if (getActivity() == null)
                                return;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (isGettingHomeCategoryDataOnly)
                                        progressBarBottom.setVisibility(View.GONE);
                                }
                            });
                            return;
                        } else if (!isNetworkConnected()) {
                            Log.e(TAG, "getHomeData().run: ConnectionManager Not Connected");

                            if (getActivity() == null)
                                return;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!isGettingHomeCategoryDataOnly && isHomeDataFetched)
                                        progressBarTop.setVisibility(View.GONE);
                                    else if (isGettingHomeCategoryDataOnly)
                                        progressBarBottom.setVisibility(View.GONE);
                                }
                            });
                        } else if (homeDataSaved == null || offset < homeDataSaved.display_count) {
                            StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                                    ApiRequest.HOME_URL, new Response.Listener<String>() {
                                @Override
                                public void onResponse(final String response) {
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            try {
                                                JSONObject mObj = new JSONObject(response);
                                                if (mObj.optInt("code") == 1) {
                                                    MultitvCipher mcipher = new MultitvCipher();
                                                    String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                                    Log.e("Home_api_response", str);
                                                    home = Json.parse(str.trim(), Home.class);

                                                    offset = home.display_offset;
                                                    if (getActivity() == null)
                                                        return;
                                                    sharedPreference.setPreferencesString(getActivity(), "offset_" + "_home_category", "" + offset);

                                                    if (!isGettingHomeCategoryDataOnly) {
                                                        AppController.getInstance().getCacheManager().put(key, home);
                                                    } else if (homeDataSaved != null && homeDataSaved.dashboard != null
                                                            && home != null && home.dashboard != null
                                                            && home.dashboard.home_category != null && home.dashboard.home_category.size() != 0) {
                                                        homeDataSaved.display_count = home.display_count;
                                                        homeDataSaved.display_offset = home.display_offset;
                                                        homeDataSaved.dashboard.home_category.addAll(home.dashboard.home_category);
                                                        AppController.getInstance().getCacheManager().put(key, homeDataSaved);
                                                        Log.e("**Home_Only_Category: ", "save successfully");
                                                    }

                                                    if (getActivity() == null)
                                                        return;
                                                    getActivity().runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            if (!isGettingHomeCategoryDataOnly) {
                                                                initializeFeatureBanner(home);
                                                                //recommendedShowData(home.recomended);
                                                                //liveShowData(home.live);
                                                            }

                                                            homesDisplayCatData(true, home);

                                                            if (!isNeedToSetVersionInApi)
                                                                isHomeDataFetched = true;

                                                            if (!isGettingHomeCategoryDataOnly && isHomeDataFetched)
                                                                progressBarTop.setVisibility(View.GONE);
                                                            else if (isGettingHomeCategoryDataOnly)
                                                                progressBarBottom.setVisibility(View.GONE);
                                                        }
                                                    });
                                                }
                                            } catch (Exception e) {
                                                Log.e("code==0", "" + e.getMessage());

                                                isHomeDataFetched = true;

                                                if (getActivity() == null)
                                                    return;
                                                getActivity().runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (!isGettingHomeCategoryDataOnly && isHomeDataFetched)
                                                            progressBarTop.setVisibility(View.GONE);
                                                        else if (isGettingHomeCategoryDataOnly)
                                                            progressBarBottom.setVisibility(View.GONE);
                                                    }
                                                });
                                            }
                                        }
                                    }).start();
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e("HomeFragment", "Error: " + error.getMessage());

                                    isHomeDataFetched = true;

                                    if (!isGettingHomeCategoryDataOnly && isHomeDataFetched)
                                        progressBarTop.setVisibility(View.GONE);
                                    else if (isGettingHomeCategoryDataOnly)
                                        progressBarBottom.setVisibility(View.GONE);
                                }
                            }) {

                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<>();

                                    //params.put("lan", LocaleHelper.getLanguage(getApplicationContext()));
                                    // params.put("m_filter", (PreferenceData.isMatureFilterEnable(getApplicationContext()) ? "" + 1 : "" + 0));

                                    params.put("device", "android");
                                    params.put("content_count", "5");
                                    params.put("display_offset", "" + offset);
                                    params.put("display_limit", "3");
                                    params.put("user_id", user_id);
                                    params.put("live_limit", "5");
                                    return params;
                                }
                            };

                            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 3,
                                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                            // Adding request to request queue
                            AppController.getInstance().addToRequestQueue(jsonObjReq);
                        } else {
                            if (getActivity() == null)
                                return;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressBarTop.setVisibility(View.GONE);
                                    progressBarBottom.setVisibility(View.GONE);
                                }
                            });
                        }
                    }
                }).start();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("Cache-fails", "" + e.getMessage());
            }
        });
    }

    //========================================update data from cache====================================================
    //==========================================================================================================
    private void updateData(final String key, final boolean isUpdatingLiveData) {
        if (isDataUpdateInProgress)
            return;
        isDataUpdateInProgress = true;

        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                ApiRequest.HOME_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("Home-update-response", response.toString());
                        try {
                            JSONObject mObj = new JSONObject(response);
                            if (mObj.optInt("code") == 1) {
                                MultitvCipher mcipher = new MultitvCipher();
                                String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                JSONObject newObj = new JSONObject(str);
                                final Home home = Json.parse(newObj.toString(), Home.class);
                                Log.e("Home-update-response", newObj.toString());

                                if (homeDataSaved != null && homeDataSaved.dashboard != null) {
                                    offset = home.display_offset;
                                    if (getActivity() == null)
                                        return;
                                    sharedPreference.setPreferencesString(getActivity(), "offset_" + "_home_category", "" + offset);

                                    homeDataSaved.version = home.version;
                                    homeDataSaved.dashboard = home.dashboard;

                                    AppController.getInstance().getCacheManager().put(key, homeDataSaved);

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            //feature_banner();
                                            displayCategoryList.clear();
                                            homesDisplayCatData(true, home);

                                            isDataUpdateInProgress = false;

                                            Log.e(TAG, "Home dashboard data update is completed.");
                                        }
                                    });
                                }
                            }
                            /*}*/
                        } catch (Exception error) {
                            isDataUpdateInProgress = false;
                            Log.e("Error", "Error: " + error.getMessage());
                        }
                    }
                }).start();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", "Error: " + error.getMessage());

                isDataUpdateInProgress = false;
                isHomeDataFetched = true;

                if (isHomeDataFetched)
                    progressBarTop.setVisibility(View.GONE);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                if (getActivity() == null)
                    return null;
                // params.put("lan", LocaleHelper.getLanguage(getActivity()));
                // params.put("m_filter", (PreferenceData.isMatureFilterEnable(getActivity()) ? "" + 1 : "" + 0));

                if (homeDataSaved != null && homeDataSaved.version != null)
                    params.put("current_live_version", homeDataSaved.version.live_version);

                if (homeDataSaved != null && homeDataSaved.version != null)
                    params.put("current_dash_version", homeDataSaved.version.dash_version);

                params.put("device", "android");
                if (isUpdatingLiveData)
                    params.put("content_count", "0");
                else
                    params.put("content_count", "5");

                params.put("display_offset", "0");
                params.put("display_limit", "3");
                params.put("live_limit", "5");
                params.put("user_id", user_id);

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }


    //========================================get version data==================================================================
    //============================================================================================================================

    private void getVersionData() {
        Log.e("getVersionData-Method", "getVersionData");
        progressBarTop.setVisibility(View.VISIBLE);
        if (getActivity() == null)
            return;
        Log.e("getVersionData-Method", "getVersionData-request");
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                ApiRequest.VERSION_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            JSONObject mObj = new JSONObject(response);
                            if (mObj.optInt("code") == 1) {
                                MultitvCipher mcipher = new MultitvCipher();
                                String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                Log.e("Version_api_response", str);

                                handleDataVersions(str);
                            }
                        } catch (Exception e) {
                            Log.e("HomeFragment", "Error:json " + e.getMessage());
                        }

                        setHomeAllData();
                    }
                }).start();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("HomeFragment", "Error: " + error.getMessage());

                setHomeAllData();

                if (isHomeDataFetched)
                    progressBarTop.setVisibility(View.GONE);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                //params.put("lan", LocaleHelper.getLanguage(getApplicationContext()));
                // params.put("m_filter", (PreferenceData.isMatureFilterEnable(getApplicationContext()) ? "" + 1 : "" + 0));

                params.put("device", "android");
                params.put("user_id", user_id);

                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    private void handleDataVersions(final String versionApiResponse) {
        SharedPreference sharedPreference = new SharedPreference();
        if (getActivity() == null)
            return;
        sharedPreference.setPreferencesString(getActivity(), "VERSION", versionApiResponse.trim());
    }


    private void setHomeAllData() {
        homeAllDataCategoryListAdapter = new HomeAllDataListAdapter(getActivity(), displayCategoryList, this, recyclerView);
        homeAllDataCategoryListAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        current_pageCategory = current_pageCategory + 1;
                        getHomeData(true);
                        Log.e("****HomeCategory***", "LoadMoreCalling...");
                    }
                }).start();
            }
        });

        if (getActivity() == null)
            return;

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setAdapter(homeAllDataCategoryListAdapter);
                setItemClickAndStartPlayerActivity();
            }
        });

        if (getActivity() == null)
            return;
        String offsetString = sharedPreference.getPreferencesString(getActivity(), "offset_" + "_home_category");
        if (!TextUtils.isEmpty(offsetString))
            offset = Integer.parseInt(offsetString);

        getHomeData(false);
    }

    private void setItemClickAndStartPlayerActivity() {
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                    }
                })
        );
    }


    @Override
    public void setLiveData(final RecyclerView recyclerView, final ProgressBar progressBar, final LinearLayout title) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        final LiveHomeChannelAdapter liveChannelAdapter = new LiveHomeChannelAdapter(getActivity(), liveArrayList, recyclerView);
        recyclerView.setAdapter(liveChannelAdapter);
        initializeLiveChannelViews(recyclerView);
        if (liveArrayList != null && liveArrayList.size() != 0) {
            title.setVisibility(View.VISIBLE);
        } else {
            title.setVisibility(View.GONE);
        }
        liveChannelAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                currentPageLive = currentPageLive + 1;
                Log.e("home-live data", "" + currentPageLive);
                countLive = liveParent.offset;
                getLiveChannelData(currentPageLive, progressBar, liveChannelAdapter);
            }
        });
        getLiveChannelData(currentPageLive, progressBar, liveChannelAdapter);

    }

    @Override
    public void setRecommendedData(RecyclerView recyclerView, final ProgressBar progressBar, LinearLayout title) {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        recommendedAdapter = new RecommendedHomeAdapter(getActivity(), recommendArrayList, recyclerView);
        recyclerView.setAdapter(recommendedAdapter);
        if (recommendArrayList != null && recommendArrayList.size() != 0) {
            title.setVisibility(View.VISIBLE);
        } else {
            title.setVisibility(View.GONE);
        }
        String offsetString = sharedPreference.getPreferencesString(getActivity(), "offset_" + "Home_ Recommended");
        if (!TextUtils.isEmpty(offsetString))
            countRecommended = Integer.parseInt(offsetString);

        getRecommended(currentPageRecommended, progressBar);
        recommendedAdapter.setOnLoadMoreListener(new CategoryOnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                currentPageRecommended = currentPageRecommended + 1;
                Log.e("more-recommended data", "" + currentPageRecommended);
                countRecommended = recommended.offset;

                if (!hasSearch_focus) {
                    getRecommended(currentPageRecommended, progressBar);
                }
            }
        });
    }


    @Override
    public void setHomeCategoryItemData(final RecyclerView recyclerView, final ProgressBar progressBar, final int position, final String cat_id, LinearLayout title) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        String offsetString = sharedPreference.getPreferencesString(getActivity(), "offset_" + "home_Cat_Category" + cat_id);

        if (!TextUtils.isEmpty(offsetString))
            countCatItem = Integer.parseInt(offsetString);

        final HomeDisplayItemWithHeaderAdapter homeDisplayItemWithHeaderAdapter = new HomeDisplayItemWithHeaderAdapter(getActivity(), dispalyCategoryItemArrayList, recyclerView);
        recyclerView.setAdapter(homeDisplayItemWithHeaderAdapter);

        if (dispalyCategoryItemArrayList != null && dispalyCategoryItemArrayList.size() != 0) {
            title.setVisibility(View.VISIBLE);
        } else {
            title.setVisibility(View.GONE);
        }
        /*Log.e("dispaly-cat-id", "_:_" + cat_id);*/
        homeDisplayItemWithHeaderAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                current_pageDis_cat = current_pageDis_cat + 1;
                Log.e("MORE-CAT-ITEMS", "_LOAD-MORE" + current_pageDis_cat);
                countCatItem = recommended1.offset;
                if (!hasSearch_focus)
                    getHomeCatData(cat_id, current_pageDis_cat, progressBar, homeDisplayItemWithHeaderAdapter);
            }
        });
        getHomeCatData(cat_id, current_pageDis_cat, progressBar, homeDisplayItemWithHeaderAdapter);

    }

    //-------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------recommended recyclerview show data-----------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------

    private void getRecommended(final int currentPageRecommended, final ProgressBar adapterProgressbar) {
        /*if (!ConnectionManager.getInstance(MoreDataActivity.this).isConnected()) {
            return;
        }*/
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                return;
            }
        });
        if (currentPageRecommended > 0) {
            adapterProgressbar.setIndeterminate(true);
            adapterProgressbar.setVisibility(View.VISIBLE);
        } else {

        }


        final String key = "Home" + "_" + "Recommended";
        final Type objectType = new TypeToken<Recommended>() {
        }.getType();
        AppController.getInstance().getCacheManager().getAsync(key, Recommended.class, objectType, new GetCallback() {
            @Override
            public void onSuccess(Object object) {
                recommendedSaved = (Recommended) object;

                if (recommendedSaved != null && recommendedSaved.content != null && recommendedSaved.content.size() != 0 &&
                        (countRecommended <= recommendedSaved.content.size() || countRecommended > recommendedSaved.totalCount)
                        && ((itemsLeftRecommended == -1 && recommendArrayList.size() == 0) || itemsLeftRecommended > 0)) {
                    Log.e("CacheManager", key + " object retrieved successfully");

                    recommended = recommendedSaved;

                    if (currentPageRecommended > 0) {
                        adapterProgressbar.setVisibility(View.GONE);
                    } else {

                    }


                    if (recommendArrayList.size() < recommendedSaved.content.size()) {
                        if (itemsLeftRecommended == -1) {
                            recommendArrayList.clear();
                            itemsLeftRecommended = recommendedSaved.content.size();
                        }

                        if (itemsLeftRecommended == recommendedSaved.content.size())
                            startCountRecommended = 0;
                        else
                            startCountRecommended = startCountRecommended + 5;

                        endCountRecommended = startCountRecommended + 5;
                        if (endCountRecommended > recommendedSaved.content.size())
                            endCountRecommended = recommendedSaved.content.size();

                        for (int i = startCountRecommended; i < endCountRecommended; i++) {
                            recommendArrayList.add(recommendedSaved.content.get(i));
                        }

                        itemsLeftRecommended = itemsLeftRecommended - 5;

                        handleRecommendedPagination(false);
                    }
                }/* else if (!ConnectionManager.getInstance(MoreDataActivity.this).isConnected()) {
                    Log.e(TAG, "ConnectionManager Not Connected");
                }*/ else if (recommendedSaved == null || countRecommended <= recommendedSaved.totalCount) {
                    Log.e("Recommended Url", ApiRequest.RECOMMENDED_LIST);
                    StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                            ApiRequest.RECOMMENDED_LIST, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            Log.e("Recommended_responce---", response.toString());

                            if (currentPageRecommended > 0) {
                                adapterProgressbar.setVisibility(View.GONE);
                            } else {

                            }


                            try {
                                JSONObject mObj = new JSONObject(response);
                                if (mObj.optInt("code") == 1) {
                                    MultitvCipher mcipher = new MultitvCipher();
                                    String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                    Log.e("Recommended response---", str);
                                    try {
                                        JSONObject newObj = new JSONObject(str);
                                        recommended = Json.parse(newObj.toString(), Recommended.class);

                                        sharedPreference.setPreferencesString(getActivity(), "offset_" + "Home_ Recommended", "" + recommended.offset);

                                        if (recommendedSaved != null) {
                                            recommendedSaved.offset = recommended.offset;
                                            recommendedSaved.content.addAll(recommended.content);
                                            AppController.getInstance().getCacheManager().putAsync(key, recommendedSaved, new PutCallback() {
                                                @Override
                                                public void onSuccess() {
                                                    Log.e("CacheManager", key + " object saved successfully");
                                                }

                                                @Override
                                                public void onFailure(Exception e) {
                                                    try {
                                                        Log.e("RecommendedMore", e.getMessage());
                                                    } catch (Exception e1) {

                                                    }

                                                }
                                            });
                                        } else {
                                            AppController.getInstance().getCacheManager().putAsync(key, recommended, new PutCallback() {
                                                @Override
                                                public void onSuccess() {
                                                    Log.e("CacheManager", key + " object saved successfully");
                                                }

                                                @Override
                                                public void onFailure(Exception e) {
                                                    Log.e("Recommended data fail", "Fialure" + e.getMessage());
                                                }
                                            });
                                        }
                                    } catch (JSONException e) {
                                        Log.e("Recommended", "JSONException" + e.getMessage());
                                    }
                                    handleRecommendedPagination(true);
                                }

                            } catch (Exception e) {
                                Log.e("Recommended", "Exception" + e.getMessage());
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError e) {
                            Log.e("Recommended", "VolleyError" + e.getMessage());

                            if (currentPageRecommended > 0) {
                                adapterProgressbar.setVisibility(View.GONE);
                            } else {

                            }

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();

                            //params.put("lan", LocaleHelper.getLanguage(MoreDataActivity.this));
                            //params.put("m_filter", (PreferenceData.isMatureFilterEnable(MoreDataActivity.this) ? "" + 1 : "" + 0));

                            params.put("device", "android");
                            params.put("user_id", user_id);
                            params.put("current_offset", String.valueOf(countRecommended));
                            params.put("max_counter", "5");

                            Log.e("**Recommende+count**", "" + countRecommended);
                            return params;
                        }
                    };

                    // Adding request to request queue
                    AppController.getInstance().addToRequestQueue(jsonObjReq);
                } else {
                    if (currentPageRecommended > 0) {
                        adapterProgressbar.setVisibility(View.GONE);
                    } else {

                    }

                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void handleRecommendedPagination(boolean isFromApiRequest) {
        refreshListRecemended(recommended.content);

        if (isFromApiRequest) {
            if (recommended != null && recommended.content != null && recommended.content.size() != 0) {
                if (recommendArrayList.size() == 0)
                    recommendArrayList.addAll(recommended.content);
                else {
                    for (int i = 0; i < recommendArrayList.size(); i++) {
                        String catId = recommendArrayList.get(i).id;
                        for (int j = 0; j < recommended.content.size(); j++) {
                            if (catId.equals(recommended.content.get(j).id)) {
                                recommended.content.remove(j);
                            }
                        }
                    }
                    if (recommended.content.size() > 0)
                        recommendArrayList.addAll(recommended.content);
                    Log.e("***RecommendedSIZE-1***", "" + recommendArrayList.size());
                    Log.e("***RecommendedSIZE-2***", "" + recommended.content.size());
                }
            }
        }

        if (recommendArrayList != null && recommendArrayList.size() != 0) {
            recommendedAdapter.setLoaded();
            recommendedAdapter.notifyDataSetChanged();
        }
    }

    private void refreshListRecemended(List<Content> recommendArrayList) {

        if (recommendArrayList.size() > 0) {
            int lastIndex = recommendArrayList.size() - 1;
            if (recommendArrayList.get(lastIndex) == null) {
                recommendArrayList.remove(lastIndex);
                recommendedAdapter.notifyItemRemoved(lastIndex);
            }

        }
    }


    //-------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------recommended recyclerview finish-----------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------


    //-------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------live recyclerview show data-----------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------


    private void getLiveChannelData(final int currentPageLive, final ProgressBar adapterProgressbar, final LiveHomeChannelAdapter liveChannelAdapter) {

        if (getActivity() == null)
            return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (currentPageLive > 0) {
                            adapterProgressbar.setIndeterminate(true);
                            adapterProgressbar.setVisibility(View.VISIBLE);
                        } else {

                        }

                    }
                });

                Log.e("Live_channel_data", ApiRequest.LIVE_CHANNEL);
                final String key = "Live_home";
                final Type liveObjectType = new TypeToken<LiveParent>() {
                }.getType();
                AppController.getInstance().getCacheManager().getAsync(key, LiveParent.class, liveObjectType, new GetCallback() {
                    @Override
                    public void onSuccess(final Object object) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                if (getActivity() == null)
                                    return;
                                liveParentSaved = (LiveParent) object;

                                /*boolean isLiveVersionChanged = VersionUtils.getIsLiveVersionChanged(getActivity(), liveParentSaved, key);

                                if (isLiveVersionChanged) { //It means live data update is available
                                    liveParentSaved = null;
                                    countLive = 0;
                                    itemsLeftLive = 0;
                                }*/

                                if (liveParentSaved != null && liveParentSaved.live != null && liveParentSaved.live.size() != 0 &&
                                        (countLive <= liveParentSaved.live.size() || countLive > liveParentSaved.totalcount)
                                        && ((itemsLeftLive == -1 && liveArrayList.size() == 0) || itemsLeftLive > 0)) {
                                    Log.e("CacheManager", key + " object retrieved successfully");

                                    liveParent = liveParentSaved;

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (currentPageLive > 0) {
                                                adapterProgressbar.setVisibility(View.GONE);
                                            } else {

                                            }

                                        }
                                    });

                                    if (liveArrayList.size() < liveParentSaved.live.size()) {
                                        if (itemsLeftLive == -1) {
                                            liveArrayList.clear();
                                            itemsLeftLive = liveParentSaved.live.size();
                                        }

                                        if (itemsLeftLive == liveParentSaved.live.size())
                                            startCountLive = 0;
                                        else
                                            startCountLive = startCountLive + 5;

                                        endCountLive = startCountLive + 5;
                                        if (endCountLive > liveParentSaved.live.size())
                                            endCountLive = liveParentSaved.live.size();

                                        for (int i = startCountLive; i < endCountLive; i++) {
                                            liveArrayList.add(liveParentSaved.live.get(i));
                                        }

                                        itemsLeftLive = itemsLeftLive - 5;

                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                handleLivePagination(false, liveChannelAdapter);
                                            }
                                        });
                                    }
                                } else if (!isNetworkConnected()) {
                                    Log.e(TAG, "ConnectionManager Not Connected");

                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (currentPageLive > 0) {
                                                adapterProgressbar.setVisibility(View.GONE);
                                            } else {

                                            }
                                        }
                                    });
                                } else if (liveParentSaved == null || countLive <= liveParentSaved.totalcount) {
                                    StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                                            ApiRequest.LIVE_CHANNEL, new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(final String response) {
                                            new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    try {
                                                        if (getActivity() == null)
                                                            return;
                                                        getActivity().runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                if (currentPageLive > 0) {
                                                                    adapterProgressbar.setVisibility(View.GONE);
                                                                } else {

                                                                }

                                                            }
                                                        });

                                                        JSONObject mObj = new JSONObject(response);

                                                        MultitvCipher mcipher = new MultitvCipher();
                                                        String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                                        Log.e("Live channel data", str);
                                                        liveParent = Json.parse(str.trim(), LiveParent.class);

                                                        //sharedPreference.setPreferencesString(getActivity(), "offset_" + "Live", "" + liveParent.offset);

                                                        if (liveParentSaved != null) {
                                                            liveParentSaved.offset = liveParent.offset;
                                                            liveParentSaved.live.addAll(liveParent.live);
                                                            Log.e("***Live Data :", "save successfully");
                                                            AppController.getInstance().getCacheManager().put(key, liveParentSaved);
                                                        } else {
                                                            AppController.getInstance().getCacheManager().put(key, liveParent);
                                                        }

                                                        if (getActivity() == null)
                                                            return;
                                                        getActivity().runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                handleLivePagination(true, liveChannelAdapter);
                                                            }
                                                        });
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }).start();
                                        }
                                    }, new Response.ErrorListener() {

                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.e("Live_Error", "Error: " + error.getMessage());
                                            if (currentPageLive > 0) {
                                                adapterProgressbar.setVisibility(View.GONE);
                                            } else {

                                            }
                                        }
                                    }) {

                                        @Override
                                        protected Map<String, String> getParams() {
                                            Map<String, String> params = new HashMap<>();

                                            //params.put("lan", LocaleHelper.getLanguage(MoreDataActivity.this));
                                            //params.put("m_filter", (PreferenceData.isMatureFilterEnable(MoreDataActivity.this) ? "" + 1 : "" + 0));

                                            params.put("device", "android");
                                            params.put("live_offset", liveParentSaved != null ? "" + liveParentSaved.offset : "" + 0);
                                            params.put("live_limit", "5");
                                            return params;
                                        }
                                    };
                                    jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 3,
                                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                                    // Adding request to request queue
                                    AppController.getInstance().addToRequestQueue(jsonObjReq);
                                } else {
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (currentPageLive > 0) {
                                                adapterProgressbar.setVisibility(View.GONE);
                                            } else {

                                            }

                                        }
                                    });
                                }
                            }
                        }).start();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        }).start();
    }

    private void handleLivePagination(boolean isFromApiRequest, LiveHomeChannelAdapter liveChannelAdapter) {

        if (isFromApiRequest) {
            if (liveParent != null && liveParent.live != null && liveParent.live.size() != 0) {
                if (liveArrayList.size() == 0)
                    liveArrayList.addAll(liveParent.live);
                else {
                    for (int i = 0; i < liveArrayList.size(); i++) {
                        String catId = liveArrayList.get(i).id;
                        for (int j = 0; j < liveParent.live.size(); j++) {
                            if (catId.equals(liveParent.live.get(j).id)) {
                                liveParent.live.remove(j);
                            }
                        }
                    }
                    if (liveParent.live.size() > 0)
                        liveArrayList.addAll(liveParent.live);
                    Log.e("***LiveList-1***", "" + liveArrayList.size());
                    Log.e("***LiveParent-2***", "" + liveParent.live.size());
                }
            }
        }


        if (liveArrayList != null && liveArrayList.size() != 0) {
            liveChannelAdapter.setLoaded();
            liveChannelAdapter.notifyDataSetChanged();
            Log.e("***LiveLoadSIZE***", "" + liveArrayList.size());
            Log.e("***Live : ***", "Live : stop loadMore");
        }
    }

    private void initializeLiveChannelViews(final RecyclerView recyclerView) {
        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (liveArrayList != null && liveArrayList.size() != 0) {
                            PlayerUtils.startPlayerActivity(getActivity(),
                                    liveArrayList.get(position).url, "", "", "n/a", "", liveArrayList.get(position).id,
                                    "LIVE", 4);
                        }
                    }
                })
        );
    }

    //-------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------live recyclerview finish loading data-----------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------


    //-------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------display cat item  recyclerview loading data-----------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------


    private void getHomeCatData(final String cat_id, final int current_page, final ProgressBar adapterProgressbar, final HomeDisplayItemWithHeaderAdapter homeDisplayItemWithHeaderAdapter) {

        if (current_page > 0) {
            adapterProgressbar.setIndeterminate(true);
            adapterProgressbar.setVisibility(View.VISIBLE);
        } else {

        }
        //Log.e("home_Cat_Data", AppUtils.generateUrl(getApplicationContext(), ApiRequest.NEW_SEARCH_URL));
        final String key = "Home" + "_" + "cat" + "_" + cat_id;
        final Type objectType = new TypeToken<Recommended>() {
        }.getType();
        Log.i("HOME_TAB", "getHomeCatData() for category Id and key : " + key);
        AppController.getInstance().getCacheManager().getAsync(key, Recommended.class, objectType, new GetCallback() {
            @Override
            public void onSuccess(Object object) {
                videoDataSaved = (Recommended) object;

                if (videoDataSaved != null && videoDataSaved.content != null && videoDataSaved.content.size() != 0 &&
                        (countCatItem <= videoDataSaved.content.size() || countCatItem > videoDataSaved.totalCount)
                        && ((itemsLeftCatItem == -1 && dispalyCategoryItemArrayList.size() == 0) || itemsLeftCatItem > 0)) {
                    Log.e("CacheManager", key + " object retrieved successfully");

                    recommended1 = videoDataSaved;

                    Log.i("HOME_TAB", "getHomeCatData() cache video not null for category Id : " + cat_id);

                    if (current_page > 0) {
                        adapterProgressbar.setVisibility(View.GONE);
                    } else {
                    }

                    Log.i("HOME_TAB", "getHomeCatData() retrieved cache video for category Id : " + cat_id + " and for key : " + key);
                    Log.i("HOME_TAB", "getHomeCatData() retrieved cache video content for category Id : " + cat_id + " and for key : " + key + " Total count : " + recommended1.totalCount);
                    if (recommended1.content != null && recommended1.content.size() > 0) {
                        Log.i("HOME_TAB", "getHomeCatData() retrieved cache video content for category Id : " + cat_id + " and for key : " + key + " : " + recommended1.content.get(0).title);
                    }

                    if (dispalyCategoryItemArrayList.size() < videoDataSaved.content.size()) {
                        if (itemsLeftCatItem == -1) {
                            dispalyCategoryItemArrayList.clear();
                            itemsLeftCatItem = videoDataSaved.content.size();
                        }

                        if (itemsLeftCatItem == videoDataSaved.content.size())
                            startCountCatItem = 0;
                        else
                            startCountCatItem = startCountCatItem + 5;

                        endCountCatItem = startCountCatItem + 5;
                        if (endCountCatItem > videoDataSaved.content.size())
                            endCountCatItem = videoDataSaved.content.size();

                        for (int i = startCountCatItem; i < endCountCatItem; i++) {
                            dispalyCategoryItemArrayList.add(videoDataSaved.content.get(i));
                        }

                        itemsLeftCatItem = itemsLeftCatItem - 5;

                        handleCategoryPagination(false, homeDisplayItemWithHeaderAdapter);
                    }

                } else if (!isNetworkConnected()) {
                    if (current_page > 0) {
                        adapterProgressbar.setVisibility(View.GONE);
                    } else {
                        adapterProgressbar.setVisibility(View.GONE);
                    }
                    Log.e(TAG, "ConnectionManager Not Connected");
                } else if (videoDataSaved == null || countCatItem <= videoDataSaved.totalCount) {
                    Log.e("HomedisplaycategoryItem", ApiRequest.VIDEO_CAT_URL_CLIST);

                    Log.i("HOME_TAB", "getHomeCatData() get video from server for category Id : " + cat_id);

                    StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                            ApiRequest.VIDEO_CAT_URL_CLIST, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            try {
                                if (current_page > 0) {
                                    adapterProgressbar.setVisibility(View.GONE);
                                } else {
                                    adapterProgressbar.setVisibility(View.GONE);
                                }

                                JSONObject mObj = new JSONObject(response);

                                MultitvCipher mcipher = new MultitvCipher();
                                String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                Log.e("more display category", str);
                                recommended1 = Json.parse(str.trim(), Recommended.class);

                                sharedPreference.setPreferencesString(getActivity(), "offset_" + "home_Cat_Category" + cat_id, "" + recommended1.offset);

                                if (videoDataSaved != null) {
                                    videoDataSaved.offset = recommended1.offset;
                                    videoDataSaved.content.addAll(recommended1.content);
                                    Log.i("HOME_TAB", "getHomeCatData() cache video for category Id : " + cat_id + " and for key : " + key);
                                    AppController.getInstance().getCacheManager().putAsync(key, videoDataSaved, new PutCallback() {
                                        @Override
                                        public void onSuccess() {
                                            Log.e("HOME_TAB", key + " itemobject saved successfully");
                                        }

                                        @Override
                                        public void onFailure(Exception e) {
                                            //Log.e("item_home_category", e.getMessage());
                                        }
                                    });
                                } else {
                                    Log.i("HOME_TAB", "getHomeCatData() cache video for category Id : " + cat_id + " and for key : " + key);
                                    Log.i("HOME_TAB", "getHomeCatData() cache video content for category Id : " + cat_id + " and for key : " + key + " Total count : " + recommended1.totalCount);
                                    if (recommended1.content != null && recommended1.content.size() > 0) {
                                        Log.i("HOME_TAB", "getHomeCatData() cache video content for category Id : " + cat_id + " and for key : " + key + " : " + recommended1.content.get(0).title);
                                    }
                                    AppController.getInstance().getCacheManager().putAsync(key, recommended1, new PutCallback() {
                                        @Override
                                        public void onSuccess() {
                                            Log.e("CacheManager", key + " itemobject saved successfully");
                                        }

                                        @Override
                                        public void onFailure(Exception e) {
                                            Log.e("_home_category_item", e.getMessage());
                                        }
                                    });
                                }

                                handleCategoryPagination(true, homeDisplayItemWithHeaderAdapter);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Error", "Error: " + error.getMessage());

                            if (current_page > 0) {
                                adapterProgressbar.setVisibility(View.GONE);
                            } else {
                                adapterProgressbar.setVisibility(View.GONE);
                            }
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();

                            //params.put("lan", LocaleHelper.getLanguage(MoreDataActivity.this));
                            // params.put("m_filter", (PreferenceData.isMatureFilterEnable(MoreDataActivity.this) ? "" + 1 : "" + 0));

                            params.put("device", "android");
                            params.put("user_id", user_id);
                            params.put("current_offset", String.valueOf(countCatItem));
                            params.put("cat_id", cat_id);
                            params.put("max_counter", "5");
                            //params.put("search_tag", query);
                            Set<String> keys = params.keySet();
                            for (String key : keys) {
                                Log.e(TAG, "getHomeContent().getParams: " + key + "      " + params.get(key));
                            }
                            return params;
                        }
                    };

                    // Adding request to request queue
                    AppController.getInstance().addToRequestQueue(jsonObjReq);
                } else {
                    if (current_page > 0) {
                        adapterProgressbar.setVisibility(View.GONE);
                    } else {
                        adapterProgressbar.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void handleCategoryPagination(boolean isFromApiRequest, HomeDisplayItemWithHeaderAdapter homeDisplayItemWithHeaderAdapter) {
       // refreshHomeList(recommended1.content, homeDisplayItemWithHeaderAdapter);
        if (isFromApiRequest) {
            if (recommended1 != null && recommended1.content != null && recommended1.content.size() != 0) {
                dispalyCategoryItemArrayList.addAll(recommended.content);
            }
        }


        if (dispalyCategoryItemArrayList != null && dispalyCategoryItemArrayList.size() != 0) {
            homeDisplayItemWithHeaderAdapter.setLoaded();
            homeDisplayItemWithHeaderAdapter.notifyDataSetChanged();
        }
    }

   /* private void refreshHomeList(List<Content> displasyList, HomeDisplayItemWithHeaderAdapter homeDisplayItemWithHeaderAdapter) {

        if (displasyList.size() > 0) {
            int lastIndex = displasyList.size() - 1;
            if (displasyList.get(lastIndex) == null) {
                displasyList.remove(lastIndex);
                homeDisplayItemWithHeaderAdapter.notifyItemRemoved(lastIndex);
            }

        }
    }*/
    //-------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------display cat item  recyclerview fin]ish loading data-----------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------


    @Override
    public void onSliderClick(BaseSliderView slider) {
        String mslider_url = slider.getBundle().getString("video_url");
        String mslider_title = slider.getBundle().getString("title");
        String mslider_des = slider.getBundle().getString("des");
        String mslider_type = slider.getBundle().getString("type");
        String mslider_thumbnail = slider.getBundle().getString("thumbnail");
        String mslider_source = slider.getBundle().getString("content_type");
        String position = slider.getBundle().getString("position");
        String fav_item = slider.getBundle().getString("fav_item");
        // String content_id = slider.getBundle().getString("content_id");
        String like = slider.getBundle().getString("like");
        String video_id = slider.getBundle().getString("video_id");
        String start_cast = slider.getBundle().getString("start_cast");

        if (getActivity() == null)
            return;
        Intent videoIntent = new Intent(getActivity(), VeqtaPlayerActivity.class);
        videoIntent.putExtra("VIDEO_URL", mslider_url);
        videoIntent.putExtra("descreption", mslider_des);
        videoIntent.putExtra("title", mslider_title);
        videoIntent.putExtra("type", mslider_type);
        videoIntent.putExtra("like", like);
        videoIntent.putExtra("start_cast", start_cast);
        videoIntent.putExtra("video_id", video_id);
        //videoIntent.putExtra("content_id", content_id);
        videoIntent.putExtra("thumbnail", mslider_thumbnail);
        videoIntent.putExtra("content_type", mslider_source);
        videoIntent.putExtra("position", position);
        videoIntent.putExtra("fav_item", fav_item);
        videoIntent.putExtra("CONTENT_TYPE_MULTITV", "VOD");
        videoIntent.putExtra("SOCIAL_LIKES", slider.getBundle().getInt("social_like"));
        videoIntent.putExtra("SOCIAL_VIEWS", slider.getBundle().getInt("social_view"));

        startActivity(videoIntent);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onResume() {
        super.onResume();

        if (homeAllDataCategoryListAdapter != null && homeAllDataCategoryListAdapter.featureBannerSlider != null) {
            homeAllDataCategoryListAdapter.featureBannerSlider.startAutoCycle();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (homeAllDataCategoryListAdapter != null && homeAllDataCategoryListAdapter.featureBannerSlider != null) {
            homeAllDataCategoryListAdapter.featureBannerSlider.stopAutoCycle();
        }
    }


}
