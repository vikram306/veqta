package com.multitv.ott.fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.reflect.TypeToken;
import com.iainconnor.objectcache.GetCallback;
import com.multitv.cipher.MultitvCipher;
import com.multitv.ott.R;
import com.multitv.ott.Utils.Json;
import com.multitv.ott.adapter.LiveChannelCategoryAdapter;
import com.multitv.ott.adapter.liveChannalContentFragmentAdapter;
import com.multitv.ott.api.ApiRequest;
import com.multitv.ott.controller.AppController;
import com.multitv.ott.listeners.LiveCategoryInterfaces;
import com.multitv.ott.listeners.OnLoadMoreListener;
import com.multitv.ott.models.categories.LiveCatData;
import com.multitv.ott.models.home.ContentHome;
import com.multitv.ott.models.live.LiveParent;
import com.multitv.ott.models.categories.LiveCategory;
import com.multitv.ott.sharedpreference.SharedPreference;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 20-02-2017.
 */

public class LiveChannelCategoryFragment extends Fragment implements LiveCategoryInterfaces {
    private SharedPreference sharedPreference;
    private String user_id;
    private liveChannalContentFragmentAdapter liveChannalContentFragmentAdapter;
    private LiveCategory category;
    private String TAG = "LiveFragment";
    private LiveParent liveParent, liveParentSaved;
    private int nextDayTestPostion, current_page, count, itemsLeft = -1, startCount = 0, endCount = 0;
    private List<ContentHome> liveArrayList = new ArrayList<>();

    @BindView(R.id.video_fragment_recyclerView)
    protected RecyclerView live_recyclerView;
    @BindView(R.id.video_fragment_progress_bar_top)
    protected ProgressBar progressBarTop;
    @BindView(R.id.video_fragment_progress_bar_bottom)
    protected ProgressBar progressBarBottom;
    @BindView(R.id.fragment_video_ll_empty)
    LinearLayout llNoRecordFound;
    @BindView(R.id.fragment_video_fl_container)
    FrameLayout flContainer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.fragment_video, container, false);
        ButterKnife.bind(this, rootview);
        sharedPreference = new SharedPreference();
        user_id = new SharedPreference().getPreferencesString(getActivity(), "user_id" + "_" + ApiRequest.TOKEN);
        getCategories();

        if (getChildFragmentManager() != null) {
            for (int i = 0; i < getChildFragmentManager().getBackStackEntryCount(); ++i) {
                getChildFragmentManager().popBackStack();
            }
        }

        return rootview;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    private void getCategories() {
        final String key = "VideoTab";
        final Type homeObjectType = new TypeToken<LiveCategory>() {
        }.getType();

        progressBarTop.setVisibility(View.VISIBLE);

        AppController.getInstance().getCacheManager().getAsync(key, LiveCategory.class, homeObjectType, new GetCallback() {
            @Override
            public void onSuccess(final Object object) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        category = (LiveCategory) object;
                        if (getActivity() == null)
                            return;
                        //if (VersionUtils.getIsCategoryVersionChanged(getActivity(), category, key))
                        // category = null;

                        if (category != null) {
                            Log.e("CacheManager", key + " object retrieved successfully");

                            if (getActivity() == null)
                                return;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (progressBarTop.getVisibility() == View.VISIBLE) {
                                        progressBarTop.setVisibility(View.GONE);
                                    }
                                    setLiveCategory();
                                }
                            });
                        } else if (!isNetworkConnected()) {
                            Log.e("LiveFragment", "ConnectionManager Not Connected");
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (progressBarTop.getVisibility() == View.VISIBLE) {
                                        progressBarTop.setVisibility(View.GONE);
                                    }
                                }
                            });
                        } else {
                            StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                                    ApiRequest.LIVE_CATEGORY, new Response.Listener<String>() {
                                @Override
                                public void onResponse(final String response) {
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.e("categoryName-Live--", response.toString());
                                            try {
                                                JSONObject mObj = new JSONObject(response);
                                                if (mObj.optInt("code") == 1) {
                                                    MultitvCipher mcipher = new MultitvCipher();
                                                    String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                                    Log.e("decode_category---", str);

                                                    category = Json.parse(str.trim(), LiveCategory.class);
                                                    Log.e("LiveFragment", "onResponse:LIST SIZE : " + category.liveCategoryArrayList.size());

                                                    Log.e("Livecategory_list--", str.toString());
                                                    AppController.getInstance().getCacheManager().put(key, category);

                                                    if (getActivity() == null)
                                                        return;
                                                    getActivity().runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            if (progressBarTop.getVisibility() == View.VISIBLE) {
                                                                progressBarTop.setVisibility(View.GONE);
                                                            }
                                                            setLiveCategory();
                                                        }
                                                    });
                                                }
                                            } catch (Exception e) {
                                                Log.e("live", "Error: " + e.getMessage());
                                            }
                                        }
                                    }).start();
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e("live", "Error: " + error.getMessage());
                                    if (progressBarTop.getVisibility() == View.VISIBLE) {
                                        progressBarTop.setVisibility(View.GONE);
                                    }
                                }
                            }) {

                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<String, String>();

                                    //params.put("lan", LocaleHelper.getLanguage(getApplicationContext()));
                                    //params.put("m_filter", (PreferenceData.isMatureFilterEnable(getApplicationContext()) ? "" + 1 : "" + 0));
                                    params.put("device", "android");

                                    return params;
                                }
                            };

                            AppController.getInstance().addToRequestQueue(jsonObjReq);
                        }
                    }
                }).start();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("categoryError", e.getMessage());
            }
        });
    }

    private void setLiveCategory() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        live_recyclerView.setLayoutManager(linearLayoutManager);
        live_recyclerView.setHasFixedSize(true);
        liveChannalContentFragmentAdapter = new liveChannalContentFragmentAdapter(getActivity(), category.liveCategoryArrayList, live_recyclerView, this);
        live_recyclerView.setAdapter(liveChannalContentFragmentAdapter);

        if (category.liveCategoryArrayList == null || category.liveCategoryArrayList.size() == 0) {
            live_recyclerView.setVisibility(View.GONE);
            llNoRecordFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setLiveCategoryItemData(RecyclerView recyclerView, final ProgressBar progressBar, int postion, final String cat_id, LinearLayout headerTitle) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        final LiveChannelCategoryAdapter liveChannelAdapter = new LiveChannelCategoryAdapter(getActivity(), liveArrayList, recyclerView);
        recyclerView.setAdapter(liveChannelAdapter);
        if (liveArrayList != null && liveArrayList.size() != 0) {
            headerTitle.setVisibility(View.VISIBLE);
        } else {
            headerTitle.setVisibility(View.GONE);
        }
        liveChannelAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                current_page = current_page + 1;
                Log.e("more data", "" + current_page);
                count = liveParent.offset;

                getLiveChannelData(current_page, progressBar, cat_id, liveChannelAdapter);
            }
        });

        getLiveChannelData(current_page, progressBar, cat_id, liveChannelAdapter);
    }


    //-------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------live recyclerview show data-----------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------


    private void getLiveChannelData(final int current_page, final ProgressBar adapterProgressbar, final String categoryId, final LiveChannelCategoryAdapter liveChannelAdapter) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (getActivity() == null)
                    return;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (current_page > 0) {
                            adapterProgressbar.setIndeterminate(true);
                            adapterProgressbar.setVisibility(View.VISIBLE);
                        } else {

                        }
                    }
                });

                Log.e("LiveFragment-URL", ApiRequest.LIVE_CHANNEL);

                final String key = "Live" + "_" + categoryId;
                final Type liveObjectType = new TypeToken<LiveParent>() {
                }.getType();
                AppController.getInstance().getCacheManager().getAsync(key, LiveParent.class, liveObjectType, new GetCallback() {
                    @Override
                    public void onSuccess(final Object object) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                liveParentSaved = (LiveParent) object;

                               /* if (getActivity() == null)
                                    return;
                                boolean isLiveVersionChanged = VersionUtils.getIsLiveVersionChanged(getActivity(), liveParentSaved, key);

                                if (isLiveVersionChanged) { //It means live data update is available
                                    liveParentSaved = null;
                                    count = 0;
                                    itemsLeft = 0;
                                }*/

                                if (liveParentSaved != null && liveParentSaved.live != null && liveParentSaved.live.size() != 0 &&
                                        (count <= liveParentSaved.live.size() || count > liveParentSaved.totalcount)
                                        && ((itemsLeft == -1 && liveArrayList.size() == 0) || itemsLeft > 0)) {
                                    Log.e("CacheManager", key + " object retrieved successfully");

                                    liveParent = liveParentSaved;

                                    if (getActivity() == null)
                                        return;
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (current_page > 0) {
                                                adapterProgressbar.setVisibility(View.GONE);
                                            } else {

                                            }
                                        }
                                    });

                                    if (liveArrayList.size() < liveParentSaved.live.size()) {
                                        if (itemsLeft == -1) {
                                            liveArrayList.clear();
                                            itemsLeft = liveParentSaved.live.size();
                                        }

                                        if (itemsLeft == liveParentSaved.live.size())
                                            startCount = 0;
                                        else
                                            startCount = startCount + 5;

                                        endCount = startCount + 5;
                                        if (endCount > liveParentSaved.live.size())
                                            endCount = liveParentSaved.live.size();

                                        for (int i = startCount; i < endCount; i++) {
                                            liveArrayList.add(liveParentSaved.live.get(i));
                                        }

                                        itemsLeft = itemsLeft - 5;

                                        if (getActivity() == null)
                                            return;
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                handleLivePagination(false, liveChannelAdapter);
                                            }
                                        });
                                    }
                                } else if (!isNetworkConnected()) {
                                    Log.e(TAG, "ConnectionManager Not Connected");

                                    if (getActivity() == null)
                                        return;
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (current_page > 0) {
                                                adapterProgressbar.setVisibility(View.GONE);
                                            } else {

                                            }
                                        }
                                    });
                                } else if (liveParentSaved == null || count <= liveParentSaved.totalcount) {
                                    StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                                            ApiRequest.LIVE_CHANNEL, new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(final String response) {
                                            new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    try {
                                                        if (getActivity() == null)
                                                            return;
                                                        getActivity().runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                if (current_page > 0) {
                                                                    adapterProgressbar.setVisibility(View.GONE);
                                                                } else {

                                                                }
                                                            }
                                                        });

                                                        JSONObject mObj = new JSONObject(response);

                                                        MultitvCipher mcipher = new MultitvCipher();
                                                        String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                                        Log.e(TAG, "" + "LiveFragment : " + str);
                                                        liveParent = Json.parse(str.trim(), LiveParent.class);

                                                        //sharedPreference.setPreferencesString(MoreDataActivity.this, "offset_" + "Live", "" + liveParent.offset);

                                                        if (liveParentSaved != null) {
                                                            liveParentSaved.offset = liveParent.offset;
                                                            liveParentSaved.live.addAll(liveParent.live);
                                                            AppController.getInstance().getCacheManager().put(key, liveParentSaved);
                                                        } else {
                                                            AppController.getInstance().getCacheManager().put(key, liveParent);
                                                        }

                                                        if (getActivity() == null)
                                                            return;
                                                        getActivity().runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                handleLivePagination(true, liveChannelAdapter);
                                                            }
                                                        });
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }).start();
                                        }
                                    }, new Response.ErrorListener() {

                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.e(TAG, "Error: " + error.getMessage());

                                            if (current_page > 0) {
                                                adapterProgressbar.setVisibility(View.GONE);
                                            } else {

                                            }
                                        }
                                    }) {

                                        @Override
                                        protected Map<String, String> getParams() {
                                            Map<String, String> params = new HashMap<>();

                                            if (getActivity() == null)
                                                return null;
                                            //params.put("lan", LocaleHelper.getLanguage(getActivity()));
                                            //params.put("m_filter", (PreferenceData.isMatureFilterEnable(getActivity()) ? "" + 1 : "" + 0));

                                            params.put("device", "android");
                                            params.put("cat_id", "" + categoryId);
                                            Log.e(TAG, "Live-cat-id" + categoryId);
                                            params.put("live_offset", liveParentSaved != null ? "" + liveParentSaved.offset : "" + 0);
                                            params.put("live_limit", "5");

                                            return params;
                                        }
                                    };

                                    // Adding request to request queue
                                    AppController.getInstance().addToRequestQueue(jsonObjReq);
                                } else {
                                    if (getActivity() == null)
                                        return;
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (current_page > 0) {
                                                adapterProgressbar.setVisibility(View.GONE);
                                            } else {

                                            }
                                        }
                                    });
                                }
                            }
                        }).start();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        }).start();
    }

    private void handleLivePagination(boolean isFromApiRequest, LiveChannelCategoryAdapter liveChannelAdapter) {
        if (isFromApiRequest) {
            if (liveParent != null && liveParent.live != null && liveParent.live.size() != 0) {
                liveArrayList.addAll(liveParent.live);
            }
        }

        if (liveArrayList != null && liveArrayList.size() != 0) {
            live_recyclerView.setVisibility(View.VISIBLE);
            llNoRecordFound.setVisibility(View.GONE);
            liveChannelAdapter.setLoaded();
            liveChannelAdapter.notifyDataSetChanged();
        } else {
            live_recyclerView.setVisibility(View.GONE);
            llNoRecordFound.setVisibility(View.VISIBLE);
        }
    }

    public void doInvisibleContainer() {
        flContainer.setVisibility(View.GONE);
    }

    @Override
    public void onGetChildList(LiveCatData liveCat) {
        if (getActivity() == null)
            return;

       /* if (getActivity() instanceof OnLoadChildCategory) {
            ((OnLoadChildCategory) getActivity()).onLoadChildCategoryFragment(videoMain);
        }*/

        VideoChildFragment videoChildFragment = new VideoChildFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(VideoChildFragment.EXTRA_LIVE_OBJECT, liveCat);
        videoChildFragment.setArguments(bundle);
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(flContainer.getId(), videoChildFragment).addToBackStack("sub cattegory");
        ft.commitAllowingStateLoss();
        flContainer.setVisibility(View.VISIBLE);
    }
}