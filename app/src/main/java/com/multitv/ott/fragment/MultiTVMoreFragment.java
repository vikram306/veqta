package com.multitv.ott.fragment;


import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.common.reflect.TypeToken;
import com.iainconnor.objectcache.GetCallback;
import com.iainconnor.objectcache.PutCallback;
import com.multitv.ott.Utils.AppNetworkAlertDialog;
import com.multitv.ott.activity.SplashActivity;
import com.multitv.ott.adapter.MoreRecommendedAdapter;
import com.multitv.cipher.MultitvCipher;
import com.multitv.ott.R;
import com.multitv.ott.Utils.Json;
import com.multitv.ott.api.ApiRequest;
import com.multitv.ott.controller.AppController;
import com.multitv.ott.listeners.OnLoadMoreListener;
import com.multitv.ott.models.home.ContentHome;
import com.multitv.ott.models.recommendeds.Content;
import com.multitv.ott.models.recommendeds.Recommended;
import com.multitv.ott.sharedpreference.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.multitv.ott.Utils.AppConstants.CONTENT_TYPE_MULTITV_PLAYER_ACTIVITY;

/**
 * Created by Lenovo on 31-01-2017.
 */

public class MultiTVMoreFragment extends Fragment {
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.progress_bar_top)
    ProgressBar progressBarTop;
    @BindView(R.id.progress_bar_bottom)
    ProgressBar progressBarBottom;
    @BindView(R.id.empty)
    LinearLayout noRecordFoundTV;
    private int position, count = 0, current_page = 0, itemsLeft = -1, startCount = 0, endCount = 0;
    private List<Content> recommendArrayList = new ArrayList<>();
    private String recommendedListId;
    private Recommended recommended;
    private MoreRecommendedAdapter moreRecommendedAdapter;
    private int recommendedListType;

    public static MultiTVMoreFragment newInstance(int position, String recommendedListId, int recommendedListType) {
        MultiTVMoreFragment multiTVMoreFragment = new MultiTVMoreFragment();

        Bundle args = new Bundle();
        args.putInt("recommendedList_TYPE", recommendedListType);
        args.putInt("POSITION", position);
        args.putString("recommendedList_ID", recommendedListId);
        multiTVMoreFragment.setArguments(args);

        return multiTVMoreFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.player_recommended_item_row, container, false);
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            recommendedListType = bundle.getInt("recommendedList_TYPE");
            position = bundle.getInt("POSITION");
            Log.e("***content-category",""+position);
            if (position != 1) {
                recommendedListId = bundle.getString("recommendedList_ID");
                if(!TextUtils.isEmpty(recommendedListId))
                Log.e("***recommendedListId",recommendedListId);
            }
        }

        if (position != 1) {
                      switch (recommendedListType) {
                case CONTENT_TYPE_MULTITV_PLAYER_ACTIVITY:
                    showRecommendedData();
                    break;
            }
        } else {
            showRecommendedData();
        }

    }



    private void showRecommendedData() {
        if (getActivity() == null)
            return;

        String offsetString = new SharedPreference().getPreferencesString(getActivity(), "offset_" + "More_ Recommended" + "_" + (!TextUtils.isEmpty(recommendedListId) ? recommendedListId : ""));
        if (!TextUtils.isEmpty(offsetString))
            count = Integer.parseInt(offsetString);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        moreRecommendedAdapter = new MoreRecommendedAdapter(getActivity(), recommendArrayList, recyclerView, true);
        recyclerView.setAdapter(moreRecommendedAdapter);
        recyclerView.setNestedScrollingEnabled(true);

        new Thread(new Runnable() {
            @Override
            public void run() {
                getRecommended(current_page);
            }
        }).start();

        moreRecommendedAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                current_page = current_page + 1;
                Log.e("more data", "" + current_page);
                count = recommended.offset;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        getRecommended(current_page);
                    }
                }).start();
            }
        });
    }



    private void getRecommended(final int current_page) {
        if (getActivity() == null)
            return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (current_page > 0) {
                    progressBarBottom.setVisibility(View.VISIBLE);
                } else {
                    progressBarTop.setVisibility(View.VISIBLE);
                }
            }
        });

        final String key = "More" + "_" + "Recommended" + (position != 1 && recommendedListId != null ? recommendedListId : "");
        final Type objectType = new TypeToken<Recommended>() {
        }.getType();
        AppController.getInstance().getCacheManager().getAsync(key, Recommended.class, objectType, new GetCallback() {
            @Override
            public void onSuccess(final Object object) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final Recommended recommendedSaved = (Recommended) object;

                        if (recommendedSaved != null && recommendedSaved.content != null && recommendedSaved.content.size() != 0 &&
                                (count <= recommendedSaved.content.size() || count > recommendedSaved.totalCount)
                                && ((itemsLeft == -1 && recommendArrayList.size() == 0) || itemsLeft > 0)) {
                            Log.e("CacheManager", key + " object retrieved successfully");

                            Log.e("CacheManager", "RecomendedList : "+recommendArrayList.size());
                            if (getActivity() == null)
                                return;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (current_page > 0)
                                        progressBarBottom.setVisibility(View.GONE);
                                    else {
                                        progressBarTop.setVisibility(View.GONE);
                                    }
                                }
                            });

                            recommended = recommendedSaved;

                            if (recommendArrayList.size() < recommendedSaved.content.size()) {
                                if (itemsLeft == -1) {
                                    recommendArrayList.clear();
                                    itemsLeft = recommendedSaved.content.size();
                                    Log.e("CacheManager", "RecomendedList : "+recommendArrayList.size());
                                }

                                if (itemsLeft == recommendedSaved.content.size())
                                    startCount = 0;
                                else
                                    startCount = startCount + 10;
                                    endCount = startCount + 10;

                                if (endCount > recommendedSaved.content.size())
                                    endCount = recommendedSaved.content.size();

                                for (int i = startCount; i < endCount; i++) {
                                    recommendArrayList.add(recommendedSaved.content.get(i));
                                    Log.e("CacheManager", "RecomendedList : "+recommendArrayList.size());
                                }

                                itemsLeft = itemsLeft - 10;

                                if (getActivity() == null)
                                    return;
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        handleRecommendedPagination(false);
                                    }
                                });
                            }
                        } else if(!AppNetworkAlertDialog.isNetworkConnected(getActivity())){
                            AppNetworkAlertDialog.showNetworkNotConnectedDialog(getActivity());
                        } else if (recommendedSaved == null || recommendedSaved.content == null || recommendedSaved.content.size() == 0
                                || count <= recommendedSaved.totalCount) {
                            if (getActivity() == null)
                                return;
                            Log.e("Recommended Url", ApiRequest.VIDEO_CAT_URL_CLIST );
                            StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                                    recommendedListId != null ? ApiRequest.VIDEO_CAT_URL_CLIST :  ApiRequest.RECOMMENDED_LIST, new Response.Listener<String>() {

                                @Override
                                public void onResponse(final String response) {
                                    Log.e("Recommended_responce---", response.toString());

                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (getActivity() == null)
                                                return;
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    if (current_page > 0)
                                                        progressBarBottom.setVisibility(View.GONE);
                                                    else {
                                                        progressBarTop.setVisibility(View.GONE);
                                                    }
                                                }
                                            });

                                            try {
                                                JSONObject mObj = new JSONObject(response);
                                                if (mObj.optInt("code") == 1) {
                                                    MultitvCipher mcipher = new MultitvCipher();
                                                    String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                                    Log.e("Recommended response---", str);
                                                    try {
                                                        JSONObject newObj = new JSONObject(str);
                                                        recommended = Json.parse(newObj.toString(), Recommended.class);

                                                        new SharedPreference().setPreferencesString(getActivity(), "offset_" + "More_ Recommended" + "_" + (!TextUtils.isEmpty(recommendedListId) ? recommendedListId : ""), "" + recommended.offset);

                                                        if (recommended == null || recommended.content == null || recommended.content.size() == 0) {
                                                            if (getActivity() == null)
                                                                return;
                                                            getActivity().runOnUiThread(new Runnable() {
                                                                @Override
                                                                public void run() {
                                                                    progressBarTop.setVisibility(View.GONE);
                                                                    recyclerView.setVisibility(View.GONE);
                                                                    noRecordFoundTV.setVisibility(View.VISIBLE);
                                                                }
                                                            });
                                                        }

                                                        if (recommendedSaved != null) {
                                                            recommendedSaved.offset = recommended.offset;
                                                            recommendedSaved.content.addAll(recommended.content);
                                                            AppController.getInstance().getCacheManager().putAsync(key, recommendedSaved, new PutCallback() {
                                                                @Override
                                                                public void onSuccess() {
                                                                    Log.e("CacheManager", key + " object saved successfully");
                                                                }

                                                                @Override
                                                                public void onFailure(Exception e) {
                                                                    Log.e("Error", "Error: " + e.getMessage());
                                                                }
                                                            });
                                                        } else {
                                                            AppController.getInstance().getCacheManager().putAsync(key, recommended, new PutCallback() {
                                                                @Override
                                                                public void onSuccess() {
                                                                    Log.e("CacheManager", key + " object saved successfully");
                                                                }

                                                                @Override
                                                                public void onFailure(Exception e) {
                                                                    Log.e("Error", "Error: " + e.getMessage());
                                                                }
                                                            });
                                                        }

                                                        // Log.e("Recommended", newObj.toString());
                                                    } catch (JSONException e) {
                                                        Log.e("Recommended", "JSONException");
                                                        Log.e("Error", "Error: " + e.getMessage());
                                                    }

                                                    if (getActivity() == null)
                                                        return;

                                                    getActivity().runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            handleRecommendedPagination(true);
                                                        }
                                                    });
                                                }

                                            } catch (Exception e) {
                                                Log.e("Error", "Error: " + e.getMessage());
                                            }
                                        }
                                    }).start();
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e("Error", "Error: " + error.getMessage());

                                    if (current_page > 0)
                                        progressBarBottom.setVisibility(View.GONE);
                                    else {
                                        progressBarTop.setVisibility(View.GONE);
                                    }
                                }
                            }) {

                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<>();

                                    //params.put("lan", LocaleHelper.getLanguage(getApplicationContext()));
                                    //params.put("m_filter", (PreferenceData.isMatureFilterEnable(getApplicationContext()) ? "" + 1 : "" + 0));

                                    params.put("device", "android");
                                    if (getActivity() == null)
                                        return params;
                                    params.put("user_id", new SharedPreference().getPreferencesString(getActivity(), "user_id" + "_" + ApiRequest.TOKEN));
                                    params.put("current_offset", String.valueOf(count));
                                    params.put("max_counter", "10");
                                    if (!TextUtils.isEmpty(recommendedListId))
                                        params.put("cat_id", recommendedListId);

                                    return params;
                                }
                            };

                            // Adding request to request queue
                            AppController.getInstance().addToRequestQueue(jsonObjReq);
                        } else {
                            if (getActivity() == null)
                                return;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (current_page > 0)
                                        progressBarBottom.setVisibility(View.GONE);
                                    else {
                                        progressBarTop.setVisibility(View.GONE);
                                    }
                                }
                            });
                        }
                    }
                }).start();
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void handleRecommendedPagination(boolean isFromApiRequest) {
        if (isFromApiRequest) {
            if (recommended != null && recommended.content != null && recommended.content.size() != 0) {
                recommendArrayList.addAll(recommended.content);
                Log.e("LoadMorre", "RecomendedList : "+recommendArrayList.size());
            }
        }

        if (recommendArrayList != null && recommendArrayList.size() != 0) {
            moreRecommendedAdapter.setLoaded();
            moreRecommendedAdapter.notifyDataSetChanged();
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }
}