package com.multitv.ott.fragment;

import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.reflect.TypeToken;
import com.iainconnor.objectcache.GetCallback;
import com.multitv.cipher.MultitvCipher;
import com.multitv.ott.R;
import com.multitv.ott.Utils.Json;
import com.multitv.ott.adapter.VideoContentFragmentAdapter;
import com.multitv.ott.api.ApiRequest;
import com.multitv.ott.controller.AppController;
import com.multitv.ott.models.Category;
import com.multitv.ott.models.categories.LiveCategory;
import com.multitv.ott.models.categories.VOD;
import com.multitv.ott.sharedpreference.SharedPreference;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;

/**
 * Created by Lenovo on 03-03-2017.
 */

public class VideoTabFragment extends Fragment {
    private LiveCategory category;
    private ViewPager viewPager;
    private SharedPreference sharedPreference;
    private String user_id;
    private VideoContentFragmentAdapter videoContentFragmentAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //setRetainInstance(true);
        View rootview = inflater.inflate(R.layout.video_parent_fragment, container, false);
        ButterKnife.bind(this, rootview);
        sharedPreference = new SharedPreference();
        viewPager = (ViewPager) rootview.findViewById(R.id.view_pager_video);
        viewPager.setOffscreenPageLimit(1);
        user_id = new SharedPreference().getPreferencesString(getActivity(), "user_id" + "_" + ApiRequest.TOKEN);
        getCategories();
        return rootview;
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    private void getCategories() {
        Log.e("VIdeoFragment", "getCategories Method");
        final String key = "VideoTab";
        final Type homeObjectType = new TypeToken<LiveCategory>() {
        }.getType();
        AppController.getInstance().getCacheManager().getAsync(key, LiveCategory.class, homeObjectType, new GetCallback() {
            @Override
            public void onSuccess(final Object object) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        category = (LiveCategory) object;
                        if (getActivity() == null)
                            return;
                        //if (VersionUtils.getIsCategoryVersionChanged(getActivity(), category, key))
                        // category = null;

                        if (category != null) {
                            Log.e("VODCacheManager", key + " object retrieved successfully");

                            if (getActivity() == null)
                                return;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    setVodCategory();
                                }
                            });
                        } else if (!isNetworkConnected()) {
                            Log.e("VIdeoFragment", "ConnectionManager Not Connected");
                        } else {
                            StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                                    ApiRequest.LIVE_CATEGORY, new Response.Listener<String>() {
                                @Override
                                public void onResponse(final String response) {
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.e("***VOD-RESPONCE***-", response.toString());
                                            try {
                                                JSONObject mObj = new JSONObject(response);
                                                if (mObj.optInt("code") == 1) {
                                                    MultitvCipher mcipher = new MultitvCipher();
                                                    String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                                    category = Json.parse(str.trim(), LiveCategory.class);
                                                    Log.e("VideoFragment", "VOD:LIST SIZE : " + category.vod.size());

                                                    Log.e("VideoFragment_list--", str.toString());
                                                    AppController.getInstance().getCacheManager().put(key, category);

                                                    if (getActivity() == null)
                                                        return;
                                                    getActivity().runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            setVodCategory();
                                                        }
                                                    });
                                                }
                                            } catch (Exception e) {
                                                Log.e("VIDEO", "Error: " + e.getMessage());
                                            }
                                        }
                                    }).start();
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e("Video", "Error: " + error.getMessage());
                                }
                            }) {

                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<String, String>();

                                    //params.put("lan", LocaleHelper.getLanguage(getApplicationContext()));
                                    //params.put("m_filter", (PreferenceData.isMatureFilterEnable(getApplicationContext()) ? "" + 1 : "" + 0));
                                    params.put("device", "android");

                                    return params;
                                }
                            };

                            AppController.getInstance().addToRequestQueue(jsonObjReq);
                        }
                    }
                }).start();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("categoryError", e.getMessage());
            }
        });
    }

    private void setVodCategory() {
        ArrayList<VOD> videoVodList = new ArrayList<>();

        for (int i = 0; i < category.vod.size(); i++) {
            VOD vod = category.vod.get(i);
            // if (!vod.name.equalsIgnoreCase("MOVIE") && !vod.name.equalsIgnoreCase("TV Shows")) {
            videoVodList.add(vod);
            // }
        }

        setTabsVideo(videoVodList);
    }


    private void setTabsVideo(final List<VOD> data) {
        if (data != null) {
            //mListener.onCategory(data.get(0).id);
            for (int i = 0; i < data.size(); i++) {
                Log.e("video name", "" + data.get(i).name);
                Log.e("video id", "" + data.get(i).id);
            }

            if (getActivity() == null)
                return;
            videoContentFragmentAdapter = new VideoContentFragmentAdapter(getChildFragmentManager(), data);
            viewPager.setAdapter(videoContentFragmentAdapter);

            if (getView() == null)
                return;


            TabLayout tabLayout = (TabLayout) getView().findViewById(R.id.sliding_tabs_video);
            tabLayout.setupWithViewPager(viewPager);
            viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
            viewPager.setOffscreenPageLimit(1);
            Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/app_customfonts.ttf");
            ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
            int tabsCount = vg.getChildCount();
            for (int j = 0; j < tabsCount; j++) {
                ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
                int tabChildsCount = vgTab.getChildCount();
                for (int i = 0; i < tabChildsCount; i++) {
                    View tabViewChild = vgTab.getChildAt(i);
                    if (tabViewChild instanceof TextView) {
                        ((TextView) tabViewChild).setTypeface(tf);
                    }
                }
            }
            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    // array.clear();
                    Log.e("position", String.valueOf(position));

                    switch (position) {

                        case 1:
                            //like a example
                            //  setViewPagerByIndex(0);
                            break;
                    }
                }

                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        }
    }

    public VideoContentFragmentAdapter getChildFragmentAdapter() {
        return videoContentFragmentAdapter;
    }

    public ViewPager getViewPager() {
        return viewPager;
    }
}
