package com.multitv.ott.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.reflect.TypeToken;
import com.iainconnor.objectcache.GetCallback;
import com.multitv.cipher.MultitvCipher;
import com.multitv.ott.R;
import com.multitv.ott.Utils.CachingCategoryUtils;
import com.multitv.ott.Utils.ConnectionUtils;
import com.multitv.ott.Utils.Json;
import com.multitv.ott.activity.MoreActivity;
import com.multitv.ott.adapter.VideoCategoryContentAdapter;
import com.multitv.ott.adapter.VideoContentAdapter;
import com.multitv.ott.api.ApiRequest;
import com.multitv.ott.controller.AppController;
import com.multitv.ott.listeners.OnLoadChildCategory;
import com.multitv.ott.listeners.OnLoadMoreListener;
import com.multitv.ott.listeners.VodAdapterActionListener;
import com.multitv.ott.models.CategoryVod.CategoryContent;
import com.multitv.ott.models.CategoryVod.CategoryContentObj;
import com.multitv.ott.models.CategoryVod.VideoMain;
import com.multitv.ott.models.categories.Child;
import com.multitv.ott.models.categories.VOD;
import com.multitv.ott.sharedpreference.SharedPreference;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 01-02-2017.
 */

public class VideoFragment extends Fragment implements VodAdapterActionListener {

    private String TAG = "VideoFragment";

    @BindView(R.id.video_fragment_recyclerView)
    protected RecyclerView parentRecyclerView;
    @BindView(R.id.video_fragment_progress_bar_top)
    protected ProgressBar progressBarTop;
    @BindView(R.id.video_fragment_progress_bar_bottom)
    protected ProgressBar progressBarBottom;
    @BindView(R.id.fragment_video_ll_empty)
    LinearLayout llNoRecordFound;
    @BindView(R.id.fragment_video_fl_container)
    FrameLayout flContainer;

    private SharedPreference sharedPreference;
    private VOD vod;
    private boolean isDataSettingProcessIsAlreadyGoingOn;
    private String user_id;
    private CategoryContentObj videoDataSaved, videoData;
    private VideoCategoryContentAdapter videoCategoryContentAdapter;
    private int numberOfMaxCategories = 3, numberOfCategoriesShown;
    private int itemsLeft = -1, startCount = 0, endCount = 0, current_page = 0, counter, childCounter, offset;
    private String parentCategoryId;

    private ArrayList<VideoMain> videoMainArrayList = new ArrayList<>();
    private ArrayList<CategoryContent> contentArrayList = new ArrayList<>();
    private CategoryContentObj videoContent, contentSaved;

    public static final String EXTRA_VIDEO = "EXTRA_VOD";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //setRetainInstance(true);
        View rootview = inflater.inflate(R.layout.fragment_video, container, false);
        ButterKnife.bind(this, rootview);
        sharedPreference = new SharedPreference();
        user_id = sharedPreference.getPreferencesString(getActivity(), "user_id" + "_" + ApiRequest.TOKEN);

        if (getChildFragmentManager() != null) {
            for (int i = 0; i < getChildFragmentManager().getBackStackEntryCount(); ++i) {
                getChildFragmentManager().popBackStack();
            }
        }

        return rootview;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            vod = (VOD) bundle.getSerializable(EXTRA_VIDEO);
            parentCategoryId = vod.id;
        }
        if (getActivity() == null)
            return;

        String offsetString = sharedPreference.getPreferencesString(getActivity(), "video_offset_" + parentCategoryId);
        if (!TextUtils.isEmpty(offsetString))
            offset = Integer.parseInt(offsetString);

        setVodCategory();
        if (vod != null && vod.children != null && vod.children.size() > 0) {
            videoDataLoad(parentCategoryId, 0);
        } else {
            llNoRecordFound.setVisibility(View.VISIBLE);
            parentRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        current_page = 0;
        itemsLeft = -1;
        startCount = 0;
        endCount = 0;
    }

    private void setVodCategory() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        parentRecyclerView.setLayoutManager(linearLayoutManager);
        parentRecyclerView.setHasFixedSize(true);
        videoCategoryContentAdapter = new VideoCategoryContentAdapter(getActivity(), videoMainArrayList, parentRecyclerView, VideoFragment.this);
        parentRecyclerView.setAdapter(videoCategoryContentAdapter);

        videoCategoryContentAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                current_page = current_page + 1;
                videoDataLoad(parentCategoryId, 1);
            }
        });
    }

    private void videoDataLoad(final String cat_id, final int tag) {
        if (isDataSettingProcessIsAlreadyGoingOn)
            return;
        isDataSettingProcessIsAlreadyGoingOn = true;
        if (getActivity() == null)
            return;
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (tag == 0) {
                    progressBarTop.setVisibility(View.VISIBLE);
                } else {
                    progressBarBottom.setVisibility(View.VISIBLE);
                }
            }
        });

        final String key = "Video" + "_" + cat_id;
        final Type contentObjectType = new TypeToken<CategoryContentObj>() {
        }.getType();
        Log.i("VIDEO_TAB", "videoDataLoad() for category id : " + cat_id);
        AppController.getInstance().getCacheManager().getAsync(key, CategoryContentObj.class, contentObjectType, new GetCallback() {
            @Override
            public void onSuccess(final Object object) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        videoDataSaved = (CategoryContentObj) object;
                        Log.i("VIDEO_TAB", "videoDataLoad() get cache video for category id : " + cat_id);
                        if (getActivity() == null)
                            return;
                        /*if (VersionUtils.getIsContentVersionChanged(getActivity(), videoDataSaved, key)) {
                            videoDataSaved = null;

                            CachingCategoryUtils.saveCategories(getActivity(), "" + categoryId, 0, 0);
                            isVersionUpdated = true;
                            itemsLeft = 0;
                        }*/

                        numberOfCategoriesShown = CachingCategoryUtils.getNumberOfCategoriesShown(getActivity(),
                                "" + cat_id);

                        boolean isNoNeedToFetchNewCategories = CachingCategoryUtils.isNoNeedToFetchNewCategories(getActivity(),
                                "" + cat_id, vod);
                        String paramCategoryIds = "";
                        if (vod != null && vod.children != null && vod.children.size() != 0) {

                            int endCount = numberOfCategoriesShown + numberOfMaxCategories;
                            if (endCount > vod.children.size())
                                endCount = vod.children.size();

                            for (int i = numberOfCategoriesShown; i < endCount; i++) {
                                Child child = vod.children.get(i);
                                if (i != endCount - 1)
                                    paramCategoryIds = paramCategoryIds + child.id + ",";
                                else
                                    paramCategoryIds = paramCategoryIds + child.id;
                            }
                        }
                        final String categoryIds = paramCategoryIds;
                        if (videoDataSaved != null && videoDataSaved.content != null && videoDataSaved.content.size() != 0
                                && ((itemsLeft == -1 && contentArrayList.size() == 0) || itemsLeft > 0)) {

                            videoData = videoDataSaved;
                            Log.i("VIDEO_TAB", "videoDataLoad() cache video not null for category id : " + cat_id);
                            if (contentArrayList.size() < videoDataSaved.content.size()) {
                                /*switch (contentType) {
                                    case Constant.TYPE_VIDEO:*/
                                if (vod != null && vod.children != null && vod.children.size() > 0) {
                                    if (itemsLeft == -1) {
                                        contentArrayList.clear();
                                        itemsLeft = numberOfCategoriesShown;
                                    }

                                    if (itemsLeft == numberOfCategoriesShown)
                                        startCount = 0;
                                    else
                                        startCount = startCount + numberOfMaxCategories;

                                    endCount = startCount + numberOfMaxCategories;
                                    if (endCount > vod.children.size())
                                        endCount = vod.children.size();

                                    for (int i = startCount; i < endCount; i++) {
                                        Child child = vod.children.get(i);

                                        for (CategoryContent content : videoDataSaved.content) {
                                            if (child.id.equalsIgnoreCase("" + content.category_ids.get(0)))
                                                contentArrayList.add(content);
                                        }
                                    }

                                    itemsLeft = itemsLeft - numberOfMaxCategories;
                                } else
                                    contentArrayList.addAll(videoDataSaved.content);
                                     /*   break;
                                }*/

                                if (getActivity() == null)
                                    return;
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        handlePagination(tag, false, key);
                                        isDataSettingProcessIsAlreadyGoingOn = false;
                                    }
                                });
                            } else {
                                if (getActivity() == null)
                                    return;
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (tag == 0) {
                                            progressBarTop.setVisibility(View.GONE);
                                        } else {
                                            progressBarBottom.setVisibility(View.GONE);
                                        }

                                        isDataSettingProcessIsAlreadyGoingOn = false;
                                        if (contentArrayList == null || contentArrayList.size() == 0) {
                                            llNoRecordFound.setVisibility(View.VISIBLE);
                                            parentRecyclerView.setVisibility(View.GONE);
                                        }
                                    }
                                });

                                Log.i("VIDEO_TAB", "videoDataLoad() content list size is less than cache video content size for category id : " + cat_id);
                            }
                        } else if (!ConnectionUtils.isNetworkConnected(getActivity())) {

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (tag == 0) {
                                        progressBarTop.setVisibility(View.GONE);
                                    } else {
                                        progressBarBottom.setVisibility(View.GONE);
                                    }

                                    videoCategoryContentAdapter.setLoaded();

                                    isDataSettingProcessIsAlreadyGoingOn = false;

                                    Log.i("VIDEO_TAB", "videoDataLoad() No Internet connection for category id : " + cat_id);
                                }
                            });
                        } else if (videoDataSaved == null || (!isNoNeedToFetchNewCategories || contentArrayList.size() == 0) && !TextUtils.isEmpty(paramCategoryIds)) {

                            if (getActivity() == null)
                                return;
                            Log.i("VIDEO_TAB", "videoDataLoad() get content from server for categories :" + categoryIds + " for category id : " + cat_id);
                            StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                                    ApiRequest.VIDEO_CAT_URL_CLIST, new Response.Listener<String>() {
                                @Override
                                public void onResponse(final String response) {
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.i("video-response---", response);
                                            try {
                                                JSONObject mObj = new JSONObject(response);
                                                if (mObj.optInt("code") == 1) {
                                                    MultitvCipher mcipher = new MultitvCipher();
                                                    String str = new String(mcipher.decryptmyapi(mObj.optString("result")));

                                                    videoData = Json.parse(str.trim(), CategoryContentObj.class);

                                                    sharedPreference.setPreferencesString(getActivity(), "video_offset_" + parentCategoryId, "" + videoData.offset);

                                                    if (getActivity() == null)
                                                        return;
                                                   /* switch (contentType) {
                                                        case Constant.TYPE_VIDEO:*/
                                                    if (vod != null && vod.children != null && vod.children.size() != 0) {
                                                        CachingCategoryUtils.saveCategories(getActivity(), "" + cat_id,
                                                                vod.children.size(),
                                                                numberOfCategoriesShown + numberOfMaxCategories >= vod.children.size() ? vod.children.size() : numberOfCategoriesShown + numberOfMaxCategories);
                                                    }
                                                           /* break;
                                                        case Constant.TYPE_MOVIES:
                                                        case Constant.TYPE_TV_SHOWS:
                                                            if (child != null && child.children != null && child.children.size() != 0) {
                                                                CachingCategoryUtils.saveCategories(getActivity(), "" + categoryId,
                                                                        child.children.size(),
                                                                        numberOfCategoriesShown + numberOfMaxCategories >= child.children.size() ? child.children.size() : numberOfCategoriesShown + numberOfMaxCategories);
                                                            }
                                                            break;
                                                    }*/

                                                    if (videoDataSaved != null) {
                                                        videoDataSaved.offset = videoData.offset;
                                                        videoDataSaved.content.addAll(videoData.content);
                                                        AppController.getInstance().getCacheManager().put(key, videoDataSaved);
                                                        //Tracer.error("CacheManager", key + " object saved successfully");
                                                    } else {
                                                        AppController.getInstance().getCacheManager().put(key, videoData);
                                                        //Tracer.error("CacheManager", key + " object saved successfully");
                                                    }

                                                    if (getActivity() == null)
                                                        return;
                                                    getActivity().runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            handlePagination(tag, true, key);

                                                            isDataSettingProcessIsAlreadyGoingOn = false;
                                                        }
                                                    });
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();

                                                isDataSettingProcessIsAlreadyGoingOn = false;
                                            }
                                            //Tracer.error("cat_id-parseing---", "" + cat_id);
                                        }
                                    }).start();
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    //Tracer.error("video", "Error: " + error.getMessage());
                                    if (tag == 0) {
                                        progressBarTop.setVisibility(View.GONE);
                                    } else {
                                        progressBarBottom.setVisibility(View.GONE);
                                    }

                                    videoCategoryContentAdapter.setLoaded();

                                    isDataSettingProcessIsAlreadyGoingOn = false;

                                    Log.i("VIDEO_TAB", "videoDataLoad() Response error Listener for category id : " + cat_id);
                                }
                            }) {
                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<>();

                                    //params.put("lang", LocaleHelper.getLanguage(getApplicationContext()));
                                    //params.put("m_filter", (PreferenceData.isMatureFilterEnable(getApplicationContext()) ? "" + 1 : "" + 0));

                                    params.put("device", "android");
                                    params.put("user_id", user_id);
                                    params.put("current_offset", "" + offset);

                                    params.put("cat_id", categoryIds);
                                    params.put("max_counter", "4");
                                   /* switch (contentType) {
                                        case Constant.TYPE_VIDEO:*/
                                    /*if (vod != null && vod.children != null && vod.children.size() != 0) {
                                        String categoryIds = "";

                                        int endCount = numberOfCategoriesShown + numberOfMaxCategories;
                                        if (endCount > vod.children.size())
                                            endCount = vod.children.size();

                                        for (int i = numberOfCategoriesShown; i < endCount; i++) {
                                            Child child = vod.children.get(i);
                                            if (i != endCount - 1)
                                                categoryIds = categoryIds + child.id + ",";
                                            else
                                                categoryIds = categoryIds + child.id;
                                        }
                                        params.put("cat_id", categoryIds);
                                        params.put("max_counter", "4");
                                               *//* if (categoryId == 869)
                                                    Log.e("Naseeb", "Cat_id " + cat_id + " , Offset = "
                                                            + offset + " , Category ids = " + categoryIds);*//*
                                    }*/
                                    /*else {
                                        params.put("max_counter", "10");
                                        params.put("cat_id", String.valueOf(cat_id));
                                    }*/
                                    params.put("cat_type", "video");
                                    return params;
                                }
                            };


                            AppController.getInstance().addToRequestQueue(jsonObjReq);
                        } else {
                            if (getActivity() == null)
                                return;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (tag == 0) {
                                        progressBarTop.setVisibility(View.GONE);
                                    } else {
                                        progressBarBottom.setVisibility(View.GONE);
                                    }

                                    isDataSettingProcessIsAlreadyGoingOn = false;

                                    if (contentArrayList == null || contentArrayList.size() == 0) {
                                        llNoRecordFound.setVisibility(View.VISIBLE);
                                        parentRecyclerView.setVisibility(View.GONE);
                                    }
                                }
                            });
                            Log.i("VIDEO_TAB", "videoDataLoad() final else block for category id : " + cat_id);
                        }
                    }
                }).start();
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void handlePagination(final int tag, boolean isFromApiRequest, final String key) {

        /*if (current_page == 0) {
            showNativeAd();
            featureBannerVideo();
        }*/

       /* if (isVersionUpdated) {
            isVersionUpdated = false;
            contentArrayList.clear();
        }*/

        if (isFromApiRequest) {
            if (videoData != null && videoData.content != null && videoData.content.size() != 0) {
                contentArrayList.addAll(videoData.content);
            }
        }

        if (contentArrayList != null && contentArrayList.size() != 0) {
          /*  switch (contentType) {
                case TYPE_VIDEO:*/
            if (vod != null && vod.children != null && vod.children.size() != 0)
                createVideoMainList(contentArrayList.size() - 1);
            else {//Adding blank categoryId for data having no categoryId
                VideoMain videoMain = new VideoMain();
                videoMain.title = "";
                videoMain.contentArrayList.addAll(contentArrayList);
                videoMainArrayList.add(videoMain);
            }
                    /*break;
                case Constant.TYPE_MOVIES:
                case Constant.TYPE_TV_SHOWS:
                    if (child != null && child.children != null && child.children.size() != 0)
                        createVideoMainList(contentArrayList.size() - 1);
                    else {//Adding blank categoryId for data having no categoryId
                        VideoMain videoMain = new VideoMain();
                        videoMain.title = "";
                        videoMain.contentArrayList.addAll(contentArrayList);
                        videoMainArrayList.add(videoMain);
                    }
                    break;
            }*/

            videoCategoryContentAdapter.notifyDataSetChanged();
            videoCategoryContentAdapter.setLoaded();
            parentRecyclerView.setVisibility(View.VISIBLE);
            llNoRecordFound.setVisibility(View.GONE);
        } else if (!isFromApiRequest) {
            CachingCategoryUtils.saveCategories(getActivity(), "" + parentCategoryId, 0, 0);
            AppController.getInstance().getCacheManager().put(key, null);
            itemsLeft = 0;

            videoDataLoad(parentCategoryId, tag);
            return;
        } else {
            parentRecyclerView.setVisibility(View.GONE);
            llNoRecordFound.setVisibility(View.VISIBLE);
        }

        if (tag == 0) {
            progressBarTop.setVisibility(View.GONE);
        } else {
            progressBarBottom.setVisibility(View.GONE);
        }
    }

    private void createVideoMainList(int contentArrayListSize) {
        String categoryId = "", categoryName = "", videoListCategoryId = "";
        List<Child> children = null;
        for (int i = counter; i < contentArrayListSize; i++) {
            if (TextUtils.isEmpty(categoryName) || TextUtils.isEmpty(videoListCategoryId)) {
               /* switch (contentType) {
                    case TYPE_VIDEO:*/
                for (int j = childCounter; j < vod.children.size(); j++) {
                    Child child = vod.children.get(j);
                    videoListCategoryId = "" + contentArrayList.get(i).category_ids.get(0);
                    if (videoListCategoryId.equalsIgnoreCase(child.id)) {
                        categoryId = child.id;
                        categoryName = child.name;
                        children = child.children;
                        childCounter = j + 1;
                        break;
                    }
                }
            }

            if (!videoListCategoryId.equalsIgnoreCase("" + contentArrayList.get(i).category_ids.get(0))) {
                VideoMain videoMain = new VideoMain();
                videoMain.title = categoryName;
                videoMain.id = categoryId;
                videoMain.contentArrayList.addAll(contentArrayList.subList(counter, i));
                if (children != null)
                    videoMain.children.addAll(children);
                videoMainArrayList.add(videoMain);
                counter = i;
                categoryName = "";
                categoryId = "";
                children = null;
                videoListCategoryId = "";
                /*break;*/
            } else if (i == contentArrayListSize - 1) {
                VideoMain videoMain = new VideoMain();
                videoMain.title = categoryName;
                videoMain.id = categoryId;
                videoMain.contentArrayList.addAll(contentArrayList.subList(counter, i + 2));
                if (children != null)
                    videoMain.children.addAll(children);
                videoMainArrayList.add(videoMain);
                counter = i;
                categoryName = "";
                categoryId = "";
                videoListCategoryId = "";
                children = null;
                counter = counter + 2;
            }
        }

     /*   if (counter <= contentArrayListSize - 1)
            createVideoMainList(contentArrayListSize);
        else
            counter++;*/
    }

    @Override
    public void onGetChildList(VideoMain videoMain) {
        if (getActivity() == null)
            return;

       /* if (getActivity() instanceof OnLoadChildCategory) {
            ((OnLoadChildCategory) getActivity()).onLoadChildCategoryFragment(videoMain);
        }*/

        VideoChildFragment videoChildFragment = new VideoChildFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(VideoChildFragment.EXTRA_VIDEO_OBJECT, videoMain);
        videoChildFragment.setArguments(bundle);
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        ft.replace(flContainer.getId(), videoChildFragment).addToBackStack("sub cattegory");
        ft.commitAllowingStateLoss();
        flContainer.setVisibility(View.VISIBLE);
    }

    public void doInvisibleContainer() {
        flContainer.setVisibility(View.GONE);
    }

    @Override
    public void onSetContentInContentRecyclerView(final RecyclerView recyclerView, final ProgressBar progressBar, final List<CategoryContent> childContentList, final String categoryId) {
        if (getActivity() == null)
            return;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);
        final VideoContentAdapter videoContentAdapter = new VideoContentAdapter(getActivity(), childContentList, recyclerView);
        recyclerView.setAdapter(videoContentAdapter);

        videoContentAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                int offset = recyclerView.getAdapter().getItemCount();
                getContentForCategory(offset, categoryId, childContentList, progressBar, videoContentAdapter);
                // vodAdapterActionListener.onLoadMoreContent(offset, videoMain.id);
            }
        });
    }

    private void getContentForCategory(final int childOffset, final String categoryId, final List<CategoryContent> childContentList, final ProgressBar progressBar, final VideoContentAdapter videoContentAdapter) {
        final String key = "Content" + "_" + categoryId;
        final Type contentObjectType = new TypeToken<CategoryContentObj>() {
        }.getType();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });
        AppController.getInstance().getCacheManager().getAsync(key, CategoryContentObj.class, contentObjectType, new GetCallback() {
            @Override
            public void onSuccess(final Object object) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        contentSaved = (CategoryContentObj) object;

                        if (getActivity() == null)
                            return;
                        /*if (VersionUtils.getIsContentVersionChanged(getActivity(), videoDataSaved, key)) {
                            videoDataSaved = null;

                            CachingCategoryUtils.saveCategories(getActivity(), "" + categoryId, 0, 0);
                            isVersionUpdated = true;
                            itemsLeft = 0;
                        }*/

                        if (contentSaved != null && contentSaved.content != null && contentSaved.content.size() != 0 &&
                                (childOffset <= contentSaved.totalCount || childOffset > contentSaved.totalCount)
                                && childContentList.size() < contentSaved.totalCount) {

                            videoContent = contentSaved;

                            if (childContentList.size() < videoContent.totalCount) {
                                childContentList.addAll(videoContent.content);

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (progressBar.getVisibility() == View.VISIBLE) {
                                            progressBar.setVisibility(View.GONE);
                                        }
                                        videoContentAdapter.notifyDataSetChanged();
                                    }
                                });
                            }
                        } else if (!ConnectionUtils.isNetworkConnected(getActivity())) {

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (progressBar.getVisibility() == View.VISIBLE) {
                                        progressBar.setVisibility(View.GONE);
                                    }
                                }
                            });
                        } else if (contentSaved == null || childOffset < contentSaved.totalCount) {

                            if (getActivity() == null)
                                return;
                            StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                                    ApiRequest.VIDEO_CAT_URL_CLIST, new Response.Listener<String>() {
                                @Override
                                public void onResponse(final String response) {
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.i("video-response---", response);
                                            try {
                                                JSONObject mObj = new JSONObject(response);
                                                if (mObj.optInt("code") == 1) {
                                                    MultitvCipher mcipher = new MultitvCipher();
                                                    String str = new String(mcipher.decryptmyapi(mObj.optString("result")));

                                                    videoContent = Json.parse(str.trim(), CategoryContentObj.class);

                                                    if (getActivity() == null)
                                                        return;


                                                    if (contentSaved != null) {
                                                        contentSaved.offset = videoContent.offset;
                                                        contentSaved.content.addAll(videoContent.content);
                                                        AppController.getInstance().getCacheManager().put(key, contentSaved);
                                                    } else {
                                                        AppController.getInstance().getCacheManager().put(key, videoContent);
                                                    }

                                                    childContentList.addAll(videoContent.content);

                                                    if (getActivity() == null)
                                                        return;
                                                    getActivity().runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            if (progressBar.getVisibility() == View.VISIBLE) {
                                                                progressBar.setVisibility(View.GONE);
                                                            }
                                                            videoContentAdapter.notifyDataSetChanged();
                                                        }
                                                    });
                                                }

                                            } catch (Exception e) {
                                                e.printStackTrace();

                                            }
                                        }
                                    }).start();
                                }
                            }, new Response.ErrorListener() {

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    if (progressBar.getVisibility() == View.VISIBLE) {
                                        progressBar.setVisibility(View.GONE);
                                    }

                                    videoCategoryContentAdapter.setLoaded();
                                }
                            }) {
                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<>();

                                    //params.put("lang", LocaleHelper.getLanguage(getApplicationContext()));
                                    //params.put("m_filter", (PreferenceData.isMatureFilterEnable(getApplicationContext()) ? "" + 1 : "" + 0));

                                    params.put("device", "android");
                                    params.put("user_id", user_id);
                                    params.put("current_offset", "" + childOffset);
                                    params.put("max_counter", "10");
                                    params.put("cat_id", categoryId);
                                    params.put("cat_type", "video");
                                    return params;
                                }
                            };
                            AppController.getInstance().addToRequestQueue(jsonObjReq);
                        } else {
                            if (getActivity() == null)
                                return;
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (progressBar.getVisibility() == View.VISIBLE) {
                                        progressBar.setVisibility(View.GONE);
                                    }
                                }
                            });
                        }
                    }
                }).start();
            }

            @Override
            public void onFailure(Exception e) {

            }
        });
    }
}
