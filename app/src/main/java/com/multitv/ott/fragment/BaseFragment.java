package com.multitv.ott.fragment;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.multitv.ott.R;
import com.multitv.ott.activity.HomeActivity;
import com.multitv.ott.adapter.BasePagerAdapter;
import com.multitv.ott.adapter.LiveChannelCategoryAdapter;
import com.multitv.ott.adapter.VideoContentFragmentAdapter;
import com.multitv.ott.listeners.OnBackPressedListener;
import com.multitv.ott.models.BaseTabMenu;
import com.multitv.ott.models.categories.LiveCategory;
import com.multitv.ott.models.categories.VOD;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo on 01-02-2017.
 */

public class BaseFragment extends Fragment implements OnBackPressedListener {
    private ArrayList<BaseTabMenu> topMenuArrayList;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private BasePagerAdapter adapter;
    private Typeface tf;
    private List<VOD> vodList;
    public static final String EXTRA_CATEGORIES = "EXTRA_CATEGORIES";

    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_base, container, false);
        tf = Typeface.createFromAsset(getActivity().getAssets(), "fonts/app_customfonts.ttf");
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout);
        Bundle bundle = getArguments();
        if (bundle != null) {
            LiveCategory category = (LiveCategory) bundle.getSerializable(EXTRA_CATEGORIES);
            if (category != null) {
                vodList = category.vod;
            }
        }
        setdata();
        return view;
    }

    private void setdata() {
        topMenuArrayList = new ArrayList<>();
        BaseTabMenu baseTabMenu = new BaseTabMenu();
        baseTabMenu.pagerTitle = "Home";
        baseTabMenu.fragment = new HomeFragment();
        topMenuArrayList.add(baseTabMenu);

        baseTabMenu = new BaseTabMenu();
        baseTabMenu.pagerTitle = "Live";
        baseTabMenu.fragment = new LiveChannelCategoryFragment();
        topMenuArrayList.add(baseTabMenu);

        if (vodList != null && vodList.size() > 0)
            for (int i = 0; i < vodList.size(); i++) {
                baseTabMenu = new BaseTabMenu();
                baseTabMenu.pagerTitle = vodList.get(i).name;
                baseTabMenu.fragment = new VideoFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable(VideoFragment.EXTRA_VIDEO, vodList.get(i));
                baseTabMenu.fragment.setArguments(bundle);
                topMenuArrayList.add(baseTabMenu);
            }
        setupViewPager();
    }

    private void setupViewPager() {
        viewPager.setOffscreenPageLimit(1);
        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setTabsFromPagerAdapter(adapter);
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(tf);
                }
            }
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                Log.e("position", String.valueOf(position));

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    private void setupViewPager(ViewPager viewPager) {
        FragmentManager manager = getChildFragmentManager();
        adapter = new BasePagerAdapter(manager, getActivity(), topMenuArrayList);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        if (adapter != null) {
            Fragment fragment = adapter.getCurrentFragment(viewPager.getCurrentItem());
            /*if (fragment == null || !(fragment instanceof VideoTabFragment)) {
                System.out.println("BACKPRESSED" + fragment + " is not instance of VideoTabFragment");
                exitFromApp();
            }
            System.out.println("BACKPRESSED" + fragment + " is instance of VideoTabFragment");
            VideoContentFragmentAdapter videoContentFragmentAdapter = ((VideoTabFragment) fragment).getChildFragmentAdapter();
            if (videoContentFragmentAdapter == null) {
                exitFromApp();
            }

             Fragment childFragment = videoContentFragmentAdapter.getCurrentFragment(((VideoTabFragment) fragment).getViewPager().getCurrentItem());
            if (childFragment == null) {
                exitFromApp();
            }
             if (childFragment.getChildFragmentManager().getBackStackEntryCount() > 0) {
                if (childFragment.getChildFragmentManager().getBackStackEntryCount() == 1)
                    ((VideoFragment) childFragment).doInvisibleContainer();
                childFragment.getChildFragmentManager().popBackStack();
            } else {
                ((HomeActivity) getActivity()).finishApp();
            }*/
            if (fragment != null || fragment instanceof VideoFragment || fragment instanceof LiveChannelCategoryFragment) {
                if (fragment.getChildFragmentManager() != null && fragment.getChildFragmentManager().getBackStackEntryCount() > 0) {
                    if (fragment.getChildFragmentManager().getBackStackEntryCount() == 1) {
                        if (fragment instanceof VideoFragment)
                            ((VideoFragment) fragment).doInvisibleContainer();
                        else if (fragment instanceof LiveChannelCategoryFragment)
                            ((LiveChannelCategoryFragment) fragment).doInvisibleContainer();
                    }
                    fragment.getChildFragmentManager().popBackStack();
                } else {
                    exitFromApp();
                }
            } else {
                System.out.println("BACKPRESSED" + fragment + " is not instance of VideoFragment");
                exitFromApp();
            }
        } else {
            ((HomeActivity) getActivity()).finishApp();
        }
    }

    private void exitFromApp() {
        ((HomeActivity) getActivity()).finishApp();
        return;
    }
}
