package com.multitv.ott.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.multitv.cipher.MultitvCipher;
import com.multitv.ott.R;
import com.multitv.ott.Utils.Json;
import com.multitv.ott.adapter.MoreRecommendedAdapter;
import com.multitv.ott.adapter.UserRelatedContentAdapter;
import com.multitv.ott.api.ApiRequest;
import com.multitv.ott.controller.AppController;
import com.multitv.ott.listeners.OnLoadMoreListener;
import com.multitv.ott.models.recommendeds.Content;
import com.multitv.ott.models.recommendeds.Recommended;
import com.multitv.ott.sharedpreference.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import butterknife.BindView;
import butterknife.ButterKnife;
import static com.multitv.ott.Utils.ConstantVeqta.EXTRA_KEY;


/**
 * Created by root on 22/11/16.
 */

public class UserRelatedContentFragment extends Fragment {
    private int typeOfFragment;
    private Recommended recommended;
    private int count = 0;
    private UserRelatedContentAdapter profileContentAdapter;
    private String offsetString;
    private int current_page = 0;

    private SharedPreference sharedPreference;
    @BindView(R.id.empty_text)
    TextView empty_text;
    @BindView(R.id.favorite_recylerviw)
    RecyclerView moreRecylerView;
    @BindView(R.id.data_frame_layout)
    LinearLayout dataFrameLayout;
    private String user_id;
    private List<Content> displayArrayList = new ArrayList<>();
    @BindView(R.id.mProgress_bar_top)
    ProgressBar mProgress_bar_top;
    @BindView(R.id.empty)
    LinearLayout noRecordFoundTV;
    @BindView(R.id.progress_bottom_loadmore)
    ProgressBar progress_bottom_loadmore;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview;
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_user_related_content, container, false);
        ButterKnife.bind(this, rootview);
        sharedPreference = new SharedPreference();
        if (getActivity() == null)
            return null;
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mProgress_bar_top.getIndeterminateDrawable().setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY);
        user_id = new SharedPreference().getPreferencesString(getActivity(), "user_id" + "_" + ApiRequest.TOKEN);

        Bundle arguments = getArguments();
        if (arguments != null) {
            typeOfFragment = arguments.getInt(EXTRA_KEY);

            LinearLayoutManager gridLayoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            moreRecylerView.setLayoutManager(gridLayoutManager1);
            profileContentAdapter = new UserRelatedContentAdapter(getActivity(), displayArrayList, moreRecylerView, true);
            moreRecylerView.setAdapter(profileContentAdapter);
            Log.e("typeOfFragment Key", "profilelikes" + "_" + typeOfFragment);
            offsetString = sharedPreference.getPreferencesString(getActivity(), "profilelikes" + "_" + typeOfFragment);

            if (!TextUtils.isEmpty(offsetString))
                count = Integer.parseInt(offsetString);
            getHomeCatData(current_page);
            profileContentAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    current_page = current_page + 1;
                    Log.e("UserProfileContent","load-count-page"+"" + current_page);
                    count = recommended.offset;
                    getHomeCatData(current_page);

                }
            });

            if (typeOfFragment == 0) {
                empty_text.setText(getResources().getString(R.string.empty_watching));
            } else if (typeOfFragment == 1) {
                empty_text.setText(getResources().getString(R.string.empty_faveroite));
            } else if (typeOfFragment == 2) {
                empty_text.setText(getResources().getString(R.string.empty_likes));
            }
        }


        return rootview;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /*==============================get user data from user_related api================================================================
         ==========================================================================================================
              ============================================================================*/

    private void getHomeCatData(final int current_page) {
        if (current_page > 0) {
            progress_bottom_loadmore.setIndeterminate(true);
            progress_bottom_loadmore.setVisibility(View.VISIBLE);
        } else {
            mProgress_bar_top.setIndeterminate(true);
            mProgress_bar_top.setVisibility(View.VISIBLE);
        }

        if (recommended == null || count < recommended.totalCount) {
            if (getActivity() == null)
                return;
            StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                    ApiRequest.LIKES_USER_CONTENT, new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    try {
                        if (current_page > 0)
                            progress_bottom_loadmore.setVisibility(View.GONE);
                        else
                            mProgress_bar_top.setVisibility(View.GONE);
                        Log.e("UserProfileContent","***UserProfileContent***"+ response.toString());
                        JSONObject mObj = new JSONObject(response);

                        MultitvCipher mcipher = new MultitvCipher();
                        String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                        Log.e("UserProfileContent","***UserProfileContent***"+ str);
                        try {
                            JSONObject newObj = new JSONObject(str);
                            recommended = Json.parse(newObj.toString(), Recommended.class);

                        } catch (JSONException e) {
                            Log.e("JSON_ERROR", "UserProfileContent" + e.getMessage().toString());
                            
                        }
                        if (recommended == null || recommended.content == null || recommended.content.size() == 0) {
                            mProgress_bar_top.setVisibility(View.GONE);
                            moreRecylerView.setVisibility(View.GONE);
                            noRecordFoundTV.setVisibility(View.VISIBLE);
                        } else {
                            displayArrayList.addAll(recommended.content);

                        }
                        if (displayArrayList != null && displayArrayList.size() != 0) {
                            profileContentAdapter.setLoaded();
                            profileContentAdapter.notifyDataSetChanged();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Error", "Error: " + error.getMessage());
                    noRecordFoundTV.setVisibility(View.VISIBLE);
                    empty_text.setText(getResources().getString(R.string.network_error));
                    if (current_page > 0)
                        progress_bottom_loadmore.setVisibility(View.GONE);
                    else
                        mProgress_bar_top.setVisibility(View.GONE);
                }
            }) {

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();

                   // params.put("lang", LocaleHelper.getLanguage(getApplicationContext()));
                    //params.put("m_filter", (PreferenceData.isMatureFilterEnable(getApplicationContext()) ? "" + 1 : "" + 0));

                    params.put("current_version", "0.0");
                    params.put("max_counter", "10");
                    params.put("device", "android");
                    params.put("user_id", user_id);
                    params.put("current_offset", String.valueOf(count));

                    Log.e("current_version", "0.0");
                    Log.e("max_counter", "10");
                    Log.e("device", "android");
                    Log.e("user_id", user_id);
                    Log.e("current_offset", String.valueOf(count));

                    if (typeOfFragment == 0) {
                        params.put("type", "watching");
                        Log.e("***type****", "watching");
                    } else if (typeOfFragment == 1) {
                        params.put("type", "favorite");
                        Log.e("***type****", "favorite");
                    } else if (typeOfFragment == 2) {
                        params.put("type", "liked");
                        Log.e("***type****", "liked");
                    }

                    Set<String> keys = params.keySet();
                    for (String key : keys) {
                        Log.e("getFaveContent", "getHomeContent().getParams: " + key + "      " + params.get(key));
                    }
                    return params;
                }
            };

            // Adding request to request queue
            AppController.getInstance().addToRequestQueue(jsonObjReq);

        } else {
            if (displayArrayList != null && displayArrayList.size() != 0) {
                profileContentAdapter.setLoaded();
                profileContentAdapter.notifyDataSetChanged();
            }

            if (current_page > 0)
                progress_bottom_loadmore.setVisibility(View.GONE);
            else
                mProgress_bar_top.setVisibility(View.GONE);
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        current_page = 0;
    }

    /*==============================on Item click on recyclerview================================================================
         ==========================================================================================================
              ============================================================================*/

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}