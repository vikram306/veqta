package com.multitv.ott.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.multitv.ott.R;

/**
 * Created by Lenovo on 30-01-2017.
 */

public class SubscriptionFragment extends Fragment {
    public SubscriptionFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_subscription, container, false);
        return view;
    }


}