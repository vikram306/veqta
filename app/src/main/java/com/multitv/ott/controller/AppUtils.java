package com.multitv.ott.controller;

import android.util.Log;

/**
 * Created by Lenovo on 03-02-2017.
 */

public class AppUtils {
    public static boolean isValidMobile(String phone) {
        Log.e("vaildation mob", "isValidMobile: " + phone);
        String regexStr = "^[0-9]$";

        if (!android.util.Patterns.PHONE.matcher(phone).matches()) {
            return false;
        }
        if (phone.length() <= 9) {
            return false;
        }
        if (phone.length() == 14) {
            return phone.startsWith("0091");
        }
        if (phone.length() == 13) {
            return phone.startsWith("+91");
        }
        if (phone.length() == 11) {
            return phone.startsWith("0");
        }
        if (phone.length() > 14) {
            return false;
        }
        return true;
    }
}
