package com.multitv.ott.controller;

import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookSdk;
import com.google.firebase.analytics.FirebaseAnalytics;

import com.iainconnor.objectcache.CacheManager;
import com.multitv.ott.BuildConfig;
import com.multitv.ott.Utils.LruBitmapCache;
import com.iainconnor.objectcache.DiskCache;

import java.io.File;



public class AppController extends MultiDexApplication {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.

    public static final String TAG = AppController.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private CacheManager cacheManager;
    private FirebaseAnalytics firebaseAnalytics;
    private static AppController mInstance;
    private int triviaInprocess = 0;
    private int triviaCompleted = 0;

    public int getTriviaInprocess() {
        return triviaInprocess;
    }

    public void setTriviaInprocess(int triviaInprocess) {
        this.triviaInprocess = triviaInprocess;
    }

    public int getTriviaCompleted() {
        return triviaCompleted;
    }

    public void setTriviaCompleted(int triviaCompleted) {
        this.triviaCompleted = triviaCompleted;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //Fabric.with(this, new Crashlytics());
        FacebookSdk.sdkInitialize(getApplicationContext());
        mInstance = this;
        // LeakCanary.install(this);
        MultiDex.install(this);
        // LocaleHelper.onCreate(this);
        //ConnectionManager.getInstance(this).startConnectionTracking();
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        Log.e(TAG, "AppController.addToRequestQueue() " + req);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


    public CacheManager getCacheManager() {
        if (cacheManager == null) {
            try {
                File cacheFile = new File(getFilesDir() + File.separator + getPackageName());
                DiskCache diskCache = new DiskCache(cacheFile, BuildConfig.VERSION_CODE, 1024 * 1024 * 10);
                cacheManager = CacheManager.getInstance(diskCache);
            } catch (Exception e) {

                Log.e("cacheManager",""+e.getMessage());
            }
        }

        return this.cacheManager;
    }

    /*private static RequestQueue prepareSerialRequestQueue(Context context) {
        Cache cache = new DiskBasedCache(context.getCacheDir(), MAX_CACHE_SIZE);
        Network network = getNetwork();
        return new RequestQueue(cache, network, MAX_SERIAL_THREAD_POOL_SIZE);
    }


    private static Network getNetwork() {
        HttpStack stack;
        String userAgent = "volley/0";
        if(Build.VERSION.SDK_INT >= 9) {
            stack = new HurlStack();
        } else {
            stack = new HttpClientStack(AndroidHttpClient.newInstance(userAgent));
        }
        return new BasicNetwork(stack);
    }*/

}
