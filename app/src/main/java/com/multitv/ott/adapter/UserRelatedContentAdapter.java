package com.multitv.ott.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.multitv.ott.R;
import com.multitv.ott.activity.VeqtaPlayerActivity;
import com.multitv.ott.listeners.OnLoadMoreListener;
import com.multitv.ott.models.recommendeds.Content;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 05-03-2017.
 */

public class UserRelatedContentAdapter extends RecyclerView.Adapter {
    private String TAG="UserRelatedContentAdapter";
    private List<Content> itemsList;
    private Context mContext;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private OnLoadMoreListener onLoadMoreListener;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private RecyclerView recyclerView;

    public UserRelatedContentAdapter(Context context, List<Content> recomendedArrayList, RecyclerView mrecyclerView,
                                  boolean isNeedToSetScrollListener) {
        this.itemsList = recomendedArrayList;
        this.mContext = context;
        recyclerView = mrecyclerView;

        if(isNeedToSetScrollListener) {
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = recyclerView.getAdapter().getItemCount();

                    try {
                        lastVisibleItem = recyclerView.getChildAdapterPosition(recyclerView.getChildAt(recyclerView.getChildCount() - 1));
                    } catch (Exception e) {
                        Log.e("Error", "onScrolled: EXCEPTION " + e.getMessage());
                        lastVisibleItem = 0;
                    }

                    if (!loading && totalItemCount == (lastVisibleItem + 1)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }

                        loading = true;
                    }
                }
            });
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.row_user_relsted_content_adapter, parent, false);

            return new UserRelatedContentAdapter.SingleItemRowHolder(v);
        } else if (viewType == VIEW_PROG) {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.load_more_progress_item, parent, false);

            return new UserRelatedContentAdapter.ProgressViewHolder(v);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof UserRelatedContentAdapter.SingleItemRowHolder) {
            Content content = itemsList.get(position);
            ((UserRelatedContentAdapter.SingleItemRowHolder) holder).mTitle.setText(content.title);

            if (content.source != null && !content.source.isEmpty()) {
                String sourceType = content.source;
                if(sourceType.equals("mp4"))
                    ((UserRelatedContentAdapter.SingleItemRowHolder) holder).source_type.setText(mContext.getResources().getString(R.string.app_name));
                else
                    ((UserRelatedContentAdapter.SingleItemRowHolder) holder).source_type.setText(sourceType);

            }
            if(content.meta.genre !=null && ! content.meta.genre.trim().isEmpty()){
                ((UserRelatedContentAdapter.SingleItemRowHolder) holder).genre_sony.setVisibility(View.VISIBLE);
                ((UserRelatedContentAdapter.SingleItemRowHolder) holder).genre_sony.setText(content.meta.genre);
            } else
                ((UserRelatedContentAdapter.SingleItemRowHolder) holder).genre_sony.setVisibility(View.GONE);

            String duration = content.duration;
            if (duration != null && !duration.equals(null) &&!TextUtils.isEmpty(duration)) {
                //((UserRelatedContentAdapter.SingleItemRowHolder) holder).duration.setText(duration);

                String splitTime[]=duration.split(":");
                String hours=splitTime[0];
                String minutes=splitTime[1];
                String seconds=splitTime[2];

                if (hours.equals("00")){
                    ((UserRelatedContentAdapter.SingleItemRowHolder) holder).duration.setText(minutes+":"+seconds+" "+mContext.getResources().getString(R.string.minute));
                }else {
                    ((UserRelatedContentAdapter.SingleItemRowHolder) holder).duration.setText(hours+":"+minutes+" "+mContext.getResources().getString(R.string.hours));
                }
            } else {
                ((UserRelatedContentAdapter.SingleItemRowHolder) holder).duration.setVisibility(View.GONE);
            }


            if (content.thumbnail.large !=null && !content.thumbnail.large.isEmpty()) {
                Picasso
                        .with(mContext)
                        .load(content.thumbnail.large)
                        .placeholder(R.mipmap.place_holder)
                        .error(R.mipmap.place_holder)
                        .resize(((UserRelatedContentAdapter.SingleItemRowHolder) holder).mImageView.getWidth(), (int) mContext.getResources().getDimension(R.dimen._100sdp))
                        .into(((UserRelatedContentAdapter.SingleItemRowHolder) holder).mImageView);

            } else {
                Picasso.with(mContext)
                        .load(R.mipmap.place_holder)
                        .into(((UserRelatedContentAdapter.SingleItemRowHolder) holder).mImageView);

            }

            ((UserRelatedContentAdapter.SingleItemRowHolder) holder).card_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    goToPlayerActivity(position);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return itemsList == null ? 0 : itemsList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return itemsList.get(position) == null ? VIEW_PROG : VIEW_ITEM;
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        // @BindView(R.id.txt_tv_time)protected TextView mTime;
        @BindView(R.id.title_tv)protected TextView mTitle;
        @BindView(R.id.duration_tv)protected TextView duration;
        @BindView(R.id.video_thumbnail_iv)protected ImageView mImageView;
        @BindView(R.id.subtitle_tv)protected TextView source_type;
        @BindView(R.id.genre)protected TextView genre_sony;
        @BindView(R.id.card_view)protected CardView card_view;
        //  protected LinearLayout cardView;
        // protected ImageView itemImage;
        public SingleItemRowHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }

    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);

            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
            //progressBar1 = (ProgressBar) v.findViewById(R.id.progressBar2);
        }
    }

    public static int getScreenWidth(Context context) {
        int screenWidth;
        //if (screenWidth == 0) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        screenWidth = size.x;
        System.out.println("=====screenWidth========="+screenWidth);
        // }
        return screenWidth;
    }


    private void goToPlayerActivity(int position){
        if (!itemsList.isEmpty()) {
            String VIDEO_URL = itemsList.get(position).url;
            if (TextUtils.isEmpty(VIDEO_URL)) {
                Toast.makeText(mContext, "Cannot play this video", Toast.LENGTH_LONG).show();
                return;
            }

            String CONTENT_TYPE = itemsList.get(position).source;
            String des=itemsList.get(position).des;
            String duration=itemsList.get(position).duration;
            String content_id = itemsList.get(position).categoryIdsList.get(0).toString();
            String video_id = itemsList.get(position).id;
            String title = itemsList.get(position).title;
            String type = itemsList.get(position).type;
            String like = itemsList.get(position).likes;
            String thumbnail = itemsList.get(position).thumbnail.large;
            String fav_item = itemsList.get(position).favorite;
            String start_cast = itemsList.get(position).meta.star_cast;
            String likes_count = itemsList.get(position).likes_count;
            String social_like = itemsList.get(position).social_like;
            String social_view = itemsList.get(position).social_view;

            try {

                Intent videoIntent = new Intent(mContext, VeqtaPlayerActivity.class);
                videoIntent.putExtra("VIDEO_URL", VIDEO_URL);
                videoIntent.putExtra("descreption", des);
                videoIntent.putExtra("title", title);
                videoIntent.putExtra("type", type);
                videoIntent.putExtra("start_cast", start_cast);
                videoIntent.putExtra("likes_count", likes_count);
                videoIntent.putExtra("like", like);
                videoIntent.putExtra("video_id", video_id);
                videoIntent.putExtra("content_id", content_id);
                videoIntent.putExtra("thumbnail", thumbnail);
                videoIntent.putExtra("position", position);
                videoIntent.putExtra("fav_item", fav_item);
                videoIntent.putExtra("content_type", CONTENT_TYPE);
                videoIntent.putExtra("CONTENT_TYPE_MULTITV", "VOD");
                videoIntent.putExtra("SOCIAL_LIKES", social_like);
                videoIntent.putExtra("SOCIAL_VIEWS", social_view);
                mContext.startActivity(videoIntent);
            } catch (Exception ex) {
                Log.e("error", ex.getMessage());
            }
        } else {
            Log.e("error", "null list");
        }
    }
}
