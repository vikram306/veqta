package com.multitv.ott.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.multitv.ott.R;
import com.multitv.ott.custom.CustomTextView;
import com.multitv.ott.fragment.HomeFragment;
import com.multitv.ott.fragment.LiveChannelCategoryFragment;
import com.multitv.ott.fragment.VideoFragment;
import com.multitv.ott.fragment.VideoTabFragment;
import com.multitv.ott.models.BaseTabMenu;

import java.util.ArrayList;

/**
 * Created by Lenovo on 01-02-2017.
 */

public class BasePagerAdapter extends FragmentStatePagerAdapter {
    private static final String TAG = "BASE_TAG" + ".PagerAdapter";

    private Context context;
    private ArrayList<BaseTabMenu> topMenuArrayList;
    private Fragment[] currentFragment;

    public BasePagerAdapter(FragmentManager fm, Context context, ArrayList<BaseTabMenu> topMenuArrayList) {
        super(fm);
        this.context = context;
        this.topMenuArrayList = topMenuArrayList;
        currentFragment = new Fragment[topMenuArrayList.size()];
    }

    @Override
    public Fragment getItem(int position) {
        //Fragment frag = null;
        //String identifire = topMenuArrayList.get(position);
        /*switch (position) {
            case 0:
                frag = new HomeFragment();
                break;
            case 1:
                frag = new LiveChannelCategoryFragment();
                //bundle.putInt(VideoTabFragment.CONTENT_TYPE, VideoTabFragment.TYPE_VIDEO);
                break;

            case 2:
                frag = new VideoTabFragment();
                //bundle.putInt(VideoTabFragment.CONTENT_TYPE, VideoTabFragment.TYPE_VIDEO);
                break;

            default:
                frag = new HomeFragment();
                break;
        }*/
        Fragment frag = topMenuArrayList.get(position).fragment;
        currentFragment[position] = frag;
        return frag;
    }


    @Override
    public int getCount() {
        return topMenuArrayList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return topMenuArrayList.get(position).pagerTitle;
    }

    public View getTabView(int position) {
        // Given you have a custom layout in `res/layout/custom_tab_item.xml` with a TextView and ImageView
        View v = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);

        CustomTextView tv = (CustomTextView) v.findViewById(R.id.tab_item_txt);
        tv.setText(topMenuArrayList.get(position).pagerTitle);
        ImageView img = (ImageView) v.findViewById(R.id.tab_item_view);
        img.setVisibility(View.GONE);
        int widthAndHeightOfIcon = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, context.getResources().getDisplayMetrics());

      /*  Picasso.with(context)
                .load(topMenuArrayList.get(position).thumbnail)
                .placeholder(R.mipmap.place_holder).error(R.mipmap.place_holder)
                .resize(widthAndHeightOfIcon, widthAndHeightOfIcon)
                .into(img);*/

        return v;
    }

    public Fragment getCurrentFragment(int currentFrag) {
        return currentFragment[currentFrag];
    }
}
