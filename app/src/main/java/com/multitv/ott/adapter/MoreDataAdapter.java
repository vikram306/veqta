package com.multitv.ott.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.multitv.ott.R;
import com.multitv.ott.Utils.ScreenUtils;
import com.multitv.ott.activity.VeqtaPlayerActivity;
import com.multitv.ott.custom.CustomTextView;
import com.multitv.ott.models.Category;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 28-01-2017.
 */

public class MoreDataAdapter extends RecyclerView.Adapter<MoreDataAdapter.MoreItemViewHolder> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<Category> categories;

    public MoreDataAdapter(Context context, ArrayList<Category> categories) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        this.categories = categories;
    }

    @Override
    public MoreDataAdapter.MoreItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MoreDataAdapter.MoreItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.more_data_row, parent, false));
    }

    @Override
    public void onBindViewHolder(MoreItemViewHolder holder, final int position) {
       Typeface tf = Typeface.createFromAsset(mContext.getAssets(), "fonts/app_customfonts.ttf");
        Category item = categories.get(position);
        holder.main_container_bg.setLayoutParams(new RecyclerView.LayoutParams
                (ScreenUtils.getScreenWidth(mContext) / 2, RecyclerView.LayoutParams.WRAP_CONTENT));
        holder.imageView.setImageResource(item.getImageId());
        holder.textView.setText(item.getCatName());
        holder.textView.setTypeface(tf);
        holder.main_container_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext, VeqtaPlayerActivity.class);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    class MoreItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.ll_desc)
        LinearLayout llDesc;

        @BindView(R.id.category_icon)
        ImageView imageView;

        @BindView(R.id.tvdesc)
        CustomTextView textView;
        @BindView(R.id.card_view)
        CardView main_container_bg;

        public MoreItemViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);
        }
    }
}

