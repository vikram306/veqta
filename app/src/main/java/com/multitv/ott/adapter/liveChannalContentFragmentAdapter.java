package com.multitv.ott.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.multitv.ott.R;
import com.multitv.ott.activity.MoreActivity;
import com.multitv.ott.listeners.LiveCategoryInterfaces;
import com.multitv.ott.listeners.OnLoadMoreListener;
import com.multitv.ott.listeners.VodAdapterActionListener;
import com.multitv.ott.models.categories.LiveCatData;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 20-02-2017.
 */

public class liveChannalContentFragmentAdapter extends RecyclerView.Adapter<liveChannalContentFragmentAdapter.SingleItemRowHolder> {

    private List<LiveCatData> liveList;
    private Context mContext;
    private Date currentTime, date;
    private OnLoadMoreListener onLoadMoreListener;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    public LiveCategoryInterfaces liveCategoryInterfaces;
    private VodAdapterActionListener vodAdapterActionListener;

    public liveChannalContentFragmentAdapter(Context context, List<LiveCatData> liveList, RecyclerView recyclerView, LiveCategoryInterfaces liveCategoryInterfaces) {
        this.liveList = liveList;
        this.mContext = context;
        this.liveCategoryInterfaces = liveCategoryInterfaces;

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = recyclerView.getAdapter().getItemCount();
                try {
                    lastVisibleItem = recyclerView.getChildAdapterPosition(recyclerView.getChildAt(recyclerView.getChildCount() - 1));
                } catch (Exception e) {
                    Log.e("Error", "onScrolled: EXCEPTION " + e.getMessage());
                    lastVisibleItem = 0;
                }

                if (!loading && totalItemCount == (lastVisibleItem + 1)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                        Log.e("Live :", "LiveLoadMore:calling");
                    }

                    loading = true;
                }
            }
        });
    }

    @Override
    public liveChannalContentFragmentAdapter.SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.home_content_row, viewGroup, false);
        liveChannalContentFragmentAdapter.SingleItemRowHolder mh = new liveChannalContentFragmentAdapter.SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(liveChannalContentFragmentAdapter.SingleItemRowHolder holder, final int position) {
        final LiveCatData live = liveList.get(position);
        holder.header_name.setText(live.name);
        liveCategoryInterfaces.setLiveCategoryItemData(holder.recycler_view_list, holder.load_more_progress_bar, position, liveList.get(position).id, holder.llTitleHeader);

        /*if (live.children == null || live.children.size() <= 0)
            holder.btnMore.setVisibility(View.INVISIBLE);
        else holder.btnMore.setVisibility(View.VISIBLE);*/

        holder.header_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (live.children != null && live.children.size() > 0)
                    liveCategoryInterfaces.onGetChildList(live);
            }
        });

        holder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MoreActivity.class);
                intent.putExtra("more_data_tag", 2);
                intent.putExtra("catName", live.name);
                intent.putExtra("cat_id", live.id);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return liveList.size();
    }

    class SingleItemRowHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.content_row_ll_title)
        LinearLayout llTitleHeader;

        @BindView(R.id.header_name)
        protected TextView header_name;

        @BindView(R.id.recycler_view_list)
        protected RecyclerView recycler_view_list;

        @BindView(R.id.load_more_progress_bar)
        ProgressBar load_more_progress_bar;

        @BindView(R.id.more_btn)
        Button btnMore;

        public SingleItemRowHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }
   /* private void goToLivePlayerActivity(int position){
        if (liveList != null && liveList.size() != 0) {
            PlayerUtils.startPlayerActivity(mContext,
                    liveList.get(position).url, "", "", "n/a", "", liveList.get(position).id,
                    "LIVE", 4);
        }
    }*/

}
