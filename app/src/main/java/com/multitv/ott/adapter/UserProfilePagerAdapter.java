package com.multitv.ott.adapter;

/**
 * Created by root on 22/11/16.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.multitv.ott.custom.FragmentItemIdStatePagerAdapterWithSupport;
import com.multitv.ott.fragment.UserRelatedContentFragment;

import java.util.Arrays;

import static com.multitv.ott.Utils.ConstantVeqta.EXTRA_KEY;
import static com.multitv.ott.Utils.ConstantVeqta.EXTRA_VALUE_FAV;
import static com.multitv.ott.Utils.ConstantVeqta.EXTRA_VALUE_LIKE;
import static com.multitv.ott.Utils.ConstantVeqta.EXTRA_VALUE_WATCH;


/**
 * Created by Sunil .
 */
public class UserProfilePagerAdapter extends FragmentItemIdStatePagerAdapterWithSupport {

    private Context context;
    private int slideCount;
    private String[] data;

    public UserProfilePagerAdapter(FragmentManager manager, Context mContext, String[] data, int count) {
        super(manager);
        this.context = mContext.getApplicationContext();
        this.data = data;
        this.slideCount = count;
    }


    @Override
    public Fragment getItem(int position) {
        Log.e("UserPorfilePagerAdapter", ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> getItem: " + position);

        Fragment frag;
        Bundle bundle = new Bundle();
        switch (position) {
            case 0:
                bundle.putInt(EXTRA_KEY, EXTRA_VALUE_WATCH);
                break;
            case 1:
                bundle.putInt(EXTRA_KEY, EXTRA_VALUE_FAV);
                break;
            case 2:
                bundle.putInt(EXTRA_KEY, EXTRA_VALUE_LIKE);
                break;

            default:

        }
        frag = new UserRelatedContentFragment();
        frag.setArguments(bundle);
        return frag;
    }

    @Override
    public int getCount() {
        return slideCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        /*switch (position) {

            case 0:
                return   context.getResources().getString(R.string.watch_tab);

            case 1:
                return   context.getResources().getString(R.string.favorite_tab);

            case 2:
                return   context.getResources().getString(R.string.like_tab);
        }*/
        return data[position];
    }

    public int getItemPosition(Object item) {
        return POSITION_NONE;
    }

    public void setData(String[] data) {
        Arrays.fill(data, null);
        this.data = data;
        notifyDataSetChanged();
    }

}