package com.multitv.ott.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.multitv.ott.R;
import com.multitv.ott.Utils.ScreenUtils;
import com.multitv.ott.activity.VeqtaPlayerActivity;
import com.multitv.ott.listeners.CategoryOnLoadMoreListener;
import com.multitv.ott.listeners.OnLoadMoreListener;
import com.multitv.ott.models.CategoryVod.CategoryContent;
import com.multitv.ott.models.recommendeds.Content;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by cyberlinks on 7/3/17.
 */

public class VideoContentAdapter extends RecyclerView.Adapter<VideoContentAdapter.SingleItemRowHolder> {

    private final String TAG = VideoContentAdapter.class.getName();

    public List<CategoryContent> dataList = new ArrayList<>();
    private Context mContext;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;

    public VideoContentAdapter(Context context, List<CategoryContent> videoList,
                               final RecyclerView recyclerView) {
        this.mContext = context;
        dataList = videoList;


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = recyclerView.getAdapter().getItemCount();
                try {
                    lastVisibleItem = recyclerView.getChildAdapterPosition(recyclerView.getChildAt(recyclerView.getChildCount() - 1));
                } catch (Exception e) {
                    Log.e("Error", "onScrolled: EXCEPTION " + e.getMessage());
                    lastVisibleItem = 0;
                }

                if (!loading && totalItemCount == (lastVisibleItem + 1)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                        Log.e("Remommended :", "RemommendedLoadMore:calling");
                    }

                    loading = true;
                }
            }
        });
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.content_row, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder viewHolder, final int position) {
        CategoryContent live = dataList.get(position);

        int widthAndHeightOfIcon = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, mContext.getResources().getDisplayMetrics());

        viewHolder.cardView.setLayoutParams(new RecyclerView.LayoutParams
                (ScreenUtils.getScreenWidth(mContext) -widthAndHeightOfIcon, RecyclerView.LayoutParams.WRAP_CONTENT));

        /*viewHolder.cardView.setLayoutParams(new RecyclerView.LayoutParams
                (ScreenUtils.getScreenWidth(mContext) / 2, RecyclerView.LayoutParams.WRAP_CONTENT));*/
        viewHolder.titleTxt.setText(live.title);
        Picasso.with(mContext)
                .load(live.thumbnail.medium)
                .placeholder(R.mipmap.place_holder)
                .error(R.mipmap.place_holder)
                .resize(viewHolder.thumbnailImg.getWidth(), (int) mContext.getResources().getDimension(R.dimen._160sdp))
                .into(viewHolder.thumbnailImg);
        viewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPlayerActivity(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList == null ? 0 : dataList.size();
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.card_view)
        protected CardView cardView;
        @BindView(R.id.content_icon)
        protected ImageView thumbnailImg;
        @BindView(R.id.tvdesc)
        protected TextView titleTxt;

        public SingleItemRowHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    private void goToPlayerActivity(int position) {
        if (!dataList.isEmpty()) {
            String VIDEO_URL = dataList.get(position).url;
            if (TextUtils.isEmpty(VIDEO_URL)) {
                Toast.makeText(mContext, "Cannot play this video", Toast.LENGTH_LONG).show();
                return;
            }

            String CONTENT_TYPE = dataList.get(position).source;
            String des = dataList.get(position).des;
            String duration = dataList.get(position).duration;
            String content_id = dataList.get(position).category_ids.get(0);
            String video_id = dataList.get(position).id;
            String title = dataList.get(position).title;
            String type = "";
            if (dataList.get(position).meta != null)
                type = dataList.get(position).meta.type;
            String like = dataList.get(position).likes;
            String thumbnail = dataList.get(position).thumbnail.large;
            String fav_item = dataList.get(position).favorite;
            //String start_cast = recemmendedList.get(position).meta.star_cast;
            String likes_count = dataList.get(position).likes_count;
            String social_like = dataList.get(position).social_like;
            String social_view = dataList.get(position).social_view;


            Log.e(TAG, "***video-url***" + VIDEO_URL);
            Log.e(TAG, "***content_id***" + content_id);
            Log.e(TAG, "***content-type***" + CONTENT_TYPE);
            try {
                Intent videoIntent = new Intent(mContext, VeqtaPlayerActivity.class);
                videoIntent.putExtra("VIDEO_URL", VIDEO_URL);
                videoIntent.putExtra("descreption", des);
                videoIntent.putExtra("title", title);
                videoIntent.putExtra("type", type);
                //videoIntent.putExtra("start_cast", start_cast);
                videoIntent.putExtra("likes_count", likes_count);
                videoIntent.putExtra("like", like);
                videoIntent.putExtra("video_id", video_id);
                videoIntent.putExtra("content_id", content_id);
                videoIntent.putExtra("thumbnail", thumbnail);
                videoIntent.putExtra("position", position);
                videoIntent.putExtra("fav_item", fav_item);
                videoIntent.putExtra("content_type", CONTENT_TYPE);
                videoIntent.putExtra("CONTENT_TYPE_MULTITV", "VOD");
                videoIntent.putExtra("SOCIAL_LIKES", social_like);
                videoIntent.putExtra("SOCIAL_VIEWS", social_view);
                mContext.startActivity(videoIntent);
            } catch (Exception ex) {
                Log.e("error", ex.getMessage());
            }
        } else {
            Log.e("error", "null list");
        }
    }
}