package com.multitv.ott.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.multitv.ott.R;
import com.multitv.ott.activity.MoreActivity;
import com.multitv.ott.activity.VeqtaPlayerActivity;
import com.multitv.ott.custom.CustomTextView;
import com.multitv.ott.listeners.OnLoadMoreListener;
import com.multitv.ott.listeners.VodAdapterActionListener;
import com.multitv.ott.models.CategoryVod.VideoMain;
import com.multitv.ott.models.categories.Child;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 28-02-2017.
 */

public class VideoCategoryContentAdapter extends RecyclerView.Adapter<VideoCategoryContentAdapter.SingleItemRowHolder> {
    private Context mContext;
    private OnLoadMoreListener onLoadMoreListener;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private List<VideoMain> videoMainList;
    private VodAdapterActionListener vodAdapterActionListener;

    public VideoCategoryContentAdapter(Context context, List<VideoMain> videoMainList, RecyclerView recyclerView, VodAdapterActionListener vodAdapterActionListener) {
        //this.vod = vod;
        this.mContext = context;
        this.videoMainList = videoMainList;
        this.vodAdapterActionListener = vodAdapterActionListener;

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = recyclerView.getAdapter().getItemCount();
                try {
                    lastVisibleItem = recyclerView.getChildAdapterPosition(recyclerView.getChildAt(recyclerView.getChildCount() - 1));
                } catch (Exception e) {
                    Log.e("video_Error", "onScrolled: EXCEPTION " + e.getMessage());
                    lastVisibleItem = 0;
                }

                if (!loading && totalItemCount == (lastVisibleItem + 1)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                        Log.e("video :", "videoLoadMore:calling");
                    }
                    loading = true;
                }
            }
        });
    }

    @Override
    public VideoCategoryContentAdapter.SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.video_content_category_adapter_row, viewGroup, false);
        return new VideoCategoryContentAdapter.SingleItemRowHolder(view);
    }

    @Override
    public void onBindViewHolder(final VideoCategoryContentAdapter.SingleItemRowHolder holder, final int position) {
        //VOD vod = videoCategoryArrayList.get(position);
        final VideoMain videoMain = videoMainList.get(position);
        holder.tvTitle.setText(videoMain.title);
        vodAdapterActionListener.onSetContentInContentRecyclerView(holder.contentRecyclerView, holder.loadMoreProgressBar, videoMain.contentArrayList, videoMain.id);

        holder.tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (videoMain.children != null && videoMain.children.size() > 0)
                    vodAdapterActionListener.onGetChildList(videoMain);
            }
        });

        holder.btnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MoreActivity.class);
                intent.putExtra("more_data_tag", 3);
                intent.putExtra("catName", videoMain.title);
                intent.putExtra("cat_id", videoMain.id);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return videoMainList.size();
    }

    class SingleItemRowHolder extends RecyclerView.ViewHolder {
        //private Context context;

        @BindView(R.id.content_row_ll_title)
        LinearLayout llTitleHeader;

        @BindView(R.id.recycler_view_list)
        protected RecyclerView contentRecyclerView;

        @BindView(R.id.header_name)
        protected CustomTextView tvTitle;

        @BindView(R.id.load_more_progress_bar)
        ProgressBar loadMoreProgressBar;

        @BindView(R.id.more_btn)
        Button btnMore;

        public SingleItemRowHolder(View view) {
            super(view);
            //this.context = view.getContext();

            ButterKnife.bind(this, view);
        }
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }
}