package com.multitv.ott.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.daimajia.slider.library.SliderLayout;
import com.multitv.ott.activity.MoreActivity;
import com.multitv.ott.custom.CustomTextView;
import com.multitv.ott.R;
import com.multitv.ott.listeners.HomeLoadDataInterface;
import com.multitv.ott.listeners.OnLoadMoreListener;
import com.multitv.ott.models.home.Home_category;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by cyberlinks on 18/1/17.
 */

public class HomeAllDataListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Home_category> displayList = new ArrayList<>();
    private final int SLIDER_TYPE = 0;
    private final int CONTENT_TYPE = 1;
    private int lastVisibleItem, totalItemCount;
    private boolean isLoading;
    private OnLoadMoreListener onLoadMoreListener;
    public HomeLoadDataInterface homeLoadDataInterface;
    public SliderLayout featureBannerSlider;

    public HomeAllDataListAdapter(Context context, List<Home_category> displayList
            , HomeLoadDataInterface homeLoadDataInterface, RecyclerView recyclerView) {
        mContext = context;
        this.displayList = displayList;
        this.homeLoadDataInterface = homeLoadDataInterface;


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = recyclerView.getAdapter().getItemCount();

                try {
                    lastVisibleItem = recyclerView.getChildAdapterPosition(recyclerView.getChildAt(recyclerView.getChildCount() - 1));
                } catch (Exception e) {
                    Log.e("Error", "onScrolled: EXCEPTION " + e.getMessage());
                    lastVisibleItem = 0;
                }

                if (!isLoading && totalItemCount == (lastVisibleItem + 1)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                        Log.e("HomeAllListAdapter", "LoadMore Calling");
                    } else {
                        Log.e("HomeAllListAdapter", "LoadMore Calling null");
                    }

                    isLoading = true;
                }
            }
        });

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == SLIDER_TYPE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.pager_slider_layout, parent, false);
            return new SliderViewHolder(view);
        } else {
            View view = LayoutInflater.from(mContext).inflate(R.layout.home_content_row, parent, false);
            return new ItemViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
       /* if (position == 0) {
            //SliderViewHolder sliderViewHolder = (SliderViewHolder) holder;
            //homeLoadDataInterface.setFeatureBannerData(sliderViewHolder.slider);
        }else{*/
        if (holder.getItemViewType() != SLIDER_TYPE) {
            ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
            if (position == 1) {
                itemViewHolder.textView.setText("Live");
                itemViewHolder.textView.setAllCaps(true);
                homeLoadDataInterface.setLiveData(itemViewHolder.recyclerView, itemViewHolder.load_more_progress_bar, itemViewHolder.headerLinearbg);
                itemViewHolder.moreBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, MoreActivity.class);
                        intent.putExtra("more_data_tag", 2);
                        mContext.startActivity(intent);
                    }
                });
            } else if (position == 2) {
                itemViewHolder.textView.setText("Recommended");
                itemViewHolder.textView.setAllCaps(true);
                homeLoadDataInterface.setRecommendedData(itemViewHolder.recyclerView, itemViewHolder.load_more_progress_bar, itemViewHolder.headerLinearbg);
                itemViewHolder.moreBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, MoreActivity.class);
                        intent.putExtra("more_data_tag", 1);
                        mContext.startActivity(intent);
                    }
                });
            } else {
                itemViewHolder.textView.setText(displayList.get(position - 3).cat_name.toUpperCase());
                String cat_id = displayList.get(position - 3).cat_id;
                homeLoadDataInterface.setHomeCategoryItemData(itemViewHolder.recyclerView, itemViewHolder.load_more_progress_bar, position, cat_id, itemViewHolder.headerLinearbg);
                itemViewHolder.moreBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(mContext, MoreActivity.class);
                        intent.putExtra("more_data_tag", 3);
                        intent.putExtra("catName", displayList.get(position - 3).cat_name);
                        intent.putExtra("cat_id", displayList.get(position - 3).cat_id);
                        mContext.startActivity(intent);
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return displayList.size() + 3;
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.recycler_view_list)
        RecyclerView recyclerView;

        @BindView(R.id.header_name)
        CustomTextView textView;

        @BindView(R.id.more_btn)
        Button moreBtn;

        @BindView(R.id.load_more_progress_bar)
        ProgressBar load_more_progress_bar;

        @BindView(R.id.content_row_ll_title)
        LinearLayout headerLinearbg;

        public ItemViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    class SliderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.slider)
        SliderLayout slider;

        public SliderViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            HomeAllDataListAdapter.this.featureBannerSlider = slider;
        }
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return SLIDER_TYPE;
        else {
            return CONTENT_TYPE;
        }

    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        onLoadMoreListener = null;
        if (featureBannerSlider != null) {
            featureBannerSlider.removeAllSliders();
            featureBannerSlider = null;
        }
    }


    public void setLoaded() {
        isLoading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }


}
