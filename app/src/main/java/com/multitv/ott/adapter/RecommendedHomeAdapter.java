package com.multitv.ott.adapter;

/**
 * Created by Created by Sunil on 09-08-2016.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.multitv.ott.R;
import com.multitv.ott.Utils.ScreenUtils;
import com.multitv.ott.activity.VeqtaPlayerActivity;
import com.multitv.ott.listeners.CategoryOnLoadMoreListener;
import com.multitv.ott.models.recommendeds.Content;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RecommendedHomeAdapter extends RecyclerView.Adapter<RecommendedHomeAdapter.SingleItemRowHolder> {

    private List<Content> recemmendedList;
    public String TAG="RecommendedHomeAdapter";
    private Context mContext;
    private Date currentTime, date;
    private CategoryOnLoadMoreListener onLoadMoreListener;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;

    public RecommendedHomeAdapter(Context context, List<Content> recemmendedList, RecyclerView recyclerView) {
        this.recemmendedList = recemmendedList;
        this.mContext = context;

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = recyclerView.getAdapter().getItemCount();
                try {
                    lastVisibleItem = recyclerView.getChildAdapterPosition(recyclerView.getChildAt(recyclerView.getChildCount() - 1));
                } catch (Exception e) {
                    Log.e("Error", "onScrolled: EXCEPTION " + e.getMessage());
                    lastVisibleItem = 0;
                }

                if (!loading && totalItemCount == (lastVisibleItem + 1)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                        Log.e("Remommended :", "RemommendedLoadMore:calling");
                    }

                    loading = true;
                }
            }
        });
    }
    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.home_content_all_adapter_row, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, final int position) {
        Content homeRecommended = recemmendedList.get(position);
        int widthAndHeightOfIcon = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, mContext.getResources().getDisplayMetrics());

        holder.cardView.setLayoutParams(new RecyclerView.LayoutParams
                (ScreenUtils.getScreenWidth(mContext) -widthAndHeightOfIcon, RecyclerView.LayoutParams.WRAP_CONTENT));

        holder.titleTxt.setText(homeRecommended.title);

        if(!TextUtils.isEmpty(homeRecommended.thumbnail.medium)) {
            Picasso.with(mContext)
                    .load(homeRecommended.thumbnail.medium)
                    .placeholder(R.mipmap.place_holder)
                    .error(R.mipmap.place_holder)
                    .resize(holder.thumbnailImg.getWidth(), (int) mContext.getResources().getDimension(R.dimen._160sdp))
                    .into(holder.thumbnailImg);
        }else{
            Picasso.with(mContext)
                    .load(R.mipmap.place_holder)
                    .resize(holder.thumbnailImg.getWidth(), (int) mContext.getResources().getDimension(R.dimen._160sdp))
                    .into(holder.thumbnailImg);
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPlayerActivity(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return recemmendedList.size();
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.card_view)
        protected CardView cardView;
        @BindView(R.id.content_icon)
        protected ImageView thumbnailImg;
        @BindView(R.id.tvdesc)
        protected TextView titleTxt;

        public SingleItemRowHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(CategoryOnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    private void goToPlayerActivity(int position){
        if (!recemmendedList.isEmpty()) {
            String VIDEO_URL = recemmendedList.get(position).url;
            if (TextUtils.isEmpty(VIDEO_URL)) {
                Toast.makeText(mContext, "Cannot play this video", Toast.LENGTH_LONG).show();
                return;
            }

            String CONTENT_TYPE = recemmendedList.get(position).source;
            String des=recemmendedList.get(position).des;
            String duration=recemmendedList.get(position).duration;
            String content_id = recemmendedList.get(position).categoryIdsList.get(0);
            String video_id = recemmendedList.get(position).id;
            String title = recemmendedList.get(position).title;
            String type = recemmendedList.get(position).type;
            String like = recemmendedList.get(position).likes;
            String thumbnail = recemmendedList.get(position).thumbnail.large;
            String fav_item = recemmendedList.get(position).favorite;
            //String start_cast = recemmendedList.get(position).meta.star_cast;
            String likes_count = recemmendedList.get(position).likes_count;
            String social_like = recemmendedList.get(position).social_like;
            String social_view = recemmendedList.get(position).social_view;


            Log.e(TAG,"***video-url***" +VIDEO_URL);
            Log.e(TAG,"***content_id***"+content_id);
            Log.e(TAG,"***content-type***" +CONTENT_TYPE);
            try {
                Intent videoIntent = new Intent(mContext, VeqtaPlayerActivity.class);
                videoIntent.putExtra("VIDEO_URL", VIDEO_URL);
                videoIntent.putExtra("descreption", des);
                videoIntent.putExtra("title", title);
                videoIntent.putExtra("type", type);
                //videoIntent.putExtra("start_cast", start_cast);
                videoIntent.putExtra("likes_count", likes_count);
                videoIntent.putExtra("like", like);
                videoIntent.putExtra("video_id", video_id);
                videoIntent.putExtra("content_id", content_id);
                videoIntent.putExtra("thumbnail", thumbnail);
                videoIntent.putExtra("position", position);
                videoIntent.putExtra("fav_item", fav_item);
                videoIntent.putExtra("content_type", CONTENT_TYPE);
                videoIntent.putExtra("CONTENT_TYPE_MULTITV", "VOD");
                videoIntent.putExtra("SOCIAL_LIKES", social_like);
                videoIntent.putExtra("SOCIAL_VIEWS", social_view);
                mContext.startActivity(videoIntent);
            } catch (Exception ex) {
                Log.e("error", ex.getMessage());
            }
        } else {
            Log.e("error", "null list");
        }
    }

}