package com.multitv.ott.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.multitv.ott.Utils.ScreenUtils;
import com.multitv.ott.activity.VeqtaPlayerActivity;
import com.multitv.ott.custom.CustomTextView;
import com.multitv.ott.listeners.OnLoadMoreListener;
import com.multitv.ott.R;
import com.multitv.ott.models.recommendeds.Content;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by cyberlinks on 17/1/17.
 */

public class HomeDisplayItemWithHeaderAdapter extends RecyclerView.Adapter<HomeDisplayItemWithHeaderAdapter.ItemViewHolder> {
    public String TAG="HomeDisplayCategory";
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<Content> displayCategoryList;
    private Date currentTime, date;
    private OnLoadMoreListener onLoadMoreListener;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;

    public HomeDisplayItemWithHeaderAdapter(Context context, List<Content> disCategoryList, RecyclerView recyclerView) {
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContext = context;
        this.displayCategoryList = disCategoryList;

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = recyclerView.getAdapter().getItemCount();
                try {
                    lastVisibleItem = recyclerView.getChildAdapterPosition(recyclerView.getChildAt(recyclerView.getChildCount() - 1));
                } catch (Exception e) {
                    Log.e("Error", "onScrolled: EXCEPTION " + e.getMessage());
                    lastVisibleItem = 0;
                }

                if (!loading && totalItemCount == (lastVisibleItem + 1)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }

                    loading = true;
                }
            }
        });
    }

    @Override
    public HomeDisplayItemWithHeaderAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HomeDisplayItemWithHeaderAdapter.ItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_content_all_adapter_row, parent, false));
    }

    @Override
    public void onBindViewHolder(HomeDisplayItemWithHeaderAdapter.ItemViewHolder holder, final int position) {

        int widthAndHeightOfIcon = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, mContext.getResources().getDisplayMetrics());

        holder.cardView.setLayoutParams(new RecyclerView.LayoutParams
                (ScreenUtils.getScreenWidth(mContext) -widthAndHeightOfIcon, RecyclerView.LayoutParams.WRAP_CONTENT));
        Content item = displayCategoryList.get(position);
        holder.textView.setText(item.title);


        Picasso.with(mContext)
                .load(item.thumbnail.medium)
                .placeholder(R.mipmap.place_holder)
                .error(R.mipmap.place_holder)
                .resize(holder.imageView.getWidth(), (int) mContext.getResources().getDimension(R.dimen._160sdp))
                .into(holder.imageView);

        if(!TextUtils.isEmpty(item.thumbnail.medium)) {
            Picasso.with(mContext)
                    .load(item.thumbnail.medium)
                    .placeholder(R.mipmap.place_holder)
                    .error(R.mipmap.place_holder)
                    .resize(holder.imageView.getWidth(), (int) mContext.getResources().getDimension(R.dimen._160sdp))
                    .into(holder.imageView);
        }else{
            Picasso.with(mContext)
                    .load(R.mipmap.place_holder)
                    .resize(holder.imageView.getWidth(), (int) mContext.getResources().getDimension(R.dimen._160sdp))
                    .into(holder.imageView);
        }


        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToPlayerActivity(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return displayCategoryList.size();
    }

    class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.content_icon)
        ImageView imageView;

        @BindView(R.id.tvdesc)
        CustomTextView textView;

        @BindView(R.id.card_view)
        CardView cardView;

        public ItemViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);
        }
    }

    private void goToPlayerActivity(int position){
        if (!displayCategoryList.isEmpty()) {
            String VIDEO_URL = displayCategoryList.get(position).url;
            if (TextUtils.isEmpty(VIDEO_URL)) {
                Toast.makeText(mContext, "Cannot play this video", Toast.LENGTH_LONG).show();
                return;
            }

            String CONTENT_TYPE = displayCategoryList.get(position).source;
            String des=displayCategoryList.get(position).des;
            String duration=displayCategoryList.get(position).duration;
            String content_id = displayCategoryList.get(position).categoryIdsList.get(0);
            String video_id = displayCategoryList.get(position).id;
            String title = displayCategoryList.get(position).title;
            String type = displayCategoryList.get(position).type;
            String like = displayCategoryList.get(position).likes;
            String thumbnail = displayCategoryList.get(position).thumbnail.large;
            String fav_item = displayCategoryList.get(position).favorite;
            //String start_cast = displayCategoryList.get(position).meta.star_cast;
            String likes_count = displayCategoryList.get(position).likes_count;
            String social_like = displayCategoryList.get(position).social_like;
            String social_view = displayCategoryList.get(position).social_view;

            Log.e(TAG,"***video-url***" +VIDEO_URL);
            Log.e(TAG,"***content_id***"+content_id);
            Log.e(TAG,"***content-type***" +CONTENT_TYPE);
            try {

                Intent videoIntent = new Intent(mContext, VeqtaPlayerActivity.class);
                videoIntent.putExtra("VIDEO_URL", VIDEO_URL);
                videoIntent.putExtra("descreption", des);
                videoIntent.putExtra("title", title);
                videoIntent.putExtra("type", type);
                //videoIntent.putExtra("start_cast", start_cast);
                videoIntent.putExtra("likes_count", likes_count);
                videoIntent.putExtra("like", like);
                videoIntent.putExtra("video_id", video_id);
                videoIntent.putExtra("content_id", content_id);
                videoIntent.putExtra("thumbnail", thumbnail);
                videoIntent.putExtra("position", position);
                videoIntent.putExtra("fav_item", fav_item);
                videoIntent.putExtra("content_type", CONTENT_TYPE);
                videoIntent.putExtra("CONTENT_TYPE_MULTITV", "VOD");
                videoIntent.putExtra("SOCIAL_LIKES", social_like);
                videoIntent.putExtra("SOCIAL_VIEWS", social_view);
                mContext.startActivity(videoIntent);
            } catch (Exception ex) {
                Log.e("error", ex.getMessage());
            }
        } else {
            Log.e("error", "null list");
        }
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

}
