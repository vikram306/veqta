package com.multitv.ott.adapter;

/**
 * Created by Lenovo on 09-02-2017.
 */
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.multitv.ott.R;
import com.multitv.ott.custom.FragmentIdStatePagerAdapterWithoutSupport;
import com.multitv.ott.fragment.MultiTVMoreFragment;

import java.util.ArrayList;


/**
 * Created by naseeb on 11/29/2016.
 */

public class PlayerMoreContentFragmentAdapter extends FragmentIdStatePagerAdapterWithoutSupport {

    private ArrayList<String> titleArrayList;
    private String contentId;
    private int contentType;
    private Context context;
    private int[] tabIcons;
    private FragmentManager fragmentManager;

    public PlayerMoreContentFragmentAdapter(FragmentManager fragmentManager, Context context, int contentType, ArrayList<String> titleArrayList,
                                            String contentId) {
        super(fragmentManager);

        this.fragmentManager = fragmentManager;
        this.contentType = contentType;
        this.titleArrayList = titleArrayList;
        this.contentId = contentId;
        this.context = context;
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return titleArrayList.size();
    }

    @Override
    public Fragment getItem(int position) {
        return MultiTVMoreFragment.newInstance(position, contentId, contentType);
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return titleArrayList.get(position);
    }

    public View getTabView(int position) {
        // Given you have a custom layout in `res/layout/custom_tab_item.xml` with a TextView and ImageView
        View v = LayoutInflater.from(context).inflate(R.layout.custom_player_tabs, null);

        TextView tv = (TextView) v.findViewById(R.id.tab_item_txt);
        tv.setText(titleArrayList.get(position));
        tv.setTextColor(context.getResources().getColor(R.color.black));

        ImageView img = (ImageView) v.findViewById(R.id.tab_item_view);

        img.setImageResource(tabIcons[position]);

        return v;
    }


}