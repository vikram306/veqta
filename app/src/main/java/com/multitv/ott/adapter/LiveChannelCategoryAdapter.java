package com.multitv.ott.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.multitv.ott.R;
import com.multitv.ott.Utils.PlayerUtils;
import com.multitv.ott.Utils.ScreenUtils;
import com.multitv.ott.listeners.OnLoadMoreListener;
import com.multitv.ott.models.home.ContentHome;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 21-02-2017.
 */

public class LiveChannelCategoryAdapter extends RecyclerView.Adapter<LiveChannelCategoryAdapter.SingleItemRowHolder> {

    private List<ContentHome> liveList;
    private Context mContext;
    private Date currentTime, date;
    private OnLoadMoreListener onLoadMoreListener;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;

    public LiveChannelCategoryAdapter(Context context, List<ContentHome> liveList, RecyclerView recyclerView) {
        this.liveList = liveList;
        this.mContext = context;


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = recyclerView.getAdapter().getItemCount();
                try {
                    lastVisibleItem = recyclerView.getChildAdapterPosition(recyclerView.getChildAt(recyclerView.getChildCount() - 1));
                } catch (Exception e) {
                    Log.e("Error", "onScrolled: EXCEPTION " + e.getMessage());
                    lastVisibleItem = 0;
                }

                if (!loading && totalItemCount == (lastVisibleItem + 1)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                        Log.e("Live :", "LiveLoadMore:calling");
                    }

                    loading = true;
                }
            }
        });
    }

    @Override
    public LiveChannelCategoryAdapter.SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.home_content_all_adapter_row, null);
        LiveChannelCategoryAdapter.SingleItemRowHolder mh = new LiveChannelCategoryAdapter.SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(LiveChannelCategoryAdapter.SingleItemRowHolder holder, final int position) {
        ContentHome live = liveList.get(position);
        int widthAndHeightOfIcon = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 20, mContext.getResources().getDisplayMetrics());

        holder.cardView.setLayoutParams(new RecyclerView.LayoutParams
                (ScreenUtils.getScreenWidth(mContext) -widthAndHeightOfIcon, RecyclerView.LayoutParams.WRAP_CONTENT));
        holder.titleTxt.setText(live.title);
        Picasso.with(mContext)
                .load(live.thumbnail.medium)
                .placeholder(R.mipmap.place_holder)
                .error(R.mipmap.place_holder)
                .resize(holder.thumbnailImg.getWidth(), (int) mContext.getResources().getDimension(R.dimen._160sdp))
                .into(holder.thumbnailImg);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLivePlayerActivity(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return liveList.size();
    }

    static class SingleItemRowHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card_view)
        protected CardView cardView;
        @BindView(R.id.content_icon)
        protected ImageView thumbnailImg;
        @BindView(R.id.tvdesc)
        protected TextView titleTxt;

        public SingleItemRowHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void setLoaded() {
        loading = false;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }
    private void goToLivePlayerActivity(int position){
        if (liveList != null && liveList.size() != 0) {
            PlayerUtils.startPlayerActivity(mContext,
                    liveList.get(position).url, "", "", "n/a", "", liveList.get(position).id,
                    "LIVE", 4);
        }
    }

}