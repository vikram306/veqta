package com.multitv.ott.adapter;

/**
 * Created by Lenovo on 03-03-2017.
 */


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.multitv.ott.custom.FragmentItemIdStatePagerAdapterWithSupport;
import com.multitv.ott.fragment.HomeFragment;
import com.multitv.ott.fragment.LiveChannelCategoryFragment;
import com.multitv.ott.fragment.VideoFragment;
import com.multitv.ott.models.categories.VOD;

import java.util.List;


/*
 * Created by root on 28/9/16.
 */
public class VideoContentFragmentAdapter extends FragmentItemIdStatePagerAdapterWithSupport {
    private List<VOD> vodList;
    private Fragment[] currentFragment;

    public VideoContentFragmentAdapter(FragmentManager fragmentManager, List<VOD> vodList) {
        super(fragmentManager);
        this.vodList = vodList;
        currentFragment = new Fragment[this.vodList.size()];
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return vodList.size();
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        Bundle bundle = new Bundle();
        bundle.putSerializable(VideoFragment.EXTRA_VIDEO, vodList.get(position));
        switch (position) {
            case 0:
                fragment = new VideoFragment();
                fragment.setArguments(bundle);
                //fragment = VideoFragment.newInstance(vodList.get(position));
                break;
            default:
                //fragment = VideoFragment.newInstance(vodList.get(position));
                fragment = new VideoFragment();
                fragment.setArguments(bundle);
                break;
        }
        currentFragment[position] = fragment;
        return fragment;
    }

    // Returns the page title for the top indicator
    @Override
    public CharSequence getPageTitle(int position) {
        return String.valueOf(vodList.get(position).name);
    }

    public Fragment getCurrentFragment(int currentFrag) {
        return currentFragment[currentFrag];
    }
}

