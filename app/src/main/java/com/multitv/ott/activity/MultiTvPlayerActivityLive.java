package com.multitv.ott.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.multitv.multitvcommonsdk.MultiTVCommonSdk;
import com.multitv.multitvcommonsdk.permission.PermissionChecker;
import com.multitv.multitvcommonsdk.utils.MultiTVException;
import com.multitv.multitvplayersdk.MultiTvPlayer;
import com.multitv.ott.R;
import com.multitv.ott.api.ApiRequest;


public class MultiTvPlayerActivityLive extends Activity implements MultiTVCommonSdk.AdDetectionListner,
        MultiTvPlayer.MultiTvPlayerListner {

    private final String TAG = "MultiTvPlayerActivity";
    private MultiTvPlayer player;
    private String video_Url;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.transparent_black_eighty));
        }

        setContentView(R.layout.activity_player_live);

        Intent intent=getIntent();
        video_Url = intent.getStringExtra("VIDEO_URL");
        Log.e(TAG, "video_Url : " + video_Url);

        if (player != null) {
            player.release();
            player = null;
        }

        player = (MultiTvPlayer) findViewById(R.id.player);
        player.setContentType(MultiTvPlayer.ContentType.LIVE);
        player.setKeyToken(ApiRequest.TOKEN);
        player.setPreRollEnable(false);
        player.setContentFilePath(video_Url);
        player.setMultiTvPlayerListner(this);
        try {
            player.setAdSkipEnable(false, 5000);
            player.preparePlayer();
        } catch (MultiTVException e) {
           Log.e("LiveActivity",e.getMessage());
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            player.getLayoutParams().width = LinearLayout.LayoutParams.MATCH_PARENT;
            player.getLayoutParams().height = LinearLayout.LayoutParams.MATCH_PARENT;
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            player.getLayoutParams().width = LinearLayout.LayoutParams.MATCH_PARENT;
            player.getLayoutParams().height = (int)getResources().getDimension(R.dimen._250sdp);
        }

        player.onConfigurationChanged(newConfig);
    }

    @Override
    public void onAdCallback(String value, String session) {
        player.onAdCallback(value, session);
    }

    @Override
    public void onPlayerReady() {
        if(player != null) {
            try {
                player.start();
            } catch (Exception e) {
                Log.e("MultitvLiveActivity",e.getMessage());
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        player.setBackgrounded(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        player.setBackgrounded(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.release();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        PermissionChecker.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }
}
