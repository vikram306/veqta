package com.multitv.ott.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.firebase.iid.FirebaseInstanceId;
import com.multitv.cipher.MultitvCipher;
import com.multitv.ott.R;
import com.multitv.ott.Utils.AppConstants;
import com.multitv.ott.Utils.AppNetworkAlertDialog;
import com.multitv.ott.Utils.Json;
import com.multitv.ott.api.ApiRequest;
import com.multitv.ott.controller.AppController;
import com.multitv.ott.controller.SignUpController;
import com.multitv.ott.custom.CustomEditText;
import com.multitv.ott.listeners.SignUpListener;
import com.multitv.ott.listeners.UpdateLoginInformationListner;
import com.multitv.ott.models.User;
import com.multitv.ott.sharedpreference.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 * Created by Lenovo on 25-01-2017.
 */

public class LoginActivity extends AppCompatActivity implements SignUpListener, GoogleApiClient.OnConnectionFailedListener{
    private Dialog myDialog;
    private CustomEditText email_field,passwordField;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private ProgressBar progressBar;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions gso;
    private int RC_SIGN_IN = 3;
    private int loginThrough;
    private SharedPreference sharedPreference;
    private Intent intent;
    UpdateLoginInformationListner updateLoginInformationListner;
    private String mGender = "", mFirstName = "", mLastName = "", mUserName = "", mDob = "", mEmail = "", mPhoneNum,mPassword="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.transparent_black_ninty));
        }
        setContentView(R.layout.login_activity);
        email_field=(CustomEditText)findViewById(R.id.username);
        passwordField=(CustomEditText)findViewById(R.id.password);
        sharedPreference=new SharedPreference();
         String firstName=sharedPreference.getUSerName(LoginActivity.this,"first_name");
         String lastName=sharedPreference.getUSerLastName(LoginActivity.this,"last_name");
        if(!TextUtils.isEmpty(firstName))
        Log.e("LoginActivity","***FirsitName***"+firstName);

        if(!TextUtils.isEmpty(lastName))
        Log.e("LoginActivity","***lastName***"+lastName);

        progressBar=(ProgressBar)findViewById(R.id.progress_signin);
        initGoogleFb();
        intent=getIntent();
        String logoutKey=intent.getStringExtra("logout");
        if(!TextUtils.isEmpty(logoutKey)){
            String LoginFrom=sharedPreference.getFromLogedIn(LoginActivity.this,"fromLogedin");
            if(LoginFrom.equals("facebook")) {
                Log.e("FromLoginStatus","facebook");
                progressBar.setVisibility(View.GONE);
                logOutFromFacebook();
            } else if(LoginFrom.equals("google")) {
                Log.e("FromLoginStatus",LoginFrom);
                progressBar.setVisibility(View.GONE);
                logOutFromGoogle();
            } else if(LoginFrom.equals("veqta")) {
                Log.e("FromLoginStatus",LoginFrom);
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Logout successfully.", Toast.LENGTH_LONG).show();
                sharedPreference.setFromLogedIn(LoginActivity.this, "fromLogedin", "");
            }
        }


    }
    public void forgetPasswordClick(View v){
        showForgotPaswordDailog();
    }
    public void signUpClick(View v){
        Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        LoginActivity.this.finish();
    }

    public void loginFromFacebook(View v){
        if (!AppNetworkAlertDialog.isNetworkConnected(LoginActivity.this)) {
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
            return;
        }else{
            fb_login();
        }
    }
    public void loginFromGoogle(View v){
        if (!AppNetworkAlertDialog.isNetworkConnected(LoginActivity.this)) {
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
            return;
        }else{
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    }

    public void logOutFromGoogle(){
        progressBar.setVisibility(View.GONE);
        if(mGoogleApiClient.isConnected())
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        Toast.makeText(getApplicationContext(), "logout susccessfully", Toast.LENGTH_SHORT).show();
                        sharedPreference.setFromLogedIn(LoginActivity.this, "fromLogedin", "");
                    }
                });
    }
    public void logOutFromFacebook() {
        LoginManager.getInstance().logOut();
        progressBar.setVisibility(View.GONE);
        sharedPreference.setFromLogedIn(LoginActivity.this, "fromLogedin", "");
        Toast.makeText(getApplicationContext(),"Logout successfully.",Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    public void SignInButtongoToHome(View v){
        hideKeyboard(v);
        if (!AppNetworkAlertDialog.isNetworkConnected(LoginActivity.this)) {
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
            return;
        }else {
            if (validate()) {
                String password = passwordField.getText().toString();
                if (!TextUtils.isEmpty(password)) {
                    mPassword = password;
                }
                String email = email_field.getText().toString();
                if (!TextUtils.isEmpty(email)) {
                    mEmail = email;
                }
                //String password = etPassword.getText().toString();
                loginThrough = AppConstants.LOGIN_THROUGH_MULTITV;
                if (!AppNetworkAlertDialog.isNetworkConnected(LoginActivity.this)) {
                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);
                    return;
                } else {
                    sendInfoToServerAndLoginResponse(mEmail,mPassword);
                }

            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            progressBar.setVisibility(View.VISIBLE);
            GoogleSignInResult googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(googleSignInResult);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void initGoogleFb() {
        loginButton = new LoginButton(this);
        callbackManager = CallbackManager.Factory.create();
        //=======google plus login from custom button click============================================================
        //  ===========================================================================
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                /*.requestIdToken(getString(R.string.default_web_client_id))*/
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    private void fb_login() {
        progressBar.setVisibility(View.VISIBLE);
        loginButton.setReadPermissions(new String[]{"email", "user_hometown", "user_likes", "public_profile"});
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //Toast.makeText(getActivity(), "login success", Toast.LENGTH_SHORT).show();
                String accessToken = loginResult.getAccessToken().getToken();
                Log.e("accessToken", accessToken);
                final GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject obj,
                                                    GraphResponse response) {
                                //etUserName.setText("");
                                //etEmail.setText("");
                                //etMobile.setText("");
                                //etPassword.setText("");
                                progressBar.setVisibility(View.GONE);

                                Log.e("facebookResponce===", response.toString());
                                try {
                                    final String firstName = obj.optString("first_name");
                                    final String lastName = obj.optString("last_name");
                                    final String gender = obj.optString("gender");
                                    final String mail_id = obj.optString("email");
                                    Log.e("id", obj.optString("id"));
                                    String id = obj.optString("id");

                                    if (!TextUtils.isEmpty(gender)) {
                                        mGender = gender;
                                    }
                                    String name = firstName;
                                    if (!TextUtils.isEmpty(lastName)) {
                                        name = name.concat(" " + lastName);
                                    }
                                    mFirstName = firstName;
                                    mLastName = lastName;
                                    mEmail = mail_id;
                                    mUserName = name;

                                    loginThrough = AppConstants.LOGIN_THROUGH_FB;
                                    updateInfoToServer("", mFirstName, mLastName, mGender, "", "", mUserName, mEmail, "", mDob, loginThrough, mPhoneNum);

                                    // profilePicUrlFromFb = data.getJSONObject("picture").getJSONObject("data").getString("url");
                                    Log.e("first_name", obj.optString("first_name"));
                                    Log.e("last_name", obj.optString("last_name"));
                                    Log.e("gender", obj.optString("gender"));
                                    Log.e("name", obj.optString("name"));
                                    Log.e("link", obj.optString("link"));
                                    Log.e("locale", obj.optString("locale"));
                                    Log.e("Email:", obj.optString("email"));
                                    if (!TextUtils.isEmpty(id)) {
                                        final String URL_FB_IMAGE = "http://graph.facebook.com/" + id + "/picture?type=large&redirect=false";

                                        AsyncTask.execute(new Runnable() {
                                            @Override
                                            public void run() {
                                                getProfileFromFb(URL_FB_IMAGE, firstName, lastName, gender, mail_id);
                                            }
                                        });

                                    }
                                    //sharedPreference.setPhoneNumber(getActivity(), "phone", "");
                                    sharedPreference.setFromLogedIn(LoginActivity.this, "fromLogedin", "facebook");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e("Fb-error: ", e.getMessage().toString());
                                    progressBar.setVisibility(View.GONE);
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,email,picture,gender,locale,first_name,last_name");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                System.out.println("onCancel");
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(FacebookException exception) {
                System.out.println("onError");
                progressBar.setVisibility(View.GONE);

                try {
                    Log.e("SignUpActivity", exception.getCause().toString());
                    Toast.makeText(LoginActivity.this, "" + exception.getCause(), Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Log.e("SignUpActivity", e.getMessage());
                }
            }
        });
        loginButton.performClick();
    }
    public void getProfileFromFb(String url, final String fname, final String lName, final String gender, final String email) {
        StringRequest jsonObjReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    Log.e("***FACEBOOK_IMAGE***", response);
                    JSONObject mObj = new JSONObject(response);
                    try {

                        JSONObject newObj = mObj.getJSONObject("data");
                        String profilePicUrlFromFb = newObj.optString("url");
                        Log.e("***FB_IMAGE_URL***", profilePicUrlFromFb);
                        if (!TextUtils.isEmpty(profilePicUrlFromFb))

                            //updateLoginInformationListner.setProfileImage(profilePicUrlFromFb);
                            sharedPreference.setImageUrl(LoginActivity.this, "imgUrl", profilePicUrlFromFb);
                       /* sharedPreference.setEmailId(LoginActivity.this,"email",email);
                        sharedPreference.setFirstName(LoginActivity.this,"email",fname);
                        sharedPreference.setLastName(LoginActivity.this,"email",lName);
                        sharedPreference.setGender(LoginActivity.this,"email",gender);*/

                    } catch (JSONException e) {

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", "Error: " + error.getMessage());

            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);

    }






    //==============google plus login handle result from getting user information==============================================
    //=========================================================================================
    private void handleSignInResult(GoogleSignInResult result) {
        Log.e("Result..... ", "handleSignInResult:" + result.getStatus().getStatusMessage());
        //progressBar.setVisibility(View.GONE);
        if (result.isSuccess()) {
            GoogleSignInAccount information = result.getSignInAccount();
            String personName = information.getDisplayName();
            String gmail_id = information.getEmail();
            String first_google_name = "", last_google_name = "";
            String[] splited = personName.split("\\s+");
            if (splited.length > 0) {
                first_google_name = splited[0];
                if (splited.length > 1)
                    last_google_name = splited[1];
            }
            if (!TextUtils.isEmpty(last_google_name)) {
                sharedPreference.setGoogleLoginLastName(this, "googleLastName", last_google_name);
            }

            sharedPreference.setFromLogedIn(this, "fromLogedin", "google");
            Log.e("person name", personName);
            Log.e("email", gmail_id);

            sharedPreference.setGoogleLoginUsername(this, "googleNAme", first_google_name);
            sharedPreference.setGoogleLoginEmail(this, "emailFromGoogle", gmail_id);
            String profilePic=null;
            if (information.getPhotoUrl() != null) {
                profilePic = information.getPhotoUrl().toString();
                //updateLoginInformationListner.setProfileImage(profilePic);
                sharedPreference.setGoogleLoginProfilePic(LoginActivity.this, "ImageGoogleProfile", profilePic);
                sharedPreference.setImageUrl(LoginActivity.this, "imgUrl", profilePic);
                Log.e("personPhotoUrl", profilePic);
            } else {
                Log.e("personPhotoUrl", "Empty");
            }
            mFirstName = first_google_name;
            mLastName = last_google_name;
            mUserName = personName;
            mEmail = gmail_id;
            loginThrough = AppConstants.LOGIN_THROUGH_GOOGLE;
            updateInfoToServer("", mFirstName, mLastName, mGender, "", "", mUserName, mEmail, "", mDob,
                    loginThrough, mPhoneNum);

        } else {
            // Toast.makeText(getActivity(), "" + result.getStatus(), Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
            Toast.makeText(LoginActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //mGoogleApiClient.stopAutoManage(this);
       // mGoogleApiClient.disconnect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.stopAutoManage(this);
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private void updateInfoToServer(String id, String fName, String lName, String gender, String link, String locale, String name, String email, String location, String dob, int loginThrough, String phoneNo) {
        /*if (!ConnectionManager.getInstance(getApplicationContext()).isConnected()) {
            ConnectionDetector.showNetworkNotConnectedDialog(SignUpActivity.this);
            return;
        }*/
        if (!(progressBar.getVisibility() == View.VISIBLE))
            progressBar.setVisibility(View.VISIBLE);
        SignUpController signUpController = new SignUpController(this, this);
        signUpController.sendInfoToServer(id, fName, lName, gender, link, locale, name, email, location, dob, loginThrough, phoneNo);
    }

    @Override
    public void onSuccess(String resultString) {
        if ((progressBar.getVisibility() == View.VISIBLE))
            progressBar.setVisibility(View.GONE);

        try {
            JSONObject mObj = new JSONObject(resultString);
            if (mObj.optInt("code") == 1) {
                MultitvCipher mcipher = new MultitvCipher();
                String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                Log.e("LoginActivity", str);
                User user = Json.parse(str.trim(), User.class);

                if (!TextUtils.isEmpty(user.gender))
                    mGender = user.gender;
                if (!TextUtils.isEmpty(user.first_name))
                    mFirstName = user.first_name;
                else mFirstName = "";
                if (!TextUtils.isEmpty(user.last_name))
                    mLastName = user.last_name;
                else mLastName = "";
                if (!TextUtils.isEmpty(user.dob) && !user.dob.equals("0000-00-00"))
                    mDob = user.dob;
                if (!TextUtils.isEmpty(user.email))
                    mEmail = user.email;
                if (!TextUtils.isEmpty(user.contact_no))
                    mPhoneNum = user.contact_no;


                    String provider=user.provider;
                   if(!TextUtils.isEmpty(provider)){
                       sharedPreference.setUserName(this, "first_name", mFirstName);
                       sharedPreference.setUserLastName(this, "last_name", mLastName);
                       sharedPreference.setDob(this, "dob", mDob);
                       sharedPreference.setEmailId(this, "email_id", mEmail);
                       sharedPreference.setPhoneNumber(this, "phone", mPhoneNum);

                       if (!TextUtils.isEmpty(user.image))
                           sharedPreference.setImageUrl(this, "ImageURl", user.image);

                       if (mGender.equalsIgnoreCase("male")) {
                           sharedPreference.setGender(this, "gender_id", "" + 0);
                       }else if (mGender.equalsIgnoreCase("female")){
                           sharedPreference.setGender(this, "gender_id", "" + 1);
                       }
                   }else{
                       sharedPreference.setEmailId(this, "email_id", mEmail);
                       sharedPreference.setFromLogedIn(LoginActivity.this, "fromLogedin", "veqta");
                   }


                Log.e("LOGIN", "onResponse: " + user.id);
                //PreferenceData.setAppSessionId(this, user.app_session_id);
                if (!TextUtils.isEmpty(user.id)) {
                    Log.e("user_id", user.id);
                    sharedPreference.setPreferencesString(this, "user_id" + "_" + ApiRequest.TOKEN, "" + user.id);
                    sharedPreference.setPreferenceBoolean(this, sharedPreference.KEY_IS_LOGGED_IN);
                    if (loginThrough == AppConstants.LOGIN_THROUGH_MULTITV && !TextUtils.isEmpty(user.otp)) {
                        Log.e("otp", user.otp);
                        checkOtp(user.otp,user.id);
                    } else {
                        sharedPreference.setPreferenceBoolean(this, sharedPreference.KEY_IS_OTP_VERIFIED);
                        moveToHomeScreen();
                    }
                }
                //FCMController.getInstance(getApplicationContext()).registerToken();
            } else {
                String error = mObj.optString("error");
                if (!TextUtils.isEmpty(error))
                    Toast.makeText(this, error, Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Log.e("error",""+e.getMessage());
        }
    }

    @Override
    public void onError() {
        if ((progressBar.getVisibility() == View.VISIBLE))
            progressBar.setVisibility(View.GONE);
    }
    private void checkOtp(final String otp, final String user_id) {
        if (!AppNetworkAlertDialog.isNetworkConnected(LoginActivity.this)) {
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
            return;
        }
        progressBar.setVisibility(View.VISIBLE);

        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                 ApiRequest.VERIFY_OTP, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("Verify_otp_api", response);

                progressBar.setVisibility(View.GONE);

                try {
                    JSONObject mObj = new JSONObject(response);
                    MultitvCipher mcipher = new MultitvCipher();
                    if (mObj.optInt("code") == 1) {
                        String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                        Log.e("Verify_otp_api", str);
                        sharedPreference.setPreferenceBoolean(LoginActivity.this, sharedPreference.KEY_IS_OTP_VERIFIED);
                        sharedPreference.setPreferenceBoolean(LoginActivity.this, sharedPreference.KEY_IS_LOGGED_IN);

                        User user = Json.parse(str.trim(), User.class);

                        if (!TextUtils.isEmpty(user.gender))
                            if (user.gender.equalsIgnoreCase("male"))
                                sharedPreference.setGender(LoginActivity.this, "gender_id", "" + 0);
                            else if (user.gender.equalsIgnoreCase("female"))
                                sharedPreference.setGender(LoginActivity.this, "gender_id", "" + 1);

                        sharedPreference.setEmailId(LoginActivity.this, "email_id", user.email);
                        sharedPreference.setUserName(LoginActivity.this, "first_name", user.first_name);
                        sharedPreference.setUserLastName(LoginActivity.this, "last_name", user.last_name);
                        sharedPreference.setPhoneNumber(LoginActivity.this, "phone", user.contact_no);
                        if (!TextUtils.isEmpty(user.dob) && !user.dob.equals("0000-00-00"))
                            sharedPreference.setDob(LoginActivity.this, "dob", user.dob);
                        sharedPreference.setPreferencesString(LoginActivity.this, "user_id" + "_" + ApiRequest.TOKEN, "" + user.id);
                        sharedPreference.setFromLogedIn(LoginActivity.this, "fromLogedin", "veqta");
                        moveToHomeScreen();
                    } else {
                        String error = new String(mcipher.decryptmyapi(mObj.optString("result")));
                        if (!TextUtils.isEmpty(error))
                            Toast.makeText(LoginActivity.this, error, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Log.e("Error",""+e.getMessage());
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Verify_otp_api", "Error: " + error.getMessage());

                progressBar.setVisibility(View.GONE);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("otp", otp);
                params.put("user_id", user_id);

                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    public void moveToHomeScreen(){
        Intent intent =new Intent(LoginActivity.this,HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        LoginActivity.this.finish();
    }


    private boolean validate() {
        boolean valid = true;
        String email = email_field.getText().toString();
        String password = passwordField.getText().toString();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (TextUtils.isEmpty(password)) {
            passwordField.setError("Password may not be blank");
            valid = false;
        }
        if(password.length() < 8){
            passwordField.setError("Password may be atleast 8 character");
            valid = false;
        }
        if(password.length() > 10){
            passwordField.setError("Password may be maximum 10 character");
            valid = false;
        }


        if (!email.matches(emailPattern)) {
            email_field.setError("Email id is not Vaild");
            valid = false;
        }

         if(TextUtils.isEmpty(email)){
             email_field.setError("Email may not be blank");
             valid = false;
         }
        return valid;
    }
    private void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void sendInfoToServerAndLoginResponse(final String email, final String password) {
        if (!AppNetworkAlertDialog.isNetworkConnected(LoginActivity.this)) {
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                ApiRequest.LOGIN_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("LoginActivity","LoginResponce-URL_:_"+ response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject mObj = new JSONObject(response);
                    if (mObj.optInt("code") == 1) {
                        String str=mObj.optString("result");
                        String otp=mObj.optString("otp");
                        String id=mObj.optString("id");
                        Log.e("LoginActivity","***code-1**"+str);
                        if (!TextUtils.isEmpty(otp)&&!TextUtils.isEmpty(id)) {
                            Log.e("otp", otp);
                            checkOtp(otp,id);
                        }else {
                            sharedPreference.setPreferenceBoolean(LoginActivity.this, sharedPreference.KEY_IS_OTP_VERIFIED);
                            moveToHomeScreen();
                        }
                    } else {
                        String error =mObj.optString("result");
                        Log.e("LoginActivity","***code-0**"+error);
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.invaild_password), Toast.LENGTH_LONG).show();
                        if (!TextUtils.isEmpty(error))
                            Toast.makeText(LoginActivity.this, error, Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    Log.e("LoginActivity","Error"+""+e.getMessage());
                    progressBar.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Log.e("LoginActivity","****LoginApi****"+ "Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                try {
                    final String model = Build.MODEL;
                    final String networkType = getNetType();

                    final String token = FirebaseInstanceId.getInstance().getToken();
                    if (token != null) {
                    }

                    final String android_id = ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
                    TelephonyManager manager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    final String carrierName = manager.getNetworkOperatorName();

                    final int os_version_code = android.os.Build.VERSION.SDK_INT;

                    DisplayMetrics displaymetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                    int height = displaymetrics.heightPixels;
                    int width = displaymetrics.widthPixels;

                    final String resolution = height + "*" + width;

                    String dodjson = "";
                    String ddjson = "";

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("os_version", String.valueOf(os_version_code));
                        jsonObject.put("app_version", "1.0");
                        jsonObject.put("network_type", networkType);
                        jsonObject.put("network_provider", carrierName);
                        dodjson = jsonObject.toString();
                    } catch (JSONException e) {
                        Log.e("LOGIN_PARAM", "getParams:1 " + e.getMessage());
                    }

                    JSONObject jsonObject1 = new JSONObject();
                    try {
                        jsonObject1.put("make_model", model);
                        jsonObject1.put("os", "android");
                        jsonObject1.put("screen_resolution", resolution);
                        jsonObject1.put("push_device_token", token == null ? "" : token);
                        jsonObject1.put("device_type", "mobile");
                        jsonObject1.put("platform", "android");
                        jsonObject1.put("device_unique_id", android_id);

                        ddjson = jsonObject1.toString();
                    } catch (JSONException e) {
                        Log.e("LOGIN_PARAM", "getParams:2 " + e.getMessage());
                    }

                    Map<String, String> params = new HashMap<>();

                    params.put("email", email);
                    params.put("password", password);
                    params.put("dd", ddjson);
                    params.put("dod", dodjson);
                    Set<String> keySet = params.keySet();
                    for (String key : keySet) {
                        Log.e("LOGIN_PARAM", "getParams: " + key + "   " + params.get(key));
                    }
                    return params;
                } catch (Exception e) {
                    Log.e("LoginApi1",e.getMessage());
                } catch (IncompatibleClassChangeError e) {
                    Log.e("LoginApi2",e.getMessage());
                }

                return null;
            }
        };
        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }
    public String getNetType() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        if (null != activeNetInfo) {
            Log.e("getNetType : ", activeNetInfo.toString());
            if (activeNetInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                return "wifi";
            } else if (activeNetInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                TelephonyManager mTelephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                int type = mTelephonyManager.getNetworkType();
                if (type == TelephonyManager.NETWORK_TYPE_UNKNOWN || type == TelephonyManager.NETWORK_TYPE_GPRS || type == TelephonyManager.NETWORK_TYPE_EDGE) {
                    return "mobile";
                } else {
                    return "Other";
                }
            }
        }
        return "No Internet Network";
    }

   //----------------------------------------------------------------------------------------------------------------------
    //---------------------------FORGOT PASSWORD-------------------------------------------------------------------------
    //--------------------------------------------------------------------------------------------------------------
   private void showForgotPaswordDailog(){
       Typeface tf = Typeface.createFromAsset(getAssets(),
               "fonts/app_customfonts.ttf");
       String first = "Don't have an account?";
       String next = "<font color='#0c6285'>Sign up</font>";

       myDialog = new Dialog(LoginActivity.this);
       myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
       myDialog.setContentView(R.layout.forgot_password_diaolg_screen);
       Button sendMailBtn=(Button)myDialog.findViewById(R.id.sendMailBtn);
       TextView donot_account=(TextView)myDialog.findViewById(R.id.donot_account);
       TextView messageText=(TextView)myDialog.findViewById(R.id.messageText);
       TextView back=(TextView)myDialog.findViewById(R.id.back);
       TextView title=(TextView)myDialog.findViewById(R.id.title);
       ImageView cancelBtn=(ImageView)myDialog.findViewById(R.id.cancelBtn);
       donot_account.setText(Html.fromHtml(first + next));
       title.setTypeface(tf);
       back.setTypeface(tf);
       messageText.setTypeface(tf);
       donot_account.setTypeface(tf);
       sendMailBtn.setTypeface(tf);

       final EditText input_mail_from_dalog=(EditText)myDialog.findViewById(R.id.input_mail_from_dalog);
       input_mail_from_dalog.setTypeface(tf);

       cancelBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               myDialog.dismiss();
           }
       });

       donot_account.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
               intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
               startActivity(intent);
               LoginActivity.this.finish();

           }
       });

       input_mail_from_dalog.setTextColor(getResources().getColor(R.color.search_shadow));
       sendMailBtn.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               String emailId=input_mail_from_dalog.getText().toString();
               if(!TextUtils.isEmpty(emailId)) {
                   sendPasswordOnMail(emailId);
               }else{
                   Toast.makeText(LoginActivity.this, "Please Enter Correct Email.", Toast.LENGTH_SHORT).show();
               }

           }
       });
       myDialog.show();

   }

    /*private void sendMail(String email,String passowrd){
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{email});
        i.putExtra(Intent.EXTRA_SUBJECT, "New Password");
        i.putExtra(Intent.EXTRA_TEXT   , passowrd);
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(LoginActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }*/

    private void sendPasswordOnMail(final String email){
        if (!AppNetworkAlertDialog.isNetworkConnected(LoginActivity.this)) {
            Toast.makeText(LoginActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
            return;
        }
        myDialog.dismiss();
        progressBar.setVisibility(View.VISIBLE);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                ApiRequest.FORGET_URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("LoginActivity","FORGOT-URL_:_"+ response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject mObj = new JSONObject(response);
                    if (mObj.optInt("code") == 1) {
                        String str=mObj.optString("result");
                        Toast.makeText(LoginActivity.this, str, Toast.LENGTH_LONG).show();
                        Log.e("LOGINACTIVITY","***FORGOT-URL-RESPONCE**"+str);

                    } else {
                        String error =mObj.optString("result");
                        if (!TextUtils.isEmpty(error))
                            Toast.makeText(LoginActivity.this, error, Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    Log.e("LoginActivity","FORGOT---****--Error"+""+e.getMessage());
                    progressBar.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("LoginActivity", "FORGOT-Error: " + error.getMessage());
                progressBar.setVisibility(View.GONE);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }



}