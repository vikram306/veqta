package com.multitv.ott.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.reflect.TypeToken;
import com.iainconnor.objectcache.GetCallback;
import com.multitv.cipher.MultitvCipher;
import com.multitv.ott.R;
import com.multitv.ott.Utils.ConnectionUtils;
import com.multitv.ott.Utils.CustomTypefaceSpan;
import com.multitv.ott.Utils.Json;
import com.multitv.ott.api.ApiRequest;
import com.multitv.ott.controller.AppController;
import com.multitv.ott.fragment.AboutUsFragment;
import com.multitv.ott.fragment.BaseFragment;
import com.multitv.ott.fragment.HelpFragment;

import com.multitv.ott.fragment.RateAppFragment;
import com.multitv.ott.fragment.SettingFragment;
import com.multitv.ott.fragment.SubscriptionFragment;
import com.multitv.ott.fragment.VideoChildFragment;
import com.multitv.ott.listeners.OnLoadChildCategory;
import com.multitv.ott.models.Category;
import com.multitv.ott.models.CategoryVod.VideoMain;
import com.multitv.ott.models.categories.Child;
import com.multitv.ott.models.categories.LiveCategory;
import com.multitv.ott.models.categories.VOD;
import com.multitv.ott.sharedpreference.SharedPreference;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.multitv.ott.Utils.ConstantVeqta.EXTRA_OPEN_HOME_SCREEN;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener /*, OnLoadChildCategory*/ {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.search_img)
    ImageView search_img;
    @BindView(R.id.logo_img)
    ImageView logo_img;
    @BindView(R.id.layout_main_pb)
    ProgressBar progressBar;

    private SharedPreference sharedPreference;
    private LiveCategory category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.transparent_black_ninty_five));
        }
        setContentView(R.layout.activity_main);
        sharedPreference = new SharedPreference();
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        try {
            Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/app_customfonts.ttf");
            for (int i = 0; i < toolbar.getChildCount(); i++) {
                if (toolbar.getChildAt(i) instanceof TextView) {
                    ((TextView) toolbar.getChildAt(i)).setTypeface(tf);
                }
            }
        } catch (Exception e) {
            Log.e("HomeActivity-toolbar", "onCreate: " + e.getMessage());
        }
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        //loadFragments();
        getCategories();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(HomeActivity.this);

        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            //the method we have create in activity
            applyFontToMenuItem(mi);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
            return;
        }

        Fragment ft = getSupportFragmentManager().findFragmentById(R.id.container);
        Log.i("onBackPressed", "" + getSupportFragmentManager().getBackStackEntryCount());
        if (ft instanceof BaseFragment) {
            ((BaseFragment) ft).onBackPressed();
            /*String msg = "Are you sure you want to exit from " + getResources().getString(R.string.app_name);
            showUpdateDialog(msg);*/
        } else {
            loadFragments(category);
            return;
            /*Log.i("onBackPressed", "" + getSupportFragmentManager().getBackStackEntryCount());
            super.onBackPressed();*/
        }
    }

    public void finishApp() {
        String msg = "Are you sure you want to exit from " + getResources().getString(R.string.app_name);
        showUpdateDialog(msg);
    }

    private void loadFragments(LiveCategory category) {
        logo_img.setVisibility(View.VISIBLE);
        search_img.setVisibility(View.VISIBLE);
        getSupportActionBar().setTitle("");
        Fragment baseFragment = new BaseFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(BaseFragment.EXTRA_CATEGORIES, category);
        baseFragment.setArguments(bundle);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, baseFragment).addToBackStack(null);
        ft.commitAllowingStateLoss();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getBooleanExtra(EXTRA_OPEN_HOME_SCREEN, false)) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            Fragment homeFragment = new BaseFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable(BaseFragment.EXTRA_CATEGORIES, category);
            homeFragment.setArguments(bundle);
            ft.replace(R.id.container, homeFragment).addToBackStack(null);
            ft.commitAllowingStateLoss();

            logo_img.setVisibility(View.VISIBLE);
            search_img.setVisibility(View.VISIBLE);
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        String title = item.getTitle().toString();
        Fragment fragment = null;
        if (id == R.id.nav_home) {
            fragment = new BaseFragment();
            // Pass video categories in bundle
            Bundle bundle = new Bundle();
            bundle.putSerializable(BaseFragment.EXTRA_CATEGORIES, category);
            fragment.setArguments(bundle);
            logo_img.setVisibility(View.VISIBLE);
            //getSupportActionBar().setTitle(title);
            search_img.setVisibility(View.VISIBLE);

        } else if (id == R.id.nav_favorite) {
            logo_img.setVisibility(View.VISIBLE);
            // fragment = new BaseFragment();
            search_img.setVisibility(View.VISIBLE);
            //Toast.makeText(HomeActivity.this,"Coming Soon...",Toast.LENGTH_LONG).show();
            Intent editProfile = new Intent(HomeActivity.this, UserProfileActivity.class);
            startActivity(editProfile);

        } else if (id == R.id.nav_subscription) {
            logo_img.setVisibility(View.GONE);
            fragment = new SubscriptionFragment();
            getSupportActionBar().setTitle(title);
            search_img.setVisibility(View.GONE);

            /*Intent intent = new Intent(HomeActivity.this, SubscriptionActivity.class);
            startActivity(intent);*/

        } else if (id == R.id.nav_help) {
            logo_img.setVisibility(View.GONE);
            fragment = new HelpFragment();
            search_img.setVisibility(View.GONE);
            getSupportActionBar().setTitle(title);

        } else if (id == R.id.nav_about) {
            logo_img.setVisibility(View.GONE);
            fragment = new AboutUsFragment();
            getSupportActionBar().setTitle(title);
            search_img.setVisibility(View.GONE);

        } else if (id == R.id.nav_sign_out) {
            String msg = "Are you sure you want to logout from " + getResources().getString(R.string.app_name);
            showLogoutDialog(msg);
        } else if (id == R.id.nav_setting) {
            fragment = new SettingFragment();
            getSupportActionBar().setTitle(title);
            search_img.setVisibility(View.GONE);
            logo_img.setVisibility(View.GONE);
        } else if (id == R.id.nav_rateus) {
            fragment = new RateAppFragment();
            getSupportActionBar().setTitle(title);
            search_img.setVisibility(View.GONE);
            logo_img.setVisibility(View.GONE);
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container, fragment).addToBackStack(null);
            ft.commit();
        }
        return true;
    }

    private void showUpdateDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyDialogTheme);
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/app_customfonts.ttf");
        SpannableString spannableMessage = new SpannableString(message);
        spannableMessage.setSpan(new CustomTypefaceSpan("", font), 0, spannableMessage.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setMessage(spannableMessage);

        SpannableString spannableString = new SpannableString(getResources().getString(R.string.app_name));
        spannableString.setSpan(new CustomTypefaceSpan("", font), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setTitle(spannableString);

        builder.setIcon(R.mipmap.ic_launcher);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                finish();
                System.exit(0);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }


    /* public void logOutFromGoogle(){
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            Toast.makeText(getApplicationContext(), "logout susccessfully", Toast.LENGTH_SHORT).show();
                        }
                    });
        }
        public void logOutFromFacebook() {
            LoginManager.getInstance().logOut();
        }
    */
    private void showLogoutDialog(String message) {

        final String LoginFrom=sharedPreference.getFromLogedIn(HomeActivity.this,"fromLogedin");
        Log.e("HomeActivity","Login-status"+"_:_"+LoginFrom);
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.MyDialogTheme);


        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/app_customfonts.ttf");

        SpannableString spannableMessage = new SpannableString(message);
        spannableMessage.setSpan(new CustomTypefaceSpan("", font), 0, spannableMessage.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setMessage(spannableMessage);

        SpannableString spannableString = new SpannableString(getResources().getString(R.string.app_name));
        spannableString.setSpan(new CustomTypefaceSpan("", font), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.setTitle(spannableString);

        builder.setIcon(R.mipmap.ic_launcher);
        final String firstName = sharedPreference.getUSerName(HomeActivity.this, "first_name");
        final String lastName = sharedPreference.getUSerLastName(HomeActivity.this, "last_name");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                if (LoginFrom.equals("google")) {
                    goToLoginActivity();
                } else if (LoginFrom.equals("facebook")) {
                    goToLoginActivity();

                } else if (LoginFrom.equals("veqta")) {
                    Log.e("HomeActivity", "***FirsitName***" + firstName);
                    Log.e("HomeActivity", "***lastName***" + lastName);
                    goToLoginActivity();
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        builder.setCancelable(false);
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }

    private void applyFontToMenuItem(MenuItem mi) {
        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/app_customfonts.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    private void goToLoginActivity() {
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("logout", "NoStatus");
        startActivity(intent);
        HomeActivity.this.finish();
    }

    private void getCategories() {
        Log.e("HomeActivity", "getCategories Method");
        progressBar.setVisibility(View.VISIBLE);

        final String key = "VideoTab";
        final Type homeObjectType = new TypeToken<LiveCategory>() {
        }.getType();
        AppController.getInstance().getCacheManager().getAsync(key, LiveCategory.class, homeObjectType, new GetCallback() {
            @Override
            public void onSuccess(final Object object) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        category = (LiveCategory) object;
                        //if (VersionUtils.getIsCategoryVersionChanged(getActivity(), category, key))
                        // category = null;

                        if (category != null) {
                            Log.e("VODCacheManager", key + " object retrieved successfully");

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loadFragments(category);
                                    progressBar.setVisibility(View.GONE);
                                }
                            });
                        } else if (!ConnectionUtils.isNetworkConnected(HomeActivity.this)) {
                            Log.e("VIdeoFragment", "ConnectionManager Not Connected");
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    progressBar.setVisibility(View.GONE);
                                }
                            });
                        } else {
                            StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                                    ApiRequest.LIVE_CATEGORY, new Response.Listener<String>() {
                                @Override
                                public void onResponse(final String response) {
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.e("***VOD-RESPONCE***-", response.toString());
                                            try {
                                                JSONObject mObj = new JSONObject(response);
                                                if (mObj.optInt("code") == 1) {
                                                    MultitvCipher mcipher = new MultitvCipher();
                                                    String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                                    category = Json.parse(str.trim(), LiveCategory.class);
                                                    Log.e("VideoFragment", "VOD:LIST SIZE : " + category.vod.size());

                                                    Log.e("VideoFragment_list--", str.toString());
                                                    AppController.getInstance().getCacheManager().put(key, category);

                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            loadFragments(category);
                                                            progressBar.setVisibility(View.GONE);
                                                        }
                                                    });
                                                } else {
                                                    runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            progressBar.setVisibility(View.GONE);
                                                        }
                                                    });
                                                }
                                            } catch (Exception e) {
                                                Log.e("VIDEO", "Error: " + e.getMessage());
                                                runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        progressBar.setVisibility(View.GONE);
                                                    }
                                                });
                                            }
                                        }
                                    }).start();
                                }
                            }, new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.e("Video", "Error: " + error.getMessage());
                                    progressBar.setVisibility(View.GONE);
                                }
                            }) {

                                @Override
                                protected Map<String, String> getParams() {
                                    Map<String, String> params = new HashMap<String, String>();

                                    //params.put("lan", LocaleHelper.getLanguage(getApplicationContext()));
                                    //params.put("m_filter", (PreferenceData.isMatureFilterEnable(getApplicationContext()) ? "" + 1 : "" + 0));
                                    params.put("device", "android");

                                    return params;
                                }
                            };

                            AppController.getInstance().addToRequestQueue(jsonObjReq);
                        }
                    }
                }).start();
            }

            @Override
            public void onFailure(Exception e) {
                Log.e("categoryError", e.getMessage());
                progressBar.setVisibility(View.GONE);
            }
        });
    }
    /*@Override
    public void onLoadChildCategoryFragment(VideoMain videoMain) {
        VideoChildFragment videoChildFragment = new VideoChildFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(VideoChildFragment.EXTRA_VIDEO_OBJECT, videoMain);
        videoChildFragment.setArguments(bundle);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, videoChildFragment).addToBackStack(null);
        ft.commitAllowingStateLoss();
    }*/
}
