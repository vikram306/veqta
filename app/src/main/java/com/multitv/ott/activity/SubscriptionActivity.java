package com.multitv.ott.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.multitv.ott.R;

import butterknife.BindView;

/**
 * Created by cyberlinks on 6/3/17.
 */

public class SubscriptionActivity extends AppCompatActivity {

    @BindView(R.id.activity_subscription_toolbar)
    Toolbar toolbar;

    @BindView(R.id.activity_subscription_pb)
    ProgressBar progressBar;

    @BindView(R.id.activity_subscription_recyclerView)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.transparent_black_ninty_five));
        }
        setContentView(R.layout.activity_subscription);

        setUpToolbar();
    }

    private void setUpToolbar() {

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle(R.string.subscription_title);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
    }

    private void getSubscribeAndAvailablePackages() {
    }
}
