package com.multitv.ott.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.multitv.cipher.MultitvCipher;
import com.multitv.ott.R;
import com.multitv.ott.Utils.AppConstants;
import com.multitv.ott.Utils.AppNetworkAlertDialog;
import com.multitv.ott.Utils.Json;
import com.multitv.ott.api.ApiRequest;
import com.multitv.ott.controller.AppController;
import com.multitv.ott.controller.SignUpController;
import com.multitv.ott.custom.CustomEditText;
import com.multitv.ott.custom.CustomTextView;
import com.multitv.ott.listeners.SignUpListener;
import com.multitv.ott.models.User;
import com.multitv.ott.sharedpreference.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Lenovo on 25-01-2017.
 */

public class SignUpActivity extends AppCompatActivity implements SignUpListener,GoogleApiClient.OnConnectionFailedListener{
    private Dialog myDialog;
    private CustomEditText email_field,passwordField,nameField,mobileNumber;
    private CustomTextView signUp_btn;
    private LoginButton loginButton;
    private CallbackManager callbackManager;
    private ProgressBar progressBar;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions gso;
    private int RC_SIGN_IN = 3;
    private int loginThrough;
    private User user;
    private String[] splited;
    private SharedPreference sharedPreference;
    private String user_id,password,name,mGender = "", mFirstName = "", mLastName = "", mUserName = "", mDob = "", mEmail = "", mPhoneNum;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.black_semi_transparent));
        }
        setContentView(R.layout.signin_activity);
        sharedPreference=new SharedPreference();
        user_id = sharedPreference.getPreferencesString(this, "user_id" + "_" + ApiRequest.TOKEN);
        mobileNumber=(CustomEditText)findViewById(R.id.mobileNumber);
        email_field=(CustomEditText)findViewById(R.id.email);
        passwordField=(CustomEditText)findViewById(R.id.password);
        signUp_btn=(CustomTextView) findViewById(R.id.signUp_btn);
        nameField=(CustomEditText)findViewById(R.id.name);
        progressBar=(ProgressBar)findViewById(R.id.progress_signin);
        initGoogleFb();
        signUp_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(SignUpActivity.this,LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    public void signUpFromFacebook(View v){
        if (!AppNetworkAlertDialog.isNetworkConnected(SignUpActivity.this)) {
            Toast.makeText(SignUpActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
            return;
        }else{
            fb_login();
        }
    }

    public void signUpFromGoogle(View v){
        if (!AppNetworkAlertDialog.isNetworkConnected(SignUpActivity.this)) {
            Toast.makeText(SignUpActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
            return;
        }else{
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    }

    public void goToHomeActivityFromSignUp(View v){
        hideKeyboard(v);
        if (!AppNetworkAlertDialog.isNetworkConnected(SignUpActivity.this)) {
            Toast.makeText(SignUpActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
            return;
        }else {
            if (validate()) {
                String mpassword = passwordField.getText().toString();
                name = nameField.getText().toString();

                String mobile = mobileNumber.getText().toString();
                if (!TextUtils.isEmpty(mobile)) {
                    mPhoneNum=mobile;
                }

                if (!TextUtils.isEmpty(mpassword)) {
                    password=mpassword;
                }
                String email = email_field.getText().toString();
                if (!TextUtils.isEmpty(email)) {
                    mEmail = email;
                }

                if(!TextUtils.isEmpty(name))
                    splited = name.split("\\s+");
                if (splited.length > 0) {
                    mFirstName = splited[0];
                    sharedPreference.setUserName(this, "first_name", mFirstName);
                    if (splited.length > 1)
                        mLastName = splited[1];
                    sharedPreference.setUserLastName(this, "last_name", mLastName);
                }



                if (!AppNetworkAlertDialog.isNetworkConnected(SignUpActivity.this)) {
                    Toast.makeText(SignUpActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
                    progressBar.setVisibility(View.GONE);
                    return;
                }else {
                    sendInfoToServer(mFirstName, mLastName,mEmail,password,mPhoneNum);
                }

            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            progressBar.setVisibility(View.VISIBLE);
            GoogleSignInResult googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(googleSignInResult);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void initGoogleFb() {
        loginButton = new LoginButton(this);
        callbackManager = CallbackManager.Factory.create();
        //=======google plus login from custom button click============================================================
        //  ===========================================================================
        gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                /*.requestIdToken(getString(R.string.default_web_client_id))*/
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    private void fb_login() {
        progressBar.setVisibility(View.VISIBLE);
        loginButton.setReadPermissions(new String[]{"email", "user_hometown", "user_likes", "public_profile"});
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //Toast.makeText(getActivity(), "login success", Toast.LENGTH_SHORT).show();
                String accessToken = loginResult.getAccessToken().getToken();
                Log.e("accessToken", accessToken);
                final GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject obj,
                                                    GraphResponse response) {

                                progressBar.setVisibility(View.GONE);

                                Log.e("facebookResponce===", response.toString());
                                try {
                                    final String firstName = obj.optString("first_name");
                                    final String lastName = obj.optString("last_name");
                                    final String gender = obj.optString("gender");
                                    final String mail_id = obj.optString("email");
                                    Log.e("id", obj.optString("id"));
                                    String id = obj.optString("id");

                                    if (!TextUtils.isEmpty(gender)) {
                                        mGender = gender;
                                    }
                                    String name = firstName;
                                    if (!TextUtils.isEmpty(lastName)) {
                                        name = name.concat(" " + lastName);
                                    }
                                    mFirstName = firstName;
                                    mLastName = lastName;
                                    mEmail = mail_id;
                                    mUserName = name;

                                    loginThrough = AppConstants.LOGIN_THROUGH_FB;

                                    if (!AppNetworkAlertDialog.isNetworkConnected(SignUpActivity.this)) {
                                        Toast.makeText(SignUpActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
                                        progressBar.setVisibility(View.GONE);
                                        return;
                                    }else{
                                        updateInfoToServer("", mFirstName, mLastName, mGender, "", "", mUserName, mEmail,
                                                "", mDob, loginThrough, mPhoneNum);
                                    }


                                           // profilePicUrlFromFb = data.getJSONObject("picture").getJSONObject("data").getString("url");
                                    Log.e("first_name", obj.optString("first_name"));
                                    Log.e("last_name", obj.optString("last_name"));
                                    Log.e("gender", obj.optString("gender"));
                                    Log.e("name", obj.optString("name"));
                                    Log.e("link", obj.optString("link"));
                                    Log.e("locale", obj.optString("locale"));
                                    Log.e("Email:", obj.optString("email"));
                                    if (!TextUtils.isEmpty(id)) {
                                        final String URL_FB_IMAGE = "http://graph.facebook.com/" + id + "/picture?type=large&redirect=false";

                                        AsyncTask.execute(new Runnable() {
                                            @Override
                                            public void run() {
                                                getProfileFromFb(URL_FB_IMAGE, firstName, lastName, gender, mail_id);
                                            }
                                        });

                                    }
                                    sharedPreference.setFromLogedIn(SignUpActivity.this, "fromLogedin", "facebook");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.e("Fb-error: ", e.getMessage().toString());
                                    progressBar.setVisibility(View.GONE);
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,email,picture,gender,locale,first_name,last_name");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                System.out.println("onCancel");
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(FacebookException exception) {
                System.out.println("onError");
                progressBar.setVisibility(View.GONE);

                try {
                    Log.e("SignUpActivity", exception.getCause().toString());
                    Toast.makeText(SignUpActivity.this, "" + exception.getCause(), Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    Log.e("SignUpActivity", e.getMessage());
                }
            }
        });
        loginButton.performClick();
    }
    public void getProfileFromFb(String url, final String fname, final String lName, final String gender, final String email) {
        StringRequest jsonObjReq = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                try {
                    Log.e("***FACEBOOK_IMAGE***", response);
                    JSONObject mObj = new JSONObject(response);
                    try {

                        JSONObject newObj = mObj.getJSONObject("data");
                        String profilePicUrlFromFb = newObj.optString("url");
                        Log.e("***FB_IMAGE_URL***", profilePicUrlFromFb);
                        if (!TextUtils.isEmpty(profilePicUrlFromFb))
                            sharedPreference.setImageUrl(SignUpActivity.this, "imgUrl", profilePicUrlFromFb);
                    } catch (JSONException e) {

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", "Error: " + error.getMessage());

            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);

    }
    //==============google plus login handle result from getting user information==============================================
    //=========================================================================================
    private void handleSignInResult(GoogleSignInResult result) {
        Log.e("Result..... ", "handleSignInResult:" + result.getStatus().getStatusMessage());
        //progressBar.setVisibility(View.GONE);
        if (result.isSuccess()) {
            GoogleSignInAccount information = result.getSignInAccount();
            String personName = information.getDisplayName();
            String gmail_id = information.getEmail();
            String first_google_name = "", last_google_name = "";
            String[] splited = personName.split("\\s+");
            if (splited.length > 0) {
                first_google_name = splited[0];
                if (splited.length > 1)
                    last_google_name = splited[1];
            }
            if (!TextUtils.isEmpty(last_google_name)) {
                sharedPreference.setGoogleLoginLastName(this, "googleLastName", last_google_name);
            }

            sharedPreference.setFromLogedIn(this, "fromLogedin", "google");
            Log.e("person name", personName);
            Log.e("email", gmail_id);

            sharedPreference.setGoogleLoginUsername(this, "googleNAme", first_google_name);
            sharedPreference.setGoogleLoginEmail(this, "emailFromGoogle", gmail_id);
            String profilePic=null;
            if (information.getPhotoUrl() != null) {
                profilePic = information.getPhotoUrl().toString();
                //updateLoginInformationListner.setProfileImage(profilePic);
                sharedPreference.setGoogleLoginProfilePic(SignUpActivity.this, "ImageGoogleProfile", profilePic);
                sharedPreference.setImageUrl(SignUpActivity.this, "imgUrl", profilePic);
                Log.e("personPhotoUrl", profilePic);
            } else {
                Log.e("personPhotoUrl", "Empty");
            }
            mFirstName = first_google_name;
            mLastName = last_google_name;
            mUserName = personName;
            mEmail = gmail_id;
            loginThrough = AppConstants.LOGIN_THROUGH_GOOGLE;



            if (!AppNetworkAlertDialog.isNetworkConnected(SignUpActivity.this)) {
                Toast.makeText(SignUpActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
                return;
            }else{
                updateInfoToServer("", mFirstName, mLastName, mGender, "", "", mUserName, mEmail, "", mDob,
                        loginThrough, mPhoneNum);
            }
        } else {
            progressBar.setVisibility(View.GONE);
            Toast.makeText(SignUpActivity.this, "Login failed", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mGoogleApiClient.stopAutoManage(this);
        mGoogleApiClient.disconnect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.stopAutoManage(this);
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    private void updateInfoToServer(String id, String fName, String lName, String gender, String link, String locale, String name, String email, String location, String dob, int loginThrough, String phoneNo) {

        if (!(progressBar.getVisibility() == View.VISIBLE))
            progressBar.setVisibility(View.VISIBLE);
        SignUpController signUpController = new SignUpController(this, this);
        signUpController.sendInfoToServer(id, fName, lName, gender, link, locale, name, email, location, dob, loginThrough, phoneNo);
    }

    @Override
    public void onSuccess(String resultString) {
        if ((progressBar.getVisibility() == View.VISIBLE))
            progressBar.setVisibility(View.GONE);

        try {
            JSONObject mObj = new JSONObject(resultString);
            if (mObj.optInt("code") == 1) {
                MultitvCipher mcipher = new MultitvCipher();
                String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                Log.e("SignUpActivity", str);
                User user = Json.parse(str.trim(), User.class);

                if (!TextUtils.isEmpty(user.gender))
                    mGender = user.gender;
                if (!TextUtils.isEmpty(user.first_name))
                    mFirstName = user.first_name;
                else mFirstName = "";
                if (!TextUtils.isEmpty(user.last_name))
                    mLastName = user.last_name;
                else mLastName = "";
                if (!TextUtils.isEmpty(user.dob) && !user.dob.equals("0000-00-00"))
                    mDob = user.dob;
                if (!TextUtils.isEmpty(user.email))
                    mEmail = user.email;
                if (!TextUtils.isEmpty(user.contact_no))
                    mPhoneNum = user.contact_no;

                String provider=user.provider;
                if(!TextUtils.isEmpty(provider)){
                    sharedPreference.setUserName(this, "first_name", mFirstName);
                    sharedPreference.setUserLastName(this, "last_name", mLastName);
                    sharedPreference.setDob(this, "dob", mDob);
                    sharedPreference.setEmailId(this, "email_id", mEmail);
                    sharedPreference.setPhoneNumber(this, "phone", mPhoneNum);

                    if (!TextUtils.isEmpty(user.image))
                        sharedPreference.setImageUrl(this, "ImageURl", user.image);

                    if (mGender.equalsIgnoreCase("male")) {
                        sharedPreference.setGender(this, "gender_id", "" + 0);
                    }else if (mGender.equalsIgnoreCase("female")){
                        sharedPreference.setGender(this, "gender_id", "" + 1);
                    }
                }

                Log.e("LOGIN", "onResponse: " + user.id);
                if (!TextUtils.isEmpty(user.id)) {
                    Log.e("user_id", user.id);
                    sharedPreference.setPreferencesString(this, "user_id" + "_" + ApiRequest.TOKEN, "" + user.id);
                    sharedPreference.setPreferenceBoolean(this, sharedPreference.KEY_IS_LOGGED_IN);
                    if (loginThrough == AppConstants.LOGIN_THROUGH_MULTITV && !TextUtils.isEmpty(user.otp)) {
                        Log.e("otp", user.otp);
                        checkOtp(user.otp,user.id);
                    } else {
                        sharedPreference.setPreferenceBoolean(this, sharedPreference.KEY_IS_OTP_VERIFIED);
                        moveToHomeScreen();
                    }
                }
                //FCMController.getInstance(getApplicationContext()).registerToken();
            } else {
                String error = mObj.optString("error");
                if (!TextUtils.isEmpty(error))
                    Toast.makeText(this, error, Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Log.e("error",""+e.getMessage());
        }
    }


    @Override
    public void onError() {
        if ((progressBar.getVisibility() == View.VISIBLE))
            progressBar.setVisibility(View.GONE);
    }

    public void moveToHomeScreen(){
        Intent intent =new Intent(SignUpActivity.this,HomeActivity.class);
        startActivity(intent);
        finish();
    }


    private boolean validate() {
        boolean valid = true;
        String email = email_field.getText().toString();
        String password = passwordField.getText().toString();
        String name = nameField.getText().toString();
        String mobile = mobileNumber.getText().toString();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

            if (TextUtils.isEmpty(mobile)) {
                mobileNumber.setError("Phone Number can not be blank ");
                valid = false;
            }
          if (!AppConstants.isValidMobile(mobile)) {
                mobileNumber.setError("Invalid phone number");
                valid = false;
            }

        if (TextUtils.isEmpty(password)) {
            passwordField.setError("Password can not be blank");
            valid = false;
        }
        if (TextUtils.isEmpty(name)) {
            nameField.setError("Username can not be blank");
            valid = false;
        }
        if(password.length() < 8){
            passwordField.setError("Password can be atleast 8 character");
            valid = false;
        }
        if(password.length() > 10){
            passwordField.setError("Password can be maximum 10 character");
            valid = false;
        }
        if (!email.matches(emailPattern)) {
            email_field.setError("Email id is not Vaild");
            valid = false;
        }

        if (TextUtils.isEmpty(email)) {
            passwordField.setError("Email can not be blank");
            valid = false;
        }

        return valid;
    }
    private void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    private void sendInfoToServer(final String first_name,final String last_name,final String email,final String password,final String phone){
        if (!AppNetworkAlertDialog.isNetworkConnected(SignUpActivity.this)) {
            Toast.makeText(SignUpActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
            return;
        }

        progressBar.setVisibility(View.VISIBLE);
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                ApiRequest.SIGNUP_URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("SIGNUPACTIVITY","SignUp-URL_:_"+ response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject mObj = new JSONObject(response);
                    MultitvCipher mcipher = new MultitvCipher();
                    if (mObj.optInt("code") == 1) {
                        //String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                        String str=mObj.optString("result");
                        String otp=mObj.optString("otp");
                        String id=mObj.optString("id");
                        Log.e("***SIGNUP-URL**",str);
                        //User user = Json.parse(str.trim(), User.class);
                       // sharedPreference.setEmailId(SignUpActivity.this, "email_id", user.email);
                        if (!TextUtils.isEmpty(otp)&&!TextUtils.isEmpty(id)) {
                            Log.e("otp", otp);
                            checkOtp(otp,id);
                        }else {
                            sharedPreference.setPreferenceBoolean(SignUpActivity.this, sharedPreference.KEY_IS_OTP_VERIFIED);
                            moveToHomeScreen();
                        }

                    } else {
                        String error =mObj.optString("result");
                        if (!TextUtils.isEmpty(error))
                            Toast.makeText(SignUpActivity.this, error, Toast.LENGTH_LONG).show();
                            progressBar.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    Log.e("SIGNUPACTIVITY","Error"+""+e.getMessage());
                    progressBar.setVisibility(View.GONE);
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Verify_otp_api", "Error: " + error.getMessage());
                progressBar.setVisibility(View.GONE);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("first_name", first_name);
                params.put("last_name", last_name);
                params.put("email", email);
                params.put("password", password);
                params.put("user_id", user_id);
                params.put("phone", phone);

                Log.e("first_name",""+first_name);
                Log.e("last_name",""+last_name);
                Log.e("email",""+email);
                Log.e("user_id",""+user_id);
                Log.e("phone",""+phone);
                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }


    private void checkOtp(final String otp, final String user_id) {
        if (!AppNetworkAlertDialog.isNetworkConnected(SignUpActivity.this)) {
            Toast.makeText(SignUpActivity.this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
            return;
        }
        progressBar.setVisibility(View.VISIBLE);

        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                ApiRequest.VERIFY_OTP, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("Verify_otp_api", response);

                progressBar.setVisibility(View.GONE);

                try {
                    JSONObject mObj = new JSONObject(response);
                    MultitvCipher mcipher = new MultitvCipher();
                    if (mObj.optInt("code") == 1) {
                        String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                        Log.e("SIGNUP-FROM-VEQTA","Verify_otp_api"+ str);

                        sharedPreference.setPreferenceBoolean(SignUpActivity.this, sharedPreference.KEY_IS_OTP_VERIFIED);
                        sharedPreference.setPreferenceBoolean(SignUpActivity.this, sharedPreference.KEY_IS_LOGGED_IN);
                        sharedPreference.setFromLogedIn(SignUpActivity.this, "fromLogedin", "veqta");

                        User user = Json.parse(str.trim(), User.class);

                        sharedPreference.setEmailId(SignUpActivity.this, "email_id", user.email);
                        sharedPreference.setUserName(SignUpActivity.this, "first_name", user.first_name);
                        sharedPreference.setUserLastName(SignUpActivity.this, "last_name", user.last_name);
                        sharedPreference.setPhoneNumber(SignUpActivity.this, "phone", user.contact_no);
                        sharedPreference.setPreferencesString(SignUpActivity.this, "user_id" + "_" + ApiRequest.TOKEN, "" + user.id);
                        progressBar.setVisibility(View.GONE);
                        moveToHomeScreen();
                    } else {
                        String error = new String(mcipher.decryptmyapi(mObj.optString("result")));
                        if (!TextUtils.isEmpty(error))
                            Toast.makeText(SignUpActivity.this, error, Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    Log.e("SIGNUP-FROM-VEQTA","Error"+""+e.getMessage());
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("SIGNUP-FROM-VEQTA","Error: " + error.getMessage());

                progressBar.setVisibility(View.GONE);
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("otp", otp);
                params.put("user_id", user_id);

                return params;
            }
        };

        jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 3,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

}