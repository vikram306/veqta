package com.multitv.ott.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.multitv.cipher.MultitvCipher;
import com.multitv.ott.R;
import com.multitv.ott.Utils.CustomTFSpan;
import com.multitv.ott.Utils.PermissionUtility;
import com.multitv.ott.Utils.TypefaceSpan;
import com.multitv.ott.api.ApiRequest;
import com.multitv.ott.controller.AppController;
import com.multitv.ott.custom.CustomEditText;
import com.multitv.ott.custom.CustomTextView;
import com.multitv.ott.imagecrop.CropImage;
import com.multitv.ott.imagecrop.CropImageView;
import com.multitv.ott.imageprocess.ChooserType;
import com.multitv.ott.imageprocess.FileUtils;
import com.multitv.ott.imageprocess.ImageChooserManager;
import com.multitv.ott.imageprocess.ImageProcessorListener;
import com.multitv.ott.listeners.ImageSenderInterface;
import com.multitv.ott.sharedpreference.SharedPreference;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import de.hdodenhof.circleimageview.CircleImageView;
import jp.wasabeef.blurry.Blurry;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.multitv.ott.Utils.ConstantVeqta.EXTRA_OPEN_HOME_SCREEN;
import static com.multitv.ott.Utils.ConstantVeqta.foldername;
import static com.multitv.ott.controller.AppUtils.isValidMobile;

/**
 * Created by Lenovo on 06-03-2017.
 */

public class EditProfileActivity extends AppCompatActivity  {
   private String TAG="EditProfileActivity";
   private DatePickerDialog.OnDateSetListener mDateSetListener;

    private LinearLayout genderLinearLayout;
    private CustomTextView genderType,date_of_birth,toolbar_save_btn;
    private CustomEditText userAboutTxt,userfirstName,userlastName,email_address,phone_number;
    private ImageView camera_bg,toolbar_back_btn;
    private RelativeLayout progressRelativeLayout1;
    private CircleImageView profile_image;
    private int  mYear,mMonth,mDay;
    private String selectedDate;

    // string object===========================================================================================================
    //===================================================================================
    private String userFirstNameStr, userLastNameStr, userDataOfBirthStr, userMobileNumberStr,
            userEmailAdddressStr,userGernderStr, encodedImageFromUrlString,useraboutmeStr;

    private String userChoosenTask, encodedImageSendToServerFromLocal, user_id, header, setting;
    private Bitmap thumbnail;
    private AlertDialog selectImageAlertDialog;
    private ImageSenderInterface imageSenderInterface;
    private Uri cameraImageUri;
    private SharedPreference sharedPreference;
    private  String cameraImagePath;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.signup_btn));
        }
        setContentView(R.layout.activity_edit_profile_screen);
        sharedPreference=new SharedPreference();
        user_id = sharedPreference.getPreferencesString(EditProfileActivity.this, "user_id" + "_" + ApiRequest.TOKEN);
        profile_image=(CircleImageView)findViewById(R.id.profile_image);

        userAboutTxt=(CustomEditText)findViewById(R.id.userAboutTxt);
        userfirstName=(CustomEditText)findViewById(R.id.userFirstName);
        userlastName=(CustomEditText)findViewById(R.id.userLastName);
        email_address=(CustomEditText)findViewById(R.id.emailId);
        phone_number=(CustomEditText)findViewById(R.id.phone);

        genderLinearLayout=(LinearLayout)findViewById(R.id.info);
        genderType=(CustomTextView)findViewById(R.id.genderType);
        toolbar_save_btn=(CustomTextView)findViewById(R.id.toolbar_save_btn);
        date_of_birth=(CustomTextView)findViewById(R.id.date_of_birth);
        camera_bg=(ImageView)findViewById(R.id.camera_bg);
        progressRelativeLayout1 = (RelativeLayout) findViewById(R.id.progressRelativeLayout1);
        toolbar_back_btn=(ImageView)findViewById(R.id.toolbar_back_btn);

        toolbar_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditProfileActivity.this, SavedProfileActivity.class);
                intent.putExtra("home_activity", "profile_activity");
                intent.putExtra(EXTRA_OPEN_HOME_SCREEN, true);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                EditProfileActivity.this.finish();
            }
        });

        genderLinearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(EditProfileActivity.this, genderLinearLayout);
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        Toast.makeText(EditProfileActivity.this,"You Clicked : " + item.getTitle(),Toast.LENGTH_SHORT).show();
                        genderType.setText(item.getTitle());
                        return true;
                    }
                });

                popup.show();
            }

        });

        //-----set select date of birth------
        setDate();
        updateInformartion();
        camera_bg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        toolbar_save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    putInformationToServer();
                }
            }
        });

    }
    public void selectBirthday(View v){
        new DatePickerDialog(EditProfileActivity.this,
                mDateSetListener,
                mYear, mMonth, mDay).show();
    }
    private void setDate(){
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR) - 20;
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH); //Default DOB is (today - 20) years
        //date picker things
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                mYear = year;
                mMonth = monthOfYear;
                mDay = dayOfMonth;
                date_of_birth.setText(new StringBuilder().append(mYear).append("-").append(mMonth + 1).append("-").append(mDay));
                if(!TextUtils.isEmpty(date_of_birth.getText())) {
                    selectedDate = date_of_birth.getText().toString();
                    Log.e(TAG,"DateFromPicker"+":_:"+selectedDate);
                }else{
                    Log.e(TAG,"DateFromPicker"+":_:"+"Data is empty");
                }
            }
        };
    }

    //==============selectImage dailog===================
    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library",
                "Cancel"};

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(EditProfileActivity.this);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/app_customfonts.ttf");
        CustomTFSpan tfSpan = new CustomTFSpan(tf);
        SpannableString spannableString = new SpannableString(getResources().getString(R.string.app_name));
        spannableString.setSpan(tfSpan, 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        alertDialogBuilder.setTitle(spannableString);
        alertDialogBuilder.setIcon(R.mipmap.ic_launcher);
        alertDialogBuilder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = PermissionUtility.checkPermission(EditProfileActivity.this);

                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        selectImageAlertDialog = alertDialogBuilder.create();
        selectImageAlertDialog.setCancelable(true);
        selectImageAlertDialog.setCanceledOnTouchOutside(false);
        selectImageAlertDialog.show();
    }

    //==============selectImage from gallery===================
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        //startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
        startActivityForResult(Intent.createChooser(intent, "Select File"), ChooserType.REQUEST_PICK_PICTURE);

    }

    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        Bitmap bitmap2 = null;
        if (data != null) {
            try {
                Bitmap bm = null;
                bm = MediaStore.Images.Media.getBitmap(EditProfileActivity.this.getContentResolver(), data.getData());
                ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();
                bm.compress(Bitmap.CompressFormat.JPEG, 90, bytearrayoutputstream);

                byte[] BYTE = bytearrayoutputstream.toByteArray();
                bitmap2 = BitmapFactory.decodeByteArray(BYTE, 0, BYTE.length);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        int width = profile_image.getWidth();
        int height = profile_image.getHeight();
        profile_image.setImageBitmap(Bitmap.createScaledBitmap(bitmap2, width, height, false));

    }

    //==============selectImage from camera===================
    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        /*File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File output = new File(dir, System.currentTimeMillis() + ".jpg");*/
        String output = FileUtils.getDirectory(foldername) + File.separator
                + Calendar.getInstance().getTimeInMillis() + ".jpg";
        File fileOutput = new File(output);
        //cameraImageUri = Uri.fromFile(fileOutput);
        cameraImageUri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", fileOutput);
        cameraImagePath = "file:" + fileOutput.getAbsolutePath();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, cameraImageUri);
        //startActivityForResult(intent, REQUEST_CAMERA);
        startActivityForResult(intent, ChooserType.REQUEST_CAPTURE_PICTURE);
    }

    private void onCaptureImageResult(Intent data) {
        thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        Log.e("filePath", "" + destination);

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        profile_image.setImageBitmap(thumbnail);

        if (thumbnail != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, baos); //bm is the bitmap object
            byte[] byteArrayImage = baos.toByteArray();
            encodedImageSendToServerFromLocal = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
            Log.e("pic", "" + encodedImageSendToServerFromLocal);
        }
    }

    //==============premission dialog===================
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PermissionUtility.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo"))
                        cameraIntent();
                    else if (userChoosenTask.equals("Choose from Library"))
                        galleryIntent();
                } else {
                    //code for deny
                }
                break;
        }
    }

    //============on activity result=====================
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (((requestCode == ChooserType.REQUEST_PICK_PICTURE) || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE) && resultCode == Activity.RESULT_OK) {
            cropImage(data, requestCode);
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                Bitmap croppedBitmap = null;
                try {
                    croppedBitmap = MediaStore.Images.Media.getBitmap(EditProfileActivity.this.getContentResolver(), resultUri);
                    //Bitmap croppedBitmap = result.getBitmap();
                    profile_image.setImageBitmap(croppedBitmap);


                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (croppedBitmap != null) {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    croppedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                    byte[] byteArrayImage = baos.toByteArray();
                    encodedImageSendToServerFromLocal = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
                   // userEditProfile(encodedImageSendToServerFromLocal);
                }
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

           /* if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_FILE)
                    onSelectFromGalleryResult(data);
                else if (requestCode == REQUEST_CAMERA)
                    onCaptureImageResult(data);
            }*/
    }

    private void cropImage(Intent data, int requestCode) {
        ImageProcessorListener imageProcessorListener = new ImageProcessorListener() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(String error) {

            }
        };

        GetImagePath getImagePath = new GetImagePath(requestCode, imageProcessorListener, data);
        getImagePath.execute();

    }

    private void startCropImageActivity(Uri imageUri) {
         Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/app_customfonts.ttf");
        SpannableString string = new SpannableString("Profile Image");
        string.setSpan(new TypefaceSpan(tf), 0, string.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAllowRotation(false)
                .setFixAspectRatio(true)
                .setCropShape(CropImageView.CropShape.OVAL)
                .setMultiTouchEnabled(false)
                .setActivityTitle(String.valueOf(string))
                .setAutoZoomEnabled(false)
                .setInitialCropWindowPaddingRatio(0)
                .start(this);
    }




    class GetImagePath extends AsyncTask<Void, Void, Uri> {
        int requestCode;
        ImageProcessorListener mImageProcessorListener;
        Intent data;
        Uri imageUri;

        GetImagePath(int reqCode, ImageProcessorListener imageProcessorListener, Intent intent) {
            this.requestCode = reqCode;
            this.mImageProcessorListener = imageProcessorListener;
            data = intent;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
           // pbProfilePic.setVisibility(View.VISIBLE);
        }

        @Override
        protected Uri doInBackground(Void... voids) {

            if (requestCode == ChooserType.REQUEST_CAPTURE_PICTURE) {
                if (!TextUtils.isEmpty(cameraImagePath))
                    cameraImageUri = Uri.parse(cameraImagePath);
            }

            ImageChooserManager imageChooserManager = new ImageChooserManager(EditProfileActivity.this, mImageProcessorListener, cameraImageUri);
            imageUri = imageChooserManager.getImageFilePath(requestCode, data);

            return imageUri;
        }

        @Override
        protected void onPostExecute(Uri uri) {
            super.onPostExecute(uri);
           // pbProfilePic.setVisibility(View.GONE);
            if (uri != null) {
                startCropImageActivity(uri);
            }
        }
    }




    private void userEditProfile(final String aboutus ,final String firistName,
                                 final String mobNumber, final String lastName, final String dob, final String gender,
                                 final String email,final String encodedImageUrl) {
            progressRelativeLayout1.setVisibility(View.VISIBLE);

        if (EditProfileActivity.this == null)
            return;
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                        ApiRequest.USER_EDIT, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("api_responce---", response.toString());
                        progressRelativeLayout1.setVisibility(View.GONE);

                        try {
                            JSONObject mObj = new JSONObject(response);

                            if (mObj.optInt("code") == 1) {
                                MultitvCipher mcipher = new MultitvCipher();
                                String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                Log.e("***user_data***", str);
                                try {
                                    JSONObject newObj = new JSONObject(str);
                                    String contact_no = newObj.getString("contact_no");
                                    String image_url = newObj.getString("image");
                                    Log.e(TAG,"***image_url***"+ "" + image_url);
                                    sharedPreference.setImageUrl(EditProfileActivity.this, "imgUrl", image_url);
                                    Log.e(TAG,"***userResponces***"+ "" + newObj.toString());
                                    progressRelativeLayout1.setVisibility(View.GONE);
                                    Toast.makeText(EditProfileActivity.this, "Profile successfully saved", Toast.LENGTH_LONG).show();

                                    Intent intent = new Intent(EditProfileActivity.this, SavedProfileActivity.class);
                                    startActivity(intent);
                                    EditProfileActivity.this.finish();

                                } catch (JSONException e) {
                                    Log.e(TAG,"***edit fail***"+ "JSONException"+e.getMessage());
                                    progressRelativeLayout1.setVisibility(View.GONE);

                                }
                            }
                        } catch (Exception e) {
                            Log.e("***editFragment***", "Exception"+e.getMessage());
                            progressRelativeLayout1.setVisibility(View.GONE);
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            if (error.networkResponse != null) {
                                Log.e("etworkResponse", "" + error.networkResponse);
                                Toast.makeText(EditProfileActivity.this, "" + error.networkResponse.toString(), Toast.LENGTH_LONG).show();
                            }
                            Log.e("user_edit_fail", "" + error.getMessage());
                            progressRelativeLayout1.setVisibility(View.GONE);
                            Toast.makeText(EditProfileActivity.this, "" + error.getMessage().toString(), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Log.e("EditProfileFragment", e.getMessage());
                        }
                    }
                }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();

                       // params.put("lang", LocaleHelper.getLanguage(getApplicationContext()));
                       // params.put("m_filter", (PreferenceData.isMatureFilterEnable(getApplicationContext()) ? "" + 1 : "" + 0));

                            params.put("id", user_id);
                            if (dob != null && !dob.equals("") && !dob.isEmpty()) {
                                params.put("dob", dob);
                                Log.e("****date***", dob);
                            }
                            if (email != null && !email.equals("") && !email.isEmpty()) {
                                params.put("about_me", email);
                            }
                            if (gender != null && !gender.equals("") && !gender.isEmpty()) {
                                params.put("gender", gender);
                            }
                            if (mobNumber != null && !mobNumber.equals("") && !mobNumber.isEmpty()) {
                                params.put("contact_no", mobNumber);
                            }
                            if (lastName != null && !lastName.equals("") && !lastName.isEmpty()) {
                                params.put("last_name", lastName);
                            }
                            if (firistName != null && !firistName.equals("") && !firistName.isEmpty()) {
                                params.put("first_name", firistName);
                            }
                        if (!TextUtils.isEmpty(encodedImageUrl)) {
                            params.put("pic", encodedImageUrl);
                            params.put("ext", "jpg");
                        }
                        if(!TextUtils.isEmpty(aboutus)){

                        }

                        return params;
                    }
                };

                jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(20 * 1000, 3,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                // Adding request to request queue
                AppController.getInstance().addToRequestQueue(jsonObjReq);
            }
        });

    }



    private void updateInformartion(){
        String firstName=sharedPreference.getUSerName(EditProfileActivity.this,"first_name");
        String lastName=sharedPreference.getUSerLastName(EditProfileActivity.this,"last_name");
        String email=sharedPreference.getEmailID(EditProfileActivity.this,"email_id");
        String dob=sharedPreference.getDob(EditProfileActivity.this,"dob");
        String phone=sharedPreference.getPhoneNumber(EditProfileActivity.this,"phone");
        String imgUrl=sharedPreference.getImageUrl(EditProfileActivity.this,"imgUrl");
        String gender_id=sharedPreference.getGender(EditProfileActivity.this,"gender_id");
        String about=sharedPreference.getAbout(EditProfileActivity.this,"about");

        if(!TextUtils.isEmpty(firstName))
            userfirstName.setText(firstName);

        if(!TextUtils.isEmpty(about))
            userAboutTxt.setText(about);


        if(!TextUtils.isEmpty(lastName))
            userlastName.setText(lastName);

        if(!TextUtils.isEmpty(email))
            email_address.setText(email);

        if(!TextUtils.isEmpty(phone))
            phone_number.setText(phone);

        if(!TextUtils.isEmpty(dob))
            date_of_birth.setText(dob);

        if(!TextUtils.isEmpty(gender_id))
            if(gender_id.equalsIgnoreCase("0")){
                genderType.setText("Male");
            }else if(gender_id.equalsIgnoreCase("1")){
                genderType.setText("Female");
            }


        if(!TextUtils.isEmpty(imgUrl)){
            Picasso
                    .with(EditProfileActivity.this)
                    .load(imgUrl)
                    .placeholder(R.mipmap.intex_profile)
                    .error(R.mipmap.intex_profile)
                    .into(profile_image);
        }else{
            //profile_image.setImageResource(R.mipmap.intex_profile);
        }

    }

    //===========================validation all edit-text field==================================================================
    //=====================================================================================
    private boolean validate() {
        boolean valid = true;
        String fistNAme = userfirstName.getText().toString();
        String aboutus=userAboutTxt.getText().toString();
        String lastName = userlastName.getText().toString();
        String mobile = phone_number.getText().toString();
        String emails = email_address.getText().toString();
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (fistNAme.isEmpty() || fistNAme.length() < 0) {
            userfirstName.setError("Enter first name");
            valid = false;
        } else {
            userfirstName.setError(null);
            valid = true;
        }
        if(TextUtils.isEmpty(aboutus)){
            userAboutTxt.setError("Enter About Details");
            valid = false;
        }else {
            userAboutTxt.setError(null);
            valid = true;
        }


        if (lastName.isEmpty() || lastName.length() < 0) {
            userlastName.setError("Enter lastName");
            valid = false;
        } else {
            userlastName.setError(null);
            valid = true;
        }
        if (!mobile.isEmpty() && !isValidMobile(mobile)) {
            phone_number.setError("Invalid phone number");
            valid = false;
        } else {
            phone_number.setError(null);
            valid = true;
        }
        if (!emails.isEmpty() && !emails.matches(emailPattern)) {
            email_address.setError("Invalid email id");
            valid = false;
        } else {
            email_address.setError(null);
            valid = true;
        }
        return valid;
    }


    private void putInformationToServer() {
        String date = date_of_birth.getText().toString();
        String mob = phone_number.getText().toString();
        String email = email_address.getText().toString();
        String lastname = userlastName.getText().toString();
        String firstNametest = userlastName.getText().toString();
        String gender=genderType.getText().toString();
        String aboutus=userAboutTxt.getText().toString();


        String regEx = "^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d{2}$";
        Matcher matcherObj = Pattern.compile(regEx).matcher(date);
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (!TextUtils.isEmpty(firstNametest)) {
            userFirstNameStr = userfirstName.getText().toString();
            sharedPreference.setUserName(EditProfileActivity.this, "first_name", userFirstNameStr);
        } else {
            userlastName.setError("UserName Should not be blank");
        }

        if (!TextUtils.isEmpty(aboutus)) {
            useraboutmeStr = userAboutTxt.getText().toString();
            sharedPreference.setAbout(EditProfileActivity.this, "about", useraboutmeStr);
        } else {
            userlastName.setError("UserName Should not be blank");
        }

        if (!TextUtils.isEmpty(lastname)) {
            userLastNameStr = userlastName.getText().toString();
            sharedPreference.setUserLastName(EditProfileActivity.this, "last_name", userLastNameStr);

        } else {
            userlastName.setError("UserName Should not be blank");
        }
        if (!TextUtils.isEmpty(date)) {
            userDataOfBirthStr = date_of_birth.getText().toString();
            sharedPreference.setDob(EditProfileActivity.this, "dob", date);
        }
        if (!TextUtils.isEmpty(mob) && isValidMobile(mob)) {
            userMobileNumberStr = phone_number.getText().toString();
            sharedPreference.setPhoneNumber(EditProfileActivity.this, "phone", userMobileNumberStr);
        }
        if (!TextUtils.isEmpty(email) && email.matches(emailPattern)) {
            userEmailAdddressStr = email_address.getText().toString();
            sharedPreference.setEmailId(EditProfileActivity.this, "email_id", userEmailAdddressStr);
        } else {
        }
        if (!TextUtils.isEmpty(gender)) {
            userGernderStr = genderType.getText().toString();
            String genderSaveStr=userGernderStr.toLowerCase();
            if (genderSaveStr.equalsIgnoreCase("male")) {
                sharedPreference.setGender(this, "gender_id", "" + 0);
            }else if (genderSaveStr.equalsIgnoreCase("female")){
                sharedPreference.setGender(this, "gender_id", "" + 1);
            }
        }
        Bitmap bitmap = ((BitmapDrawable) profile_image.getDrawable()).getBitmap();
        if (bitmap != null) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
            byte[] byteArrayImage = baos.toByteArray();
            encodedImageSendToServerFromLocal = Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
        }

        userEditProfile(aboutus,userFirstNameStr, userMobileNumberStr, userLastNameStr, userDataOfBirthStr, userGernderStr, userEmailAdddressStr,encodedImageSendToServerFromLocal);

    }


    @Override
    public void onBackPressed() {
       /* if (selectImageAlertDialog != null && selectImageAlertDialog.isShowing()) {
            selectImageAlertDialog.dismiss();
            return;
        }*/

        //PreferenceData.setPreviousFragmentSelectedPosition(getApplicationContext(), 0);
        Intent intent = new Intent(EditProfileActivity.this, SavedProfileActivity.class);
        //intent.putExtra(EXTRA_OPEN_HOME_SCREEN, true);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        EditProfileActivity.this.finish();
        super.onBackPressed();
    }


}
