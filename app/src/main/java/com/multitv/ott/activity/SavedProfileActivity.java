package com.multitv.ott.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;

import com.multitv.ott.R;
import com.multitv.ott.custom.CustomTextView;
import com.multitv.ott.sharedpreference.SharedPreference;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.multitv.ott.Utils.ConstantVeqta.EXTRA_OPEN_HOME_SCREEN;

/**
 * Created by Lenovo on 05-03-2017.
 */

public class SavedProfileActivity extends AppCompatActivity {
    private String TAG="SavedProfileActivity";
   private CustomTextView username,email_address,date_of_birth,phone_number,aboutTextview;
    private RadioButton male,female;
    private ImageView toolbar_back_btn;
    private LinearLayout aboutMeLinearBg;
    private CircleImageView profile_image;
    private SharedPreference sharedPreference;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.signup_btn));
        }
        setContentView(R.layout.activity_saved_profile);
        sharedPreference=new SharedPreference();
        email_address=(CustomTextView)findViewById(R.id.email_address);
        username=(CustomTextView)findViewById(R.id.username);
        aboutTextview=(CustomTextView)findViewById(R.id.aboutTextview);
        date_of_birth=(CustomTextView)findViewById(R.id.date_of_birth);
        phone_number=(CustomTextView)findViewById(R.id.phone_number);
        male=(RadioButton)findViewById(R.id.male);
        aboutMeLinearBg=(LinearLayout) findViewById(R.id.aboutMeLinearBg);
        female=(RadioButton)findViewById(R.id.female);
        profile_image=(CircleImageView) findViewById(R.id.profile_image);
        toolbar_back_btn=(ImageView)findViewById(R.id.toolbar_back_btn);

        toolbar_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SavedProfileActivity.this, UserProfileActivity.class);
                intent.putExtra("home_activity", "profile_activity");
                intent.putExtra(EXTRA_OPEN_HOME_SCREEN, true);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                SavedProfileActivity.this.finish();
            }
        });

        updateInformartion();
    }
    public void editProfileClick(View v){
        Intent editProfile = new Intent(SavedProfileActivity.this, EditProfileActivity.class);
        startActivity(editProfile);
        //SavedProfileActivity.this.finish();
    }

    private void updateInformartion(){
        String firstName=sharedPreference.getUSerName(SavedProfileActivity.this,"first_name");
        String lastName=sharedPreference.getUSerLastName(SavedProfileActivity.this,"last_name");
        String email=sharedPreference.getEmailID(SavedProfileActivity.this,"email_id");
        String dob=sharedPreference.getDob(SavedProfileActivity.this,"dob");
        String phone=sharedPreference.getPhoneNumber(SavedProfileActivity.this,"phone");
        String imgUrl=sharedPreference.getImageUrl(SavedProfileActivity.this,"imgUrl");
        String gender_id=sharedPreference.getGender(SavedProfileActivity.this,"gender_id");
        String aboutStr=sharedPreference.getAbout(SavedProfileActivity.this,"about");
        if(!TextUtils.isEmpty(gender_id)){
            if(gender_id.equalsIgnoreCase("0")){
                male.setChecked(true);
            }else if(gender_id.equalsIgnoreCase("1")){
                female.setChecked(true);
            }
        }else {
            male.setChecked(false);
            female.setChecked(false);
        }
        if(!TextUtils.isEmpty(firstName)) {
            username.setVisibility(View.VISIBLE);
            username.setText(firstName);
        }else{
            username.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(lastName)) {
            username.setVisibility(View.VISIBLE);
            username.setText(firstName +" "+ lastName);
        }else{
            username.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(email)) {
            email_address.setVisibility(View.VISIBLE);
            email_address.setText(email);
        }else{
            email_address.setText(getResources().getString(R.string.notAvailable));
        }

        if(!TextUtils.isEmpty(phone)) {
            if(!phone.equalsIgnoreCase("0")) {
                phone_number.setVisibility(View.VISIBLE);
                phone_number.setText(phone);
            }
        }else{
            phone_number.setText(getResources().getString(R.string.notAvailable));
        }

        if(!TextUtils.isEmpty(dob)) {
            date_of_birth.setVisibility(View.VISIBLE);
            date_of_birth.setText(dob);
        }else{
            date_of_birth.setText(getResources().getString(R.string.notAvailable));
        }

        if(!TextUtils.isEmpty(imgUrl)){
            Picasso
                    .with(SavedProfileActivity.this)
                    .load(imgUrl)
                    .placeholder(R.mipmap.intex_profile)
                    .error(R.mipmap.intex_profile)
                    .into(profile_image);
        }else{
            //profile_image.setImageResource(R.mipmap.intex_profile);
        }
        if(!TextUtils.isEmpty(aboutStr)){
            aboutMeLinearBg.setVisibility(View.VISIBLE);
            aboutTextview.setText(aboutStr);
        }else{
            aboutMeLinearBg.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        updateInformartion();
        Log.e(TAG,"onNewIntent"+"method calling");
    }

    @Override
    public void onBackPressed() {
       /* if (selectImageAlertDialog != null && selectImageAlertDialog.isShowing()) {
            selectImageAlertDialog.dismiss();
            return;
        }*/

        //PreferenceData.setPreviousFragmentSelectedPosition(getApplicationContext(), 0);
        Intent intent = new Intent(SavedProfileActivity.this, UserProfileActivity.class);
        //intent.putExtra(EXTRA_OPEN_HOME_SCREEN, true);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        SavedProfileActivity.this.finish();
        super.onBackPressed();
    }
}
