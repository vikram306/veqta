package com.multitv.ott.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.reflect.TypeToken;
import com.iainconnor.objectcache.GetCallback;
import com.iainconnor.objectcache.PutCallback;
import com.multitv.cipher.MultitvCipher;
import com.multitv.ott.R;
import com.multitv.ott.Utils.ConnectionUtils;
import com.multitv.ott.Utils.Json;
import com.multitv.ott.Utils.PlayerUtils;
import com.multitv.ott.Utils.RecyclerItemClickListener;
import com.multitv.ott.adapter.MoreHomeDisplayCategoryAdapter;
import com.multitv.ott.adapter.MoreLiveAdapter;
import com.multitv.ott.adapter.MoreRecommendedAdapter;
import com.multitv.ott.api.ApiRequest;
import com.multitv.ott.controller.AppController;
import com.multitv.ott.listeners.OnLoadMoreListener;
import com.multitv.ott.models.home.ContentHome;
import com.multitv.ott.models.home.Home;
import com.multitv.ott.models.live.LiveParent;
import com.multitv.ott.models.recommendeds.Content;
import com.multitv.ott.models.recommendeds.Recommended;
import com.multitv.ott.sharedpreference.SharedPreference;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Lenovo on 12-02-2017.
 */

public class MoreActivity extends AppCompatActivity {


    private final int SPEECP_REQUEST_CODE = 12345;
    private final String TAG = "MoreDataActivity";
    @BindView(R.id.top_relative_layout)
    RelativeLayout topRelativeLayout;
    @BindView(R.id.load_recyler_view)
    RecyclerView moreRecylerView;
    @BindView(R.id.activity_more_data_dropdown_lang)
    TextView mTextViewLanguage;
    @BindView(R.id.activity_more_data_dropdown_price)
    TextView mTextViewPricing;
    @BindView(R.id.activity_more_data_dropdown_provider)
    TextView mTextViewContentProvider;
    @BindView(R.id.progressBar2)
    ProgressBar mProgress_bar_top;
    @BindView(R.id.progress_bar_search)
    ProgressBar mProgress_bar_search;
    @BindView(R.id.more_frame)
    ViewGroup more_frame_layout;
    @BindView(R.id.search_frame)
    ViewGroup search_frame_layout;
    @BindView(R.id.search_count_textview)
    TextView searchCountTextview;
    @BindView(R.id.search_recylerview)
    RecyclerView searchRecylerview;
    @BindView(R.id.empty)
    LinearLayout noRecordFoundTextview;
    private int tag;
    private String query = "";
    private String title, content_id, rating, type, thumbnail;

    private MoreHomeDisplayCategoryAdapter moreHomeDisplayCategoryAdapter;
    private String status;
    private long watchedDuration;
    private Recommended recommendedSaved, recommended, homeDataSave;
    private MoreRecommendedAdapter recommendedAdapter;
    private List<Content> recommendArrayList = new ArrayList<>();
    private String user_id, des, cat_id, cat_type;
    private Intent intent;
    private ProgressBar progressbar1;
    private SharedPreference sharedPreference;
    private int itemsLeft = -1, startCount = 0, endCount = 0, count = 0, mCuretPage = 1, current_page = 0;
    private boolean hasSearch_focus = false;
    private boolean isComingFromSearchActivity;

    //----live tv----------------
    private LiveParent liveParent, liveParentSaved;
    private List<ContentHome> liveArrayList = new ArrayList<>();

    private List<Content> displaycategoryItemArraylist = new ArrayList<>();

    private MoreLiveAdapter moreLiveAdapter, moreLiveSearchAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("MKR", "MoreDataActivity.onCreate() ");
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.transparent_black_ninty_five));
        }
        setContentView(R.layout.activity_more_data_parent);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        try {
            Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/app_customfonts.ttf");
            for (int i = 0; i < toolbar.getChildCount(); i++) {
                Log.e("MoreDataActivity", "onCreate: " + toolbar.getChildAt(i));
                if (toolbar.getChildAt(i) instanceof TextView) {
                    ((TextView) toolbar.getChildAt(i)).setTypeface(tf);
                }
            }
        } catch (Exception e) {
            Log.e("MoreDataActivity", "onCreate: " + e.getMessage());
        }

        sharedPreference = new SharedPreference();
        user_id = new SharedPreference().getPreferencesString(MoreActivity.this, "user_id" + "_" + ApiRequest.TOKEN);
        progressbar1 = (ProgressBar) findViewById(R.id.progressBar1);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        noRecordFoundTextview = (LinearLayout) findViewById(R.id.empty);
        intent = getIntent();
        tag = intent.getIntExtra("more_data_tag", 0);
        Log.e(TAG, "onCreate: TAG " + tag);


        switch (tag) {
            case 1:
                //--------------------recommended data ----------------------------------------------------------------
                //--------------------------------------------------------------------------------------------------
                getSupportActionBar().setTitle("Recommended");
                String offsetString = sharedPreference.getPreferencesString(this, "offset_" + "More_ Recommended");
                if (!TextUtils.isEmpty(offsetString))
                    count = Integer.parseInt(offsetString);

                getRecommended(current_page);

                LinearLayoutManager gridLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
                moreRecylerView.setLayoutManager(gridLayoutManager);
                recommendedAdapter = new MoreRecommendedAdapter(MoreActivity.this, recommendArrayList, moreRecylerView, true);
                moreRecylerView.setAdapter(recommendedAdapter);
                recommendtdShowData(false);
                recommendedAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                    @Override
                    public void onLoadMore() {
                        current_page = current_page + 1;
                        Log.e("more data", "" + current_page);
                        count = recommended.offset;

                        if (!hasSearch_focus) {
                            getRecommended(current_page);
                        }
                    }
                });
                break;
            //--------------------LIVE data -----------------------------------------------------------------
            //-------------------------------------------------------------------------------------------------------
            case 2:
                cat_id = intent.getStringExtra("cat_id");
                //cat_type = intent.getStringExtra(EXTRA_CATEGORY_TYPE);
                String catName = intent.getStringExtra("catName");
                if (!TextUtils.isEmpty(cat_id) && !TextUtils.isEmpty(catName))
                    getSupportActionBar().setTitle(catName);
                else
                    getSupportActionBar().setTitle("Live TV");

                initializeLiveChannelViews();
                //initializeLiveChannelViewsForSearch();
                if (!TextUtils.isEmpty(cat_id))
                    offsetString = sharedPreference.getPreferencesString(this, "offset_" + "Live_" + cat_id);
                else
                    offsetString = sharedPreference.getPreferencesString(this, "offset_" + "Live");

                if (!TextUtils.isEmpty(offsetString))
                    count = Integer.parseInt(offsetString);

                moreLiveAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                    @Override
                    public void onLoadMore() {
                        current_page = current_page + 1;
                        Log.e("more data", "" + current_page);
                        count = liveParent.offset;

                        getLiveChannelData(current_page, cat_id);
                    }
                });
           /* moreLiveSearchAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    current_page = current_page + 1;
                    Log.e("more data", "" + current_page);
                    count = liveParent.offset;

                    getLiveChannelData(current_page);
                }
            });*/
                getLiveChannelData(current_page, cat_id);
                break;
            case 3:
                Log.e(TAG, " SWITCH.onCreate: " + isComingFromSearchActivity);
                cat_id = intent.getStringExtra("cat_id");
                //cat_type = intent.getStringExtra(EXTRA_CATEGORY_TYPE);
                catName = intent.getStringExtra("catName");
                getSupportActionBar().setTitle(catName);
                // if (!isComingFromSearchActivity) {
                LinearLayoutManager gridLayoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
                moreRecylerView.setLayoutManager(gridLayoutManager1);
                moreHomeDisplayCategoryAdapter = new MoreHomeDisplayCategoryAdapter(MoreActivity.this, displaycategoryItemArraylist, moreRecylerView);
                moreRecylerView.setAdapter(moreHomeDisplayCategoryAdapter);
                displayCategoryDataShow();

                offsetString = sharedPreference.getPreferencesString(this, "offset_" + "More_Home_Category" + cat_id);
                if (!TextUtils.isEmpty(offsetString))
                    count = Integer.parseInt(offsetString);

                getHomeCatData(cat_id, current_page);

                moreHomeDisplayCategoryAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                    @Override
                    public void onLoadMore() {
                        current_page = current_page + 1;
                        Log.e("more data", "" + current_page);
                        count = recommended.offset;

                        if (!hasSearch_focus)
                            getHomeCatData(cat_id, current_page);
                    }
                });
                    /*LinearLayoutManager mLayoutManager = new LinearLayoutManager(MoreActivity.this, LinearLayoutManager.VERTICAL, false);
                    searchRecylerview.setLayoutManager(mLayoutManager);
                    recommendedAdapter = new MoreRecommendedAdapter(MoreActivity.this, recommendArrayList, searchRecylerview, true);
                    searchRecylerview.setAdapter(recommendedAdapter);
                    recommendedAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                        @Override
                        public void onLoadMore() {
                            current_page = current_page + 1;
                            Log.e("more data", "" + current_page);
                            if (recommended != null) {
                                count = recommended.offset;
                            } else {
                                count = 0;
                            }
                            //searchDataLoad(query);
                        }
                    });
                    recommendtdShowData(true);*/
               /* } else {
                    LinearLayoutManager mLayoutManager = new LinearLayoutManager(MoreActivity.this, LinearLayoutManager.VERTICAL, false);
                    searchRecylerview.setLayoutManager(mLayoutManager);
                    recommendedAdapter = new MoreRecommendedAdapter(MoreActivity.this, recommendArrayList, searchRecylerview, true);
                    searchRecylerview.setAdapter(recommendedAdapter);
                    recommendedAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                        @Override
                        public void onLoadMore() {
                            current_page = current_page + 1;
                            Log.e("more data", "" + current_page);
                            if (recommended != null) {
                                count = recommended.offset;
                            } else {
                                count = 0;
                            }
                            //searchDataLoad(query);
                        }
                    });
                    recommendtdShowData(true);
                    //searchDataLoad(query);
                }
*/
                break;

        }
    }

    //-------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------recommended recyclerview show data-----------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------

    private void getRecommended(final int current_page) {
        /*if (!ConnectionManager.getInstance(MoreDataActivity.this).isConnected()) {
            return;
        }*/
        if (current_page > 0) {
            progressbar1.setIndeterminate(true);
            progressbar1.setVisibility(View.VISIBLE);
        } else
            mProgress_bar_top.setVisibility(View.VISIBLE);

        final String key = "More" + "_" + "Recommended";
        final Type objectType = new TypeToken<Recommended>() {
        }.getType();
        AppController.getInstance().getCacheManager().getAsync(key, Recommended.class, objectType, new GetCallback() {
            @Override
            public void onSuccess(Object object) {
                recommendedSaved = (Recommended) object;

                if (recommendedSaved != null && recommendedSaved.content != null && recommendedSaved.content.size() != 0 &&
                        (count <= recommendedSaved.content.size() || count > recommendedSaved.totalCount)
                        && ((itemsLeft == -1 && recommendArrayList.size() == 0) || itemsLeft > 0)) {
                    Log.e("CacheManager", key + " object retrieved successfully");

                    recommended = recommendedSaved;

                    if (current_page > 0)
                        progressbar1.setVisibility(View.GONE);
                    else
                        mProgress_bar_top.setVisibility(View.GONE);

                    if (recommendArrayList.size() < recommendedSaved.content.size()) {
                        if (itemsLeft == -1) {
                            recommendArrayList.clear();
                            itemsLeft = recommendedSaved.content.size();
                        }

                        if (itemsLeft == recommendedSaved.content.size())
                            startCount = 0;
                        else
                            startCount = startCount + 10;

                        endCount = startCount + 10;
                        if (endCount > recommendedSaved.content.size())
                            endCount = recommendedSaved.content.size();

                        for (int i = startCount; i < endCount; i++) {
                            recommendArrayList.add(recommendedSaved.content.get(i));
                        }

                        itemsLeft = itemsLeft - 10;

                        handleRecommendedPagination(false);
                    }
                }/* else if (!ConnectionManager.getInstance(MoreDataActivity.this).isConnected()) {
                    Log.e(TAG, "ConnectionManager Not Connected");
                }*/ else if (recommendedSaved == null || count <= recommendedSaved.totalCount) {
                    Log.e("Recommended Url", ApiRequest.RECOMMENDED_LIST);
                    StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                            ApiRequest.RECOMMENDED_LIST, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            Log.e("Recommended_responce---", response.toString());

                            if (current_page > 0) {
                                progressbar1.setVisibility(View.GONE);
                            } else {
                                mProgress_bar_top.setVisibility(View.GONE);
                            }

                            try {
                                JSONObject mObj = new JSONObject(response);
                                if (mObj.optInt("code") == 1) {
                                    MultitvCipher mcipher = new MultitvCipher();
                                    String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                    Log.e("Recommended response---", str);
                                    try {
                                        JSONObject newObj = new JSONObject(str);
                                        recommended = Json.parse(newObj.toString(), Recommended.class);

                                        sharedPreference.setPreferencesString(MoreActivity.this, "offset_" + "More_ Recommended", "" + recommended.offset);

                                        if (recommendedSaved != null) {
                                            recommendedSaved.offset = recommended.offset;
                                            recommendedSaved.content.addAll(recommended.content);
                                            AppController.getInstance().getCacheManager().putAsync(key, recommendedSaved, new PutCallback() {
                                                @Override
                                                public void onSuccess() {
                                                    Log.e("CacheManager", key + " object saved successfully");
                                                }

                                                @Override
                                                public void onFailure(Exception e) {
                                                    Log.e("RecommendedMore", e.getMessage());
                                                }
                                            });
                                        } else {
                                            AppController.getInstance().getCacheManager().putAsync(key, recommended, new PutCallback() {
                                                @Override
                                                public void onSuccess() {
                                                    Log.e("CacheManager", key + " object saved successfully");
                                                }

                                                @Override
                                                public void onFailure(Exception e) {
                                                    Log.e("Recommended", "Fialure" + e.getMessage());
                                                }
                                            });
                                        }
                                    } catch (JSONException e) {
                                        Log.e("Recommended", "JSONException" + e.getMessage());
                                    }
                                    handleRecommendedPagination(true);
                                }

                            } catch (Exception e) {
                                Log.e("Recommended", "Exception" + e.getMessage());
                            }


                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError e) {
                            Log.e("Recommended", "VolleyError" + e.getMessage());

                            if (current_page > 0)
                                progressbar1.setVisibility(View.GONE);
                            else
                                mProgress_bar_top.setVisibility(View.GONE);

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();

                            //params.put("lan", LocaleHelper.getLanguage(MoreDataActivity.this));
                            //params.put("m_filter", (PreferenceData.isMatureFilterEnable(MoreDataActivity.this) ? "" + 1 : "" + 0));

                            params.put("device", "android");
                            params.put("user_id", user_id);
                            params.put("current_offset", String.valueOf(count));
                            params.put("max_counter", "10");
                            return params;
                        }
                    };

                    // Adding request to request queue
                    AppController.getInstance().addToRequestQueue(jsonObjReq);
                } else {
                    if (current_page > 0)
                        progressbar1.setVisibility(View.GONE);
                    else
                        mProgress_bar_top.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void handleRecommendedPagination(boolean isFromApiRequest) {
        refreshListRecemended(recommended.content);

        if (isFromApiRequest) {
            if (recommended != null && recommended.content != null && recommended.content.size() != 0) {
                recommendArrayList.addAll(recommended.content);
            }
        }

        if (recommendArrayList != null && recommendArrayList.size() != 0) {
            recommendedAdapter.setLoaded();
            recommendedAdapter.notifyDataSetChanged();
        }
    }

    private void refreshListRecemended(List<Content> recommendArrayList) {

        if (recommendArrayList.size() > 0) {
            int lastIndex = recommendArrayList.size() - 1;
            if (recommendArrayList.get(lastIndex) == null) {
                recommendArrayList.remove(lastIndex);
                recommendedAdapter.notifyItemRemoved(lastIndex);
            }

        }
    }

    private void recommendtdShowData(boolean isFromSearching) {
        RecyclerView recyclerView;
        if (!isFromSearching)
            recyclerView = moreRecylerView;
        else
            recyclerView = searchRecylerview;

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        String VIDEO_URL = recommendArrayList.get(position).url.toString();
                        String CONTENT_TYPE = recommendArrayList.get(position).source.toString();
                        title = recommendArrayList.get(position).title;
                        type = recommendArrayList.get(position).type;
                        content_id = recommendArrayList.get(position).id;
                        if (recommendArrayList.get(position).des != null && recommendArrayList.get(position).des != null && !recommendArrayList.get(position).des.isEmpty()) {
                            des = recommendArrayList.get(position).des;
                        }
                        if (recommendArrayList.get(position).thumbnail.large != null && !recommendArrayList.get(position).thumbnail.large.equals("") && !recommendArrayList.get(position).thumbnail.large.equals(null)) {
                            thumbnail = recommendArrayList.get(position).thumbnail.large;
                        }
                        String fav_item = recommendArrayList.get(position).favorite.toString();
                        String video_id = recommendArrayList.get(position).id.toString();

                        Log.e("url", VIDEO_URL);

                        try {
                            if (!VIDEO_URL.equals("")) {
                                Intent videoIntent = new Intent(MoreActivity.this, VeqtaPlayerActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                videoIntent.putExtra("VIDEO_URL", VIDEO_URL);
                                videoIntent.putExtra("descreption", des);
                                videoIntent.putExtra("title", title);
                                videoIntent.putExtra("type", type);
                                videoIntent.putExtra("video_id", video_id);
                                videoIntent.putExtra("content_id", content_id);
                                videoIntent.putExtra("position", position);
                                videoIntent.putExtra("fav_item", fav_item);
                                videoIntent.putExtra("thumbnail", thumbnail);
                                videoIntent.putExtra("content_type", CONTENT_TYPE);
                                videoIntent.putExtra("CONTENT_TYPE_MULTITV", "VOD");
                                videoIntent.putExtra("WATCHED_DURATION", watchedDuration);
                                videoIntent.putExtra("SOCIAL_LIKES", recommendArrayList.get(position).social_like);
                                videoIntent.putExtra("SOCIAL_VIEWS", recommendArrayList.get(position).social_view);
                                startActivity(videoIntent);

                                // PlayerUtils.startPlayerActivity(mContext, VIDEO_URL,thumbnail, content_id, title,CONTENT_TYPE, des, "VOD", 1);
                            } else {
                                Toast.makeText(MoreActivity.this, "No Record Found", Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception ex) {
                            Log.e("error", ex.getMessage());
                        }

                    }
                })
        );

    }
    //-------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------recommended recyclerview finish-----------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------


    //-------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------live recyclerview show data-----------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------

    private void getLiveChannelData(final int current_page, final String categoryID) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (current_page > 0) {
                            progressbar1.setVisibility(View.VISIBLE);
                        } else {
                            mProgress_bar_top.setVisibility(View.VISIBLE);
                        }
                    }
                });

                Log.e("Live_channel_data", ApiRequest.LIVE_CHANNEL);

                String key = "Live";
                if (!TextUtils.isEmpty(categoryID)) {
                    key = "Live_" + categoryID;
                }
                final String cacheKey = key;
                final Type liveObjectType = new TypeToken<LiveParent>() {
                }.getType();
                AppController.getInstance().getCacheManager().getAsync(cacheKey, LiveParent.class, liveObjectType, new GetCallback() {
                    @Override
                    public void onSuccess(final Object object) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                liveParentSaved = (LiveParent) object;

                               /* boolean isLiveVersionChanged = VersionUtils.getIsLiveVersionChanged(MoreActivity.this, liveParentSaved, key);

                                if (isLiveVersionChanged) { //It means live data update is available
                                    liveParentSaved = null;
                                    count = 0;
                                    itemsLeft = 0;
                                }
*/
                                if (liveParentSaved != null && liveParentSaved.live != null && liveParentSaved.live.size() != 0 &&
                                        (count <= liveParentSaved.live.size() || count > liveParentSaved.totalcount)
                                        && ((itemsLeft == -1 && liveArrayList.size() == 0) || itemsLeft > 0)) {
                                    Log.e("CacheManager", cacheKey + " object retrieved successfully");

                                    liveParent = liveParentSaved;

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (current_page > 0)
                                                progressbar1.setVisibility(View.GONE);
                                            else
                                                mProgress_bar_top.setVisibility(View.GONE);
                                        }
                                    });

                                    if (liveArrayList.size() < liveParentSaved.live.size()) {
                                        if (itemsLeft == -1) {
                                            liveArrayList.clear();
                                            itemsLeft = liveParentSaved.live.size();
                                        }

                                        if (itemsLeft == liveParentSaved.live.size())
                                            startCount = 0;
                                        else
                                            startCount = startCount + 10;

                                        endCount = startCount + 10;
                                        if (endCount > liveParentSaved.live.size())
                                            endCount = liveParentSaved.live.size();

                                        for (int i = startCount; i < endCount; i++) {
                                            liveArrayList.add(liveParentSaved.live.get(i));
                                        }

                                        itemsLeft = itemsLeft - 10;

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                handleLivePagination(false);
                                            }
                                        });
                                    }
                                } else if (!ConnectionUtils.isNetworkConnected(MoreActivity.this)) {
                                    Log.e(TAG, "ConnectionManager Not Connected");

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (current_page > 0)
                                                progressbar1.setVisibility(View.GONE);
                                            else
                                                mProgress_bar_top.setVisibility(View.GONE);
                                        }
                                    });
                                } else if (liveParentSaved == null || count <= liveParentSaved.totalcount) {
                                    StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                                            ApiRequest.LIVE_CHANNEL, new Response.Listener<String>() {
                                        @Override
                                        public void onResponse(final String response) {
                                            new Thread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    try {
                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                if (current_page > 0)
                                                                    progressbar1.setVisibility(View.GONE);
                                                                else
                                                                    mProgress_bar_top.setVisibility(View.GONE);
                                                            }
                                                        });

                                                        JSONObject mObj = new JSONObject(response);

                                                        MultitvCipher mcipher = new MultitvCipher();
                                                        String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                                        Log.e("Live channel data", str);
                                                        liveParent = Json.parse(str.trim(), LiveParent.class);

                                                        if (!TextUtils.isEmpty(categoryID)) {
                                                            sharedPreference.setPreferencesString(MoreActivity.this, "offset_" + "Live_" + categoryID, "" + liveParent.offset);
                                                        } else {
                                                            sharedPreference.setPreferencesString(MoreActivity.this, "offset_" + "Live", "" + liveParent.offset);
                                                        }

                                                        if (liveParentSaved != null) {
                                                            liveParentSaved.offset = liveParent.offset;
                                                            liveParentSaved.live.addAll(liveParent.live);
                                                            AppController.getInstance().getCacheManager().put(cacheKey, liveParentSaved);
                                                        } else {
                                                            AppController.getInstance().getCacheManager().put(cacheKey, liveParent);
                                                        }

                                                        runOnUiThread(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                handleLivePagination(true);
                                                            }
                                                        });
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }).start();
                                        }
                                    }, new Response.ErrorListener() {

                                        @Override
                                        public void onErrorResponse(VolleyError error) {
                                            Log.e("Error", "Error: " + error.getMessage());

                                            if (current_page > 0)
                                                progressbar1.setVisibility(View.GONE);
                                            else
                                                mProgress_bar_top.setVisibility(View.GONE);
                                        }
                                    }) {

                                        @Override
                                        protected Map<String, String> getParams() {
                                            Map<String, String> params = new HashMap<>();

                                            //params.put("lan", LocaleHelper.getLanguage(MoreDataActivity.this));
                                            //params.put("m_filter", (PreferenceData.isMatureFilterEnable(MoreDataActivity.this) ? "" + 1 : "" + 0));

                                            params.put("device", "android");
                                            params.put("live_offset", liveParentSaved != null ? "" + liveParentSaved.offset : "" + 0);
                                            params.put("live_limit", "10");
                                            if (!TextUtils.isEmpty(categoryID)) {
                                                params.put("cat_id", "" + categoryID);
                                            }
                                            return params;
                                        }
                                    };

                                    // Adding request to request queue
                                    AppController.getInstance().addToRequestQueue(jsonObjReq);
                                } else {
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (current_page > 0)
                                                progressbar1.setVisibility(View.GONE);
                                            else
                                                mProgress_bar_top.setVisibility(View.GONE);
                                        }
                                    });
                                }
                            }
                        }).start();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        e.printStackTrace();
                    }
                });
            }
        }).start();
    }

    private void handleLivePagination(boolean isFromApiRequest) {
        if (isFromApiRequest) {
            if (liveParent != null && liveParent.live != null && liveParent.live.size() != 0) {
                liveArrayList.addAll(liveParent.live);
            }
        }

        if (liveArrayList != null && liveArrayList.size() != 0) {
            moreLiveAdapter.setLoaded();
            moreLiveAdapter.notifyDataSetChanged();
        }
    }

    private void initializeLiveChannelViews() {
        moreLiveAdapter = new MoreLiveAdapter(this, liveArrayList, moreRecylerView);
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        moreRecylerView.setLayoutManager(gridLayoutManager);
        moreRecylerView.setNestedScrollingEnabled(false);
        moreRecylerView.setAdapter(moreLiveAdapter);

        moreRecylerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (liveArrayList != null && liveArrayList.size() != 0) {
                            PlayerUtils.startPlayerActivity(MoreActivity.this,
                                    liveArrayList.get(position).url, "", "", "n/a", "", liveArrayList.get(position).id,
                                    "LIVE", 4);
                        }
                    }
                })
        );
    }

   /* private void initializeLiveChannelViewsForSearch() {
        moreLiveSearchAdapter = new MoreLiveAdapter(this, liveSearchArrayList, searchRecylerview);
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        searchRecylerview.setLayoutManager(gridLayoutManager);
        searchRecylerview.setNestedScrollingEnabled(false);
        searchRecylerview.setAdapter(moreLiveSearchAdapter);

        searchRecylerview.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if (liveSearchArrayList != null && liveSearchArrayList.size() != 0) {
                            PlayerUtils.startPlayerActivity(MoreDataActivity.this,
                                    liveSearchArrayList.get(position).url, "", "", "n/a", "", liveSearchArrayList.get(position).id,
                                    "LIVE", 4);
                        }
                    }
                })
        );
    }
*/

    //-------------------------------------------------------------------------------------------------------------------------
    //----------------------------------------live recyclerview show data-----------------------------------------------
    //--------------------------------------------------------------------------------------------------------------------------


    private void getHomeCatData(final String cat_id, final int current_page) {

        if (current_page > 0) {
            progressbar1.setIndeterminate(true);
            progressbar1.setVisibility(View.VISIBLE);
        } else {
            mProgress_bar_top.setVisibility(View.VISIBLE);
        }
        //Log.e("home_Cat_Data", AppUtils.generateUrl(getApplicationContext(), ApiRequest.NEW_SEARCH_URL));
        final String key = "More" + "_" + "cat" + "_" + cat_id;
        final Type objectType = new TypeToken<Recommended>() {
        }.getType();
        AppController.getInstance().getCacheManager().getAsync(key, Recommended.class, objectType, new GetCallback() {
            @Override
            public void onSuccess(Object object) {
                homeDataSave = (Recommended) object;
                if (homeDataSave != null && homeDataSave.content != null && homeDataSave.content.size() != 0 &&
                        (count <= homeDataSave.content.size() || count > homeDataSave.totalCount)
                        && ((itemsLeft == -1 && displaycategoryItemArraylist.size() == 0) || itemsLeft > 0)) {
                    Log.e("CacheManager", key + " object retrieved successfully");

                    recommended = homeDataSave;

                    if (current_page > 0)
                        progressbar1.setVisibility(View.GONE);
                    else
                        mProgress_bar_top.setVisibility(View.GONE);

                    if (displaycategoryItemArraylist.size() < homeDataSave.content.size()) {
                        if (itemsLeft == -1) {
                            displaycategoryItemArraylist.clear();
                            itemsLeft = homeDataSave.content.size();
                        }

                        if (itemsLeft == homeDataSave.content.size())
                            startCount = 0;
                        else
                            startCount = startCount + 10;

                        endCount = startCount + 10;
                        if (endCount > homeDataSave.content.size())
                            endCount = homeDataSave.content.size();

                        for (int i = startCount; i < endCount; i++) {
                            displaycategoryItemArraylist.add(homeDataSave.content.get(i));
                        }

                        itemsLeft = itemsLeft - 10;

                        handleCategoryPagination(false);
                    }
                } /*else if (!ConnectionManager.getInstance(MoreDataActivity.this).isConnected()) {
                    Log.e(TAG, "ConnectionManager Not Connected");
                }*/ else if (homeDataSave == null || count <= homeDataSave.totalCount) {
                    Log.e("more display category", ApiRequest.VIDEO_CAT_URL_CLIST);
                    StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                            ApiRequest.VIDEO_CAT_URL_CLIST, new Response.Listener<String>() {

                        @Override
                        public void onResponse(String response) {
                            try {
                                if (current_page > 0)
                                    progressbar1.setVisibility(View.GONE);
                                else
                                    mProgress_bar_top.setVisibility(View.GONE);

                                JSONObject mObj = new JSONObject(response);

                                MultitvCipher mcipher = new MultitvCipher();
                                String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                Log.e("more display category", str);
                                recommended = Json.parse(str.trim(), Recommended.class);

                                if (recommended.content != null && recommended.content.size() > 0)
                                    sharedPreference.setPreferencesString(MoreActivity.this, "offset_" + "More_Home_Category" + cat_id, "" + recommended.offset);

                                if (homeDataSave != null) {
                                    homeDataSave.offset = recommended.offset;
                                    homeDataSave.content.addAll(recommended.content);
                                    AppController.getInstance().getCacheManager().putAsync(key, homeDataSave, new PutCallback() {
                                        @Override
                                        public void onSuccess() {
                                            Log.e("CacheManager", key + " object saved successfully");
                                        }

                                        @Override
                                        public void onFailure(Exception e) {
                                            Log.e("more_home_category", e.getMessage());
                                        }
                                    });
                                } else {
                                    AppController.getInstance().getCacheManager().putAsync(key, recommended, new PutCallback() {
                                        @Override
                                        public void onSuccess() {
                                            Log.e("CacheManager", key + " object saved successfully");
                                        }

                                        @Override
                                        public void onFailure(Exception e) {
                                            Log.e("more_home_category", e.getMessage());
                                        }
                                    });
                                }

                                handleCategoryPagination(true);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.e("Error", "Error: " + error.getMessage());

                            if (current_page > 0)
                                progressbar1.setVisibility(View.GONE);
                            else
                                mProgress_bar_top.setVisibility(View.GONE);
                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() {
                            Map<String, String> params = new HashMap<>();

                            //params.put("lan", LocaleHelper.getLanguage(MoreDataActivity.this));
                            // params.put("m_filter", (PreferenceData.isMatureFilterEnable(MoreDataActivity.this) ? "" + 1 : "" + 0));

                            params.put("device", "android");
                            params.put("user_id", user_id);
                            params.put("current_offset", String.valueOf(count));
                            params.put("cat_id", cat_id);
                            params.put("max_counter", "10");
                            //params.put("search_tag", query);
                            Set<String> keys = params.keySet();
                            for (String key : keys) {
                                Log.e(TAG, "getMoreContent().getParams: " + key + "      " + params.get(key));
                            }
                            return params;
                        }
                    };

                    // Adding request to request queue
                    AppController.getInstance().addToRequestQueue(jsonObjReq);
                } else {
                    if (current_page > 0)
                        progressbar1.setVisibility(View.GONE);
                    else
                        mProgress_bar_top.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Exception e) {
                e.printStackTrace();
            }
        });
    }

    private void handleCategoryPagination(boolean isFromApiRequest) {
        refreshHomeList(recommended.content);
        if (isFromApiRequest) {
            if (recommended != null && recommended.content != null && recommended.content.size() != 0) {
                displaycategoryItemArraylist.addAll(recommended.content);
            }
        }

        if (displaycategoryItemArraylist != null && displaycategoryItemArraylist.size() != 0) {
            moreHomeDisplayCategoryAdapter.setLoaded();
            moreHomeDisplayCategoryAdapter.notifyDataSetChanged();
        }
    }

    private void refreshHomeList(List<Content> displasyList) {

        if (displasyList.size() > 0) {
            int lastIndex = displasyList.size() - 1;
            if (displasyList.get(lastIndex) == null) {
                displasyList.remove(lastIndex);
                moreHomeDisplayCategoryAdapter.notifyItemRemoved(lastIndex);
            }
        }
    }

    private void displayCategoryDataShow() {
        moreRecylerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        String VIDEO_URL = displaycategoryItemArraylist.get(position).url.toString();
                        String CONTENT_TYPE = displaycategoryItemArraylist.get(position).source.toString();
                        title = displaycategoryItemArraylist.get(position).title;
                        type = displaycategoryItemArraylist.get(position).meta.type;
                        content_id = "" + displaycategoryItemArraylist.get(position).categoryIdsList.get(0);
                        if (displaycategoryItemArraylist.get(position).des != null && displaycategoryItemArraylist.get(position).des != null && !displaycategoryItemArraylist.get(position).des.isEmpty()) {
                            des = displaycategoryItemArraylist.get(position).des;
                        }
                        if (displaycategoryItemArraylist.get(position).thumbnail.large != null && !displaycategoryItemArraylist.get(position).thumbnail.large.equals("") && !displaycategoryItemArraylist.get(position).thumbnail.large.equals(null)) {
                            thumbnail = displaycategoryItemArraylist.get(position).thumbnail.large;
                        }
                        String fav_item = displaycategoryItemArraylist.get(position).favorite.toString();
                        String video_id = displaycategoryItemArraylist.get(position).id;

                        Log.e("url", VIDEO_URL);


                        try {
                            if (!VIDEO_URL.equals("")) {
                                Intent videoIntent = new Intent(MoreActivity.this, VeqtaPlayerActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                videoIntent.putExtra("VIDEO_URL", VIDEO_URL);
                                videoIntent.putExtra("descreption", des);
                                videoIntent.putExtra("title", title);
                                videoIntent.putExtra("type", type);
                                videoIntent.putExtra("video_id", video_id);
                                videoIntent.putExtra("content_id", content_id);
                                videoIntent.putExtra("position", position);
                                videoIntent.putExtra("fav_item", fav_item);
                                videoIntent.putExtra("thumbnail", thumbnail);
                                videoIntent.putExtra("content_type", CONTENT_TYPE);
                                videoIntent.putExtra("CONTENT_TYPE_MULTITV", "VOD");
                                videoIntent.putExtra("WATCHED_DURATION", watchedDuration);
                                videoIntent.putExtra("SOCIAL_LIKES", displaycategoryItemArraylist.get(position).social_like);
                                videoIntent.putExtra("SOCIAL_VIEWS", displaycategoryItemArraylist.get(position).social_view);
                                startActivity(videoIntent);


                                // PlayerUtils.startPlayerActivity(mContext, VIDEO_URL,thumbnail, content_id, title,CONTENT_TYPE, des, "VOD", 1);
                            } else {
                                Toast.makeText(MoreActivity.this, "No Record Found", Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception ex) {
                            Log.e("error", ex.getMessage());
                        }
                    }
                })
        );
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click on tool-bar here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }
        return super.onOptionsItemSelected(item);
    }

}
