package com.multitv.ott.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.multitv.ott.R;
import com.multitv.ott.Utils.AppNetworkAlertDialog;
import com.multitv.ott.Utils.AppPermissionController;
import com.multitv.ott.sharedpreference.SharedPreference;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Lenovo on 25-01-2017.
 */

public class SplashActivity extends AppCompatActivity implements AppPermissionController.OnAppPermissionControllerListener{
    private SharedPreference sharedPreference;
    private int status= 0;
    private AppPermissionController mAppPermissionController;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splash_layout_activity);
        sharedPreference =new SharedPreference();
        mAppPermissionController = new AppPermissionController(this, this);
        mAppPermissionController.initializedAppPermission();

        String LoginFrom=sharedPreference.getFromLogedIn(SplashActivity.this,"fromLogedin");
        if(!TextUtils.isEmpty(LoginFrom)) {
            Log.e("FromLoginStatus",LoginFrom);
            if (LoginFrom.equals("google")||LoginFrom.equals("facebook")) {
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                SplashActivity.this.finish();
            }else if(LoginFrom.equals("veqta")){
                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                SplashActivity.this.finish();
            }
        }
    }
    public void goToSignUpActivity(View v){

        if(!AppNetworkAlertDialog.isNetworkConnected(SplashActivity.this)){
            AppNetworkAlertDialog.showNetworkNotConnectedDialog(SplashActivity.this);
        }else{
            Intent intent = new Intent(SplashActivity.this, SignUpActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            SplashActivity.this.finish();
        }



    }
    public void goToLoginActivity(View v){
        if(!AppNetworkAlertDialog.isNetworkConnected(SplashActivity.this)){
            AppNetworkAlertDialog.showNetworkNotConnectedDialog(SplashActivity.this);
        }else {
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            SplashActivity.this.finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e("spalsh", "onRequestPermissionsResult: ");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mAppPermissionController != null) {
            mAppPermissionController.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (mAppPermissionController != null) {
//            mAppPermissionController.onRequestPermissionsResult(requestCode);
//        }
    }

    @Override
    public void onAppPermissionControllerListenerHaveAllRequiredPermission() {
        Log.e("Spalsh", "onAppPermissionControllerListenerHaveAllRequiredPermission: ");
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                int i = 0;
                while (i < 15) {
                    if (!isNetworkConnected()) {
                        break;
                    }
                    i++;
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (isNetworkConnected()) {
                    status=1;
                    Log.e("***Internet***","Connect");
                } else {
                    status=0;
                    Log.e("***Internet***","Not Connect");
                    AppNetworkAlertDialog.showNetworkNotConnectedDialog(SplashActivity.this);
                }
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }


}
