package com.multitv.ott.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.multitv.ott.R;
import com.multitv.ott.adapter.UserProfilePagerAdapter;
import com.multitv.ott.api.ApiRequest;
import com.multitv.ott.listeners.UpdateLoginInformationListner;
import com.multitv.ott.sharedpreference.SharedPreference;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.multitv.ott.Utils.ConstantVeqta.EXTRA_OPEN_HOME_SCREEN;

/**
 * Created by Lenovo on 04-03-2017.
 */

public class UserProfileActivity extends AppCompatActivity implements UpdateLoginInformationListner{
    private String TAG="UserProfileActivity";
    private TextView userName,emailTxt;
    private CircleImageView circleImageView;
    private Toolbar toolbar;
    private ImageView toolbar_back_btn,toolbar_next_btn;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private SharedPreference sharedPreference;
    private String user_id;
    private     UserProfilePagerAdapter userProfileAdapter;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.signup_btn));
        }
        setContentView(R.layout.activity_user_profile);
        inIt();
    }

    @Override
    public void onBackPressed() {
       /* if (selectImageAlertDialog != null && selectImageAlertDialog.isShowing()) {
            selectImageAlertDialog.dismiss();
            return;
        }*/

        //PreferenceData.setPreviousFragmentSelectedPosition(getApplicationContext(), 0);
        Intent intent = new Intent(UserProfileActivity.this, HomeActivity.class);
        intent.putExtra(EXTRA_OPEN_HOME_SCREEN, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        //UserProfileActivity.this.finish();
        super.onBackPressed();
    }

    private void inIt(){
        sharedPreference = new SharedPreference();
        user_id = new SharedPreference().getPreferencesString(UserProfileActivity.this, "user_id" + "_" + ApiRequest.TOKEN);

        //=============find view id====================
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        emailTxt = (TextView) findViewById(R.id.emailTxt);
        userName = (TextView) findViewById(R.id.title_name);
        circleImageView = (CircleImageView) findViewById(R.id.profile_image);
        toolbar_back_btn=(ImageView)findViewById(R.id.toolbar_back_btn);
        toolbar_next_btn=(ImageView)findViewById(R.id.toolbar_next_btn);
        toolbar_next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserProfileActivity.this, SavedProfileActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                //UserProfileActivity.this.finish();
            }
        });
        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserProfileActivity.this, SavedProfileActivity.class);
                startActivity(intent);

            }
        });

        toolbar_back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserProfileActivity.this, HomeActivity.class);
                intent.putExtra("home_activity", "profile_activity");
                intent.putExtra(EXTRA_OPEN_HOME_SCREEN, true);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                UserProfileActivity.this.finish();
            }
        });
        setupToolbar();
        setupCollapsingToolbar();
        setupViewPager();
        updateInformartion();

    }


    //==========setup view Collapsing toolbar================
    private void setupCollapsingToolbar() {
        final CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(
                R.id.collapsing_toolbar);

        collapsingToolbar.setTitleEnabled(false);
    }

    //==========setup view pager with tool bar================
    private void setupViewPager() {
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setOffscreenPageLimit(1);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setTabsFromPagerAdapter(userProfileAdapter);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/app_customfonts.ttf");
        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(tf);
                }
            }
        }

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                Log.e("position", String.valueOf(position));

            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }


    //==========setup tool bar================
    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    //==========setup adapter on view pager================
    private void setupViewPager(ViewPager viewPager) {
        String[] stringArray = getResources().getStringArray(R.array.profiletitle);
        userProfileAdapter = new UserProfilePagerAdapter(getSupportFragmentManager(), UserProfileActivity.this, stringArray, stringArray.length);
        viewPager.setAdapter(userProfileAdapter);
    }


    private void updateInformartion(){
        String firstName=sharedPreference.getUSerName(UserProfileActivity.this,"first_name");
        String lastName=sharedPreference.getUSerLastName(UserProfileActivity.this,"last_name");
        String email=sharedPreference.getEmailID(UserProfileActivity.this,"email_id");
        String dob=sharedPreference.getDob(UserProfileActivity.this,"dob");
        String phone=sharedPreference.getPhoneNumber(UserProfileActivity.this,"phone");
        String imgUrl=sharedPreference.getImageUrl(UserProfileActivity.this,"imgUrl");
        String gender_id=sharedPreference.getGender(UserProfileActivity.this,"gender_id");
        if(!TextUtils.isEmpty(firstName)) {
            userName.setVisibility(View.VISIBLE);
            userName.setText(firstName);
        }else{
            userName.setText(getResources().getString(R.string.edit_profile));
        }

        if(!TextUtils.isEmpty(lastName)) {
            userName.setVisibility(View.VISIBLE);
            userName.setText(firstName +" " +lastName);
        }else{
            userName.setText(getResources().getString(R.string.edit_profile));
        }

        if(!TextUtils.isEmpty(email)) {
            emailTxt.setVisibility(View.VISIBLE);
            emailTxt.setText(email);
        }else{
            emailTxt.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(imgUrl)){
            Picasso
                    .with(UserProfileActivity.this)
                    .load(imgUrl)
                    .placeholder(R.mipmap.intex_profile)
                    .error(R.mipmap.intex_profile)
                    .into(circleImageView);
        }else{
            //circleImageView.setImageResource(R.mipmap.intex_profile);
        }

    }

    @Override
    public void setProfileImage(String url) {

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e(TAG,"onNewIntent"+"method calling");
        updateInformartion();
    }



}
