package com.multitv.ott.activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.multitv.cipher.MultitvCipher;
import com.multitv.multitvcommonsdk.MultiTVCommonSdk;
import com.multitv.multitvcommonsdk.utils.MultiTVException;
import com.multitv.multitvplayersdk.MultiTvPlayer;
import com.multitv.ott.R;
import com.multitv.ott.Utils.AppNetworkAlertDialog;
import com.multitv.ott.Utils.ScreenUtils;
import com.multitv.ott.adapter.PlayerMoreContentFragmentAdapter;
import com.multitv.ott.api.ApiRequest;
import com.multitv.ott.controller.AppController;
import com.multitv.ott.custom.CustomTextView;
import com.multitv.ott.custom.ExpandableTextView;
import com.multitv.ott.sharedpreference.SharedPreference;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.multitv.ott.Utils.AppConstants.CONTENT_TYPE_MULTITV_PLAYER_ACTIVITY;
import static java.lang.String.valueOf;

/**
 * Created by Lenovo on 31-01-2017.
 */

public class VeqtaPlayerActivity extends YouTubeFailureRecoveryActivity implements MultiTVCommonSdk.AdDetectionListner,
        MultiTvPlayer.MultiTvPlayerListner, YouTubePlayer.OnInitializedListener{
    private String TAG="VeqtaPlayerActivity : ";
    private ExpandableTextView expandableTextView;
    private ImageView toggleImg;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private CustomTextView tittleTv,like_countTV;
    private ImageView btnShare,btnLike,btnFavorite;
    private Typeface tf;
    private String favorite, like,Faveroite_TAG = "Faveroite";
    private String video_Url, content_id, video_id, content_type_multitv, user_id, des, title,typePlayer;
    private PlayerMoreContentFragmentAdapter playerMoreContentFragmentAdapter;
    private SharedPreference sharedPreference;
    private Intent intent;

    private MultiTvPlayer player;
    private YouTubePlayerView youTubePlayerView;
    private FrameLayout framePlayerLayout;
    private LinearLayout content_nestedlayout,title_linear_bg;
    public boolean isYoutubePlayerInitializationGoingOn;
    private YouTubePlayer youTubePlayer;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private long total_duration, current_position;
    private int width, height,likes_count;
    private boolean isActivityInitialized, isOnCreateCalled = true, isHorizontalOrientation, isYoutubePlayerPaused;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.transparent_black_ninty_five));
        }
        setContentView(R.layout.activity_veqta_player);
        sharedPreference = new SharedPreference();
        user_id = sharedPreference.getPreferencesString(this, "user_id" + "_" + ApiRequest.TOKEN);
        tf = Typeface.createFromAsset(getAssets(), "fonts/app_customfonts.ttf");

        tabLayout = (TabLayout) findViewById(R.id.sliding_tabs);
        framePlayerLayout = (FrameLayout) findViewById(R.id.main_player_layout_bg);
        title_linear_bg = (LinearLayout) findViewById(R.id.border);
        tittleTv=(CustomTextView) findViewById(R.id.tittle);
        like_countTV=(CustomTextView) findViewById(R.id.like_count);
        content_nestedlayout = (LinearLayout) findViewById(R.id.appbar_bg);
        viewPager=(ViewPager)findViewById(R.id.view_pager);
        expandableTextView = (ExpandableTextView) this.findViewById(R.id.expandableTextView);
        toggleImg = (ImageView) this.findViewById(R.id.collaspe_change_img);
        btnShare = (ImageView) findViewById(R.id.shareBtn);
        btnLike = (ImageView) findViewById(R.id.likeBtn);
        btnFavorite = (ImageView) findViewById(R.id.favoriteBtn);

        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!AppNetworkAlertDialog.isNetworkConnected(VeqtaPlayerActivity.this)){
                    AppNetworkAlertDialog.showNetworkNotConnectedDialog(VeqtaPlayerActivity.this);
                }else {
                    shareVideo();
                }
            }
        });

        btnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppNetworkAlertDialog.isNetworkConnected(VeqtaPlayerActivity.this)) {
                    AppNetworkAlertDialog.showNetworkNotConnectedDialog(VeqtaPlayerActivity.this);
                }
                {
                    if (Faveroite_TAG.equalsIgnoreCase("Faveroite")) {
                        addToFavorite("1");
                        sharedPreference.setPreferencesString(VeqtaPlayerActivity.this, "favorite_" + video_id, "1");
                        Faveroite_TAG = "UnFaveroite";
                        btnFavorite.setImageResource(R.mipmap.favorite_selected);
                    } else if (Faveroite_TAG.equalsIgnoreCase("UnFaveroite")) {
                        addToFavorite("0");
                        sharedPreference.setPreferencesString(VeqtaPlayerActivity.this, "favorite_" + video_id, "0");
                        Faveroite_TAG = "Faveroite";
                        btnFavorite.setImageResource(R.mipmap.favorite_icon_unselect);
                    }
                }
            }
        });

        btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!AppNetworkAlertDialog.isNetworkConnected(VeqtaPlayerActivity.this)) {
                    AppNetworkAlertDialog.showNetworkNotConnectedDialog(VeqtaPlayerActivity.this);
                } else {
                    if (like != null && !like.equals("null") && !like.equals("") && !like.isEmpty()) {
                        if (like.equalsIgnoreCase("0")) {
                            sendLikeToServer(1);
                            btnLike.setImageResource(R.mipmap.like_selected);

                        } else if (like.equalsIgnoreCase("1")) {
                            sendLikeToServer(0);
                            likes_count = likes_count - 1;
                            Log.e("**like_countbtnclick**", "" + likes_count);
                            like = "0";
                            like_countTV.setText(getResources().getString(R.string.likes_hint) + " " + likes_count);
                            sharedPreference.setPreferencesLikes_Count(VeqtaPlayerActivity.this, "like_count" + video_id + "_" + user_id, "" + likes_count);
                            sharedPreference.setPreferencesLikes(VeqtaPlayerActivity.this, "like_" + video_id + "_" + user_id, "0");
                            btnLike.setImageResource(R.mipmap.likes_unselect);
                        }
                    } else {
                        getLikeToServer();
                    }
                }
            }
        });

        inIt();

    }
    private void inIt(){
        intent = getIntent();
        content_type_multitv = intent.getStringExtra("CONTENT_TYPE_MULTITV");
        Log.e(TAG, "content_type_multitv : " + content_type_multitv);
        Log.e(TAG, "onCreate: USER ID :" + user_id);

        current_position = intent.getLongExtra("WATCHED_DURATION", 0);
        if (intent.getStringExtra("VIDEO_URL") != null && !intent.getStringExtra("VIDEO_URL").isEmpty()) {
            video_Url = intent.getStringExtra("VIDEO_URL");
            Log.e(TAG, "onCreate: video_Url" + video_Url);
        }

        if (intent.getStringExtra("title") != null && !intent.getStringExtra("title").isEmpty()) {
            title = intent.getStringExtra("title");
            tittleTv.setText(title);
            Log.e(TAG, "onCreate: Title :" + title);
        }

        if (intent.getStringExtra("descreption") != null && !intent.getStringExtra("descreption").isEmpty()) {
            des = intent.getStringExtra("descreption");
            Log.e(TAG, "onCreate: description :" + des);
        }

        if (intent.getStringExtra("content_type") != null && !intent.getStringExtra("content_type").isEmpty()) {
            typePlayer = intent.getStringExtra("content_type");
            Log.e(TAG, "onCreate: typePlayer :" + typePlayer);
        }

        if (intent.getStringExtra("content_id") != null && !intent.getStringExtra("content_id").isEmpty()) {
            content_id = intent.getStringExtra("content_id");
            Log.e(TAG, "onCreate: content_id :" + content_id);
        }

        if (intent.getStringExtra("video_id") != null && !intent.getStringExtra("video_id").isEmpty()) {
            video_id = intent.getStringExtra("video_id");
            Log.e(TAG, "onCreate: VIDEO ID : " + video_id);
        }

        expandableTextView.setAnimationDuration(750L);
        expandableTextView.setInterpolator(new OvershootInterpolator());
        expandableTextView.setExpandInterpolator(new OvershootInterpolator());
        expandableTextView.setCollapseInterpolator(new OvershootInterpolator());
        expandableTextView.setTypeface(tf);
        if(!TextUtils.isEmpty(des)) {
            toggleImg.setVisibility(View.VISIBLE);
            expandableTextView.setVisibility(View.VISIBLE);
            expandableTextView.setText(des);
        }else{
            expandableTextView.setVisibility(View.GONE);
            toggleImg.setVisibility(View.GONE);
        }
        toggleImg.setOnClickListener(new View.OnClickListener()
        {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onClick(final View v)
            {
                if (expandableTextView.isExpanded()) {
                    expandableTextView.collapse();
                    toggleImg.setImageResource(R.mipmap.dc_down);
                } else {
                    expandableTextView.expand();
                    toggleImg.setImageResource(R.mipmap.dc_up);
                }
            }
        });

        expandableTextView.setOnExpandListener(new ExpandableTextView.OnExpandListener() {
            @Override
            public void onExpand(final ExpandableTextView view) {
                Log.e(TAG+":"+"oncreate", "ExpandableTextView-expanded");
            }

            @Override
            public void onCollapse(final ExpandableTextView view) {
                Log.e(TAG+":"+"oncreate", "ExpandableTextView-collapsed");
            }
        });

        setupViewPager();
        setupAllPlayerView();
        getlikeVdieo();
        getFavoriteVideo();

        if (!TextUtils.isEmpty(content_type_multitv)) {
            setMultitvPlayer(video_Url, content_type_multitv);
        } else {
            setMultitvPlayer(video_Url, "VOD");
        }
        try {
            playVideo();
        } catch (Exception e) {
            Log.e("playVideo method : ","Error while video playing");
        }
    }

    private void shareVideo(){
        try {
        Uri shreVideoUrl = Uri.parse(intent.getStringExtra("VIDEO_URL"));
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Veqta");
        String shareText = "\nEnjoy this Awesome Video From Veqta\n\n";
        shareText = shareText + shreVideoUrl;
        Log.e("share***", "" + shareText);
        intent.putExtra(Intent.EXTRA_TEXT, shareText);
        startActivity(Intent.createChooser(intent, "choose one"));
    } catch (Exception e) {
        e.printStackTrace();
    }
    }
    private void getlikeVdieo(){
        //--likes----
        String likeStoredFromPreference =
                sharedPreference.getPreferencesLikes(this, "like_" + video_id + "_" + user_id);
        String likeCountStoredFromPreference =
                sharedPreference.getPreferencesLikes_Count(VeqtaPlayerActivity.this, "like_count" + video_id + "_" + user_id);

        if (!TextUtils.isEmpty(likeCountStoredFromPreference)) {
            likes_count = Integer.parseInt(likeCountStoredFromPreference);
            like_countTV.setText(getResources().getString(R.string.likes_hint) +" "+ likes_count);
        } else {
            getLikeToServer();
        }

        if (!TextUtils.isEmpty(likeStoredFromPreference)) {
            like = likeStoredFromPreference;
            if (like.equalsIgnoreCase("0"))
                btnLike.setImageResource(R.mipmap.likes_unselect);
            else
                btnLike.setImageResource(R.mipmap.like_selected);
        } else {
            getLikeToServer();
        }

    }

    private void getFavoriteVideo(){
        //---faveroite----
        String faveroiteStoredFromPreference =
                sharedPreference.getPreferencesString(this, "favorite_" + video_id);

        if (!TextUtils.isEmpty(faveroiteStoredFromPreference))
            favorite = faveroiteStoredFromPreference;
        else
            favorite = intent.getStringExtra("fav_item");

        if (!TextUtils.isEmpty(favorite)) {
            if (favorite.equalsIgnoreCase("1")) {
                btnFavorite.setImageResource(R.mipmap.favorite_selected);
            } else {
                btnFavorite.setImageResource(R.mipmap.favorite_icon_unselect);
            }
        }
    }

    private void setupViewPager() {
        viewPager.setOffscreenPageLimit(1);
        ArrayList<String> titleArrayList = new ArrayList<>();
        titleArrayList.add(getResources().getString(R.string.related_tab));
        titleArrayList.add(getResources().getString(R.string.recommended_tab));
        playerMoreContentFragmentAdapter = new PlayerMoreContentFragmentAdapter(
                getFragmentManager(), VeqtaPlayerActivity.this, CONTENT_TYPE_MULTITV_PLAYER_ACTIVITY, titleArrayList, content_id);
        viewPager.setAdapter(playerMoreContentFragmentAdapter);

        tabLayout.setupWithViewPager(viewPager);

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(tf);
                }
            }
        }


        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setTabsFromPagerAdapter(playerMoreContentFragmentAdapter);
        viewPager.setOffscreenPageLimit(1);
        Log.e("tab****", "enter tab layout");
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // array.clear();
                playerMoreContentFragmentAdapter.notifyDataSetChanged(); //t
                Log.d("position", String.valueOf(position));

                switch (position) {

                    case 1:
                        //like a example
                        //  setViewPagerByIndex(0);
                        break;
                }
            }

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }
    private void setupAllPlayerView(){
        framePlayerLayout.setLayoutParams(new RelativeLayout.LayoutParams
                (RelativeLayout.LayoutParams.MATCH_PARENT, ScreenUtils.getScreenHeight(this) / 3));
    }


    private void playVideo() {
        Log.e(TAG, "playVideo: " + (intent.getStringExtra("video_id")) + "   " + typePlayer);

        if (typePlayer.equals("youtube")) {
            isYoutubePlayerInitializationGoingOn = true;
            setyoutubePlayer();
            //FirebaseUtils.logVideoPlayData(video_id, title, "YOUTUBE");
        }  else {
            try {
                player.preparePlayer();
                player.setVisibility(View.VISIBLE);
                /*FirebaseUtils.logVideoPlayData(video_id, title, "MULTITV");
                if (intent.getStringExtra("video_id") != null && !intent.getStringExtra("video_id").isEmpty()) {
                    new SendAnalytics(this, ApiRequest.TOKEN, intent.getStringExtra("video_id"), null, null, 5).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }*/
            } catch (MultiTVException e) {
                Log.e("MultiTVException",e.getMessage());
            }
        }
    }


    private void setMultitvPlayer(String video_Url, String content_type_multitv) {

        if (player != null) {
            player.release();
            player = null;
        }
        player = (MultiTvPlayer) findViewById(R.id.player);
        player.setVisibility(View.GONE);

        if (content_type_multitv.equalsIgnoreCase("VOD"))
            player.setContentType(MultiTvPlayer.ContentType.VOD);
        else if (content_type_multitv.equalsIgnoreCase("LIVE"))
            player.setContentType(MultiTvPlayer.ContentType.LIVE);

        player.setKeyToken(ApiRequest.TOKEN);
        player.setPreRollEnable(false);
            /*player.setContentID(channelId);*/
        player.setContentFilePath(video_Url);
        player.setMultiTvPlayerListner(this);
        if (current_position != 0)
            player.resumeFromPosition((int) current_position);
        try {
            player.setAdSkipEnable(false, 5000);
            // player.preparePlayer();
        } catch (MultiTVException e) {
            Log.e("MultiTVPlayer : ",e.getMessage());
        }

    }
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        isYoutubePlayerInitializationGoingOn = false;
        if (!wasRestored && !isFinishing() && !isDestroyed()) {
            try {
                this.youTubePlayer = youTubePlayer;
                video_Url = video_Url.substring(video_Url.lastIndexOf("=") + 1);
                /*youTubePlayer.cueVideo(video_Url);*/
                youTubePlayer.loadVideo(video_Url);
                youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                /*if (intent.getStringExtra("video_id") != null && !intent.getStringExtra("video_id").isEmpty()) {
                    new SendAnalytics(this, ApiRequest.TOKEN, intent.getStringExtra("video_id"), null, null, 5).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private void setyoutubePlayer() {
        try {
            youTubePlayerView = (YouTubePlayerView) findViewById(R.id.you_tube_player);
            //btnPlay.setVisibility(View.GONE);
            //player_image.setVisibility(View.GONE);
            youTubePlayerView.setVisibility(View.VISIBLE);
            youTubePlayerView.initialize(getResources().getString(R.string.you_tube_key), VeqtaPlayerActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult errorReason) {
        isYoutubePlayerInitializationGoingOn = false;
        if (errorReason.isUserRecoverableError()) {
            errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
        } else {
            String errorMessage = String.format(
                    "error", errorReason.toString());
            Log.e("MultiTvPlayerActivity", errorMessage);
        }
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubePlayerView;
    }

    @Override
    public void onAdCallback(String value, String session) {
        player.onAdCallback(value, session);
    }

    @Override
    public void onPlayerReady() {
        try {
            player.start();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage()
            );
        }
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (width == 0 || height == 0) {
            width = framePlayerLayout.getLayoutParams().width;
            height = framePlayerLayout.getLayoutParams().height;
        }

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            isHorizontalOrientation = true;
            content_nestedlayout.setVisibility(View.GONE);
            title_linear_bg.setVisibility(View.GONE);
            framePlayerLayout.getLayoutParams().width = RelativeLayout.LayoutParams.MATCH_PARENT;
            framePlayerLayout.getLayoutParams().height = RelativeLayout.LayoutParams.MATCH_PARENT;
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            isHorizontalOrientation = false;
            content_nestedlayout.setVisibility(View.VISIBLE);
            title_linear_bg.setVisibility(View.VISIBLE);
            framePlayerLayout.setLayoutParams(new RelativeLayout.LayoutParams
                    (RelativeLayout.LayoutParams.MATCH_PARENT, ScreenUtils.getScreenHeight(this) / 3));
        }

        player.onConfigurationChanged(newConfig);
    }
    @Override
    protected void onPause() {
        super.onPause();

        if (!isActivityInitialized || isOnCreateCalled) {
            isOnCreateCalled = false;
            return;
        }
        if (typePlayer.equals("youtube") && youTubePlayer != null) {
            youTubePlayer.release();
            youTubePlayer = null;
            isYoutubePlayerPaused = true;
            isYoutubePlayerInitializationGoingOn = false;
        }

        player.setBackgrounded(true);

    }
    @Override
    protected void onResume() {
        super.onResume();

        if (!isActivityInitialized || isOnCreateCalled) {
            isOnCreateCalled = false;
            return;
        }

        if (typePlayer.equals("youtube") && isYoutubePlayerPaused) {
            isYoutubePlayerInitializationGoingOn = true;
            //btnPlay.setVisibility(View.GONE);
            setyoutubePlayer();
        }
        player.setBackgrounded(false);

    }


    @Override
    public void onBackPressed() {
        if (isYoutubePlayerInitializationGoingOn)
            return;
        if (isHorizontalOrientation) {
            isHorizontalOrientation = false;
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else
            finish();
    }


    private void sendWatchingDataToServer() {
        if(!AppNetworkAlertDialog.isNetworkConnected(VeqtaPlayerActivity.this)){
            AppNetworkAlertDialog.showNetworkNotConnectedDialog(VeqtaPlayerActivity.this);
        }
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                ApiRequest.WATCHING_SET_DATA_URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("api_get_watching", response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                //params.put("lan", LocaleHelper.getLanguage(MultiTvPlayerActivity.this));
                //params.put("m_filter", (PreferenceData.isMatureFilterEnable(MultiTvPlayerActivity.this) ? "" + 1 : "" + 0));

                params.put("c_id", user_id); //user id
                params.put("content_id", video_id);
                params.put("duration", "" + current_position);
                params.put("total_duration", "" + total_duration);

                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        user_id = new SharedPreference().getPreferencesString(VeqtaPlayerActivity.this, "user_id" + "_" + ApiRequest.TOKEN);

        if (player != null) {
            total_duration = player.getDuration();
            current_position = player.getCurrentPosition();
            player.release();
        }
        Log.e(TAG, "Player total duration : " + total_duration +
                " current position : " + current_position);
        sendWatchingDataToServer();
    }
    
    
    
    //-----------------------------------------------------------like get from server---------------------------------------------
    //-----------------------------------------------------------------------------------------------------------------------------
    //------------------------------------------------------------------------------------------------------------------------
    //----send like to server---
    private void getLikeToServer() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.e("===LIKE_URL==",  ApiRequest.LIKE_URL);
                if(!AppNetworkAlertDialog.isNetworkConnected(VeqtaPlayerActivity.this)){
                   return;
                }
                StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                        ApiRequest.LIKE_URL, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("===likes_responce==", response);

                        try {
                            JSONObject mObj = new JSONObject(response);
                            if (mObj.optInt("code") == 1) {
                                MultitvCipher mcipher = new MultitvCipher();
                                String str = new String(mcipher.decryptmyapi(mObj.optString("result")));
                                Log.e("===Like_response===", str);
                                JSONObject mObj1 = new JSONObject(str);
                                like = mObj1.getString("like");
                                likes_count = Integer.parseInt(mObj1.getString("total_likes"));
                                Log.e("===total_likes===", "" + likes_count);
                                like_countTV.setText(getResources().getString(R.string.likes_hint) +" "+ likes_count);
                                Log.e("===like==", like);
                                sharedPreference.setPreferencesLikes_Count(VeqtaPlayerActivity.this, "like_count" + video_id + "_" + user_id, "" + likes_count);
                                sharedPreference.setPreferencesLikes(VeqtaPlayerActivity.this, "like_" + video_id + "_" + user_id, like);
                                if (like.equalsIgnoreCase("0")) {
                                    btnLike.setImageResource(R.mipmap.likes_unselect);
                                } else {
                                    btnLike.setImageResource(R.mipmap.like_selected);
                                }
                                like_countTV.setText(getResources().getString(R.string.likes_hint) +" "+ likes_count);
                            } /*else if (mObj.optInt("code") == 0) {
                                btn_like.setImageResource(R.mipmap.unlike_bg);
                            }*/

                        } catch (Exception e) {
                            Log.e("Error", "Error: " + e.getMessage());
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Error", "Error: " + error.getMessage());
                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();

                        //params.put("lan", LocaleHelper.getLanguage(MultiTvPlayerActivity.this));
                        //params.put("m_filter", (PreferenceData.isMatureFilterEnable(MultiTvPlayerActivity.this) ? "" + 1 : "" + 0));

                        params.put("device", "android");
                        params.put("content_id", video_id);
                        params.put("type", "video");
                        params.put("user_id", user_id);
                        Log.e("===user_id==", user_id);
                        Log.e("===content_id===", video_id);

                        return params;
                    }
                };

                // Adding request to request queue
                AppController.getInstance().addToRequestQueue(jsonObjReq);
            }
        }).start();
    }


    private void sendLikeToServer(final int likes) {
        Log.e("***sendLikeToServer***", "URL :_ :"+ApiRequest.LIKE_URL_Post);
        if(!AppNetworkAlertDialog.isNetworkConnected(VeqtaPlayerActivity.this)){
            return;
        }
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                ApiRequest.LIKE_URL_Post, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e("***likes_getResponce***", response.toString());
                try {
                    JSONObject mObj = new JSONObject(response);
                    if (mObj.optInt("code") == 1) {
                        Log.e("***code***", "code==1");
                        JSONObject mObj1 = new JSONObject(response);
                        String result_like_send = mObj1.getString("result");
                        Log.e("***likes_sending****", result_like_send);
                        if (!result_like_send.equals("") && !result_like_send.isEmpty()) {
                            getLikeToServer();
                            Log.e("***RED***", result_like_send);
                        }
                    } else {
                        Log.e("***code***", "code==0");
                        getLikeToServer();
                    }
                } catch (Exception e) {
                    Log.e("", "Error: " + e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("", "Error: " + error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                //params.put("lan", LocaleHelper.getLanguage(MultiTvPlayerActivity.this));
                //params.put("m_filter", (PreferenceData.isMatureFilterEnable(MultiTvPlayerActivity.this) ? "" + 1 : "" + 0));

                params.put("device", "android");
                params.put("content_id", video_id);
                params.put("type", "video");
                params.put("user_id", user_id);
                params.put("like", valueOf(likes));

              /*  Log.e("***likes***", valueOf(likes));
                Log.e("user_id_fromVideoplay",user_id);*/
                Log.e("user_id", user_id);
                Log.e("video_id", video_id);
                Log.e("like", valueOf(likes));
                Log.e("device", "android");
                Log.e("content_type", "video");
                return params;
            }
        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }


    //---favorite send to server-----

    private void addToFavorite(final String favourite) {
        Log.e(TAG,"favoriteUrl"+ "" + ApiRequest.FAVORITE_URL);

        if(!AppNetworkAlertDialog.isNetworkConnected(VeqtaPlayerActivity.this)){
           return;
        }
        StringRequest jsonObjReq = new StringRequest(Request.Method.POST,
                ApiRequest.FAVORITE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG,"faveorte-add_fav"+response.toString());
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG,"faveorte-Error: " + error.getMessage());
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();

                //params.put("lan", LocaleHelper.getLanguage(MultiTvPlayerActivity.this));
               // params.put("m_filter", (PreferenceData.isMatureFilterEnable(MultiTvPlayerActivity.this) ? "" + 1 : "" + 0));

                params.put("user_id", user_id);
                params.put("device", "android");
                params.put("content_type", "video");
                params.put("content_id", video_id);
                params.put("favorite", favourite);

                Log.e(TAG,"user_id"+user_id);
                Log.e(TAG,"favorite"+ favourite);


                return params;
            }
        };

        AppController.getInstance().addToRequestQueue(jsonObjReq);
    }


}
